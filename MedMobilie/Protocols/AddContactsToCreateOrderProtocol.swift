//
//  AddContactsToCreateOrderProtocol.swift
//  MedMobilie
//
//  Created by dr.mac on 09/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
protocol AddContactsToCreateOrderProtocol {
    func contactdetails(name : String , Contact_id : Int)
}
