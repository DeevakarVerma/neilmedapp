//
//  VisitDetailsCell.swift
//  MedMobilie
//
//  Created by dr.mac on 23/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class VisitDetailsCell: UITableViewCell {

    
    @IBOutlet weak var lblDetails: UILabel?
    @IBOutlet weak var lblAddressVisit: UILabel?
    @IBOutlet weak var lblVisitDate: UILabel?
    @IBOutlet weak var VisitName: UILabel?
    @IBOutlet weak var lblVisitType: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
