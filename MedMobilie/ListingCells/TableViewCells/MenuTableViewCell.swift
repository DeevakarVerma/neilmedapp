//
//  MenuTableViewCell.swift
//  MedMobilie
//
//  Created by MAC on 11/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Title_IMG: UIImageView!
    @IBOutlet weak var Title_LBL: UILabel!
    
}
