//
//  MMTextfield.swift
//  MedMobilie
//
//  Created by MAC on 27/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

protocol MMTextFieldDelegate: class {
    func mmTextFieldDidBeginEditing(_ textField: UITextField)
}

@IBDesignable
class MMTextfield: UIView {

    var delegate: MMTextFieldDelegate?
    
    private let lblTitle = UILabel()
    var txtField : UITextField?
    private let borderLayer = CALayer()
    private let borderThickness : CGFloat = 1
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateRightimage()
        }
    }
    
    @IBInspectable var title: String? {
        didSet {
            updateTitle()
        }
    }
    
    @IBInspectable var layerColor: UIColor = .gray {
        didSet {
            updateLayerColor()
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        lblTitle.frame =  CGRect.init(x: 0, y: 0, width: rect.width, height: rect.height/4)

        lblTitle.font = UIFont.init(name: "Regular", size: 12.0)
        lblTitle.textColor = GlobalConstantClass.setColor.borderColor
        
        self.addSubview(lblTitle)
        txtField = UITextField.init(frame: CGRect.init(x: 0, y: lblTitle.frame.maxY, width: rect.width, height: rect.height - lblTitle.frame.height))
        txtField?.textColor = UIColor.black
        txtField?.delegate = self
        self.addSubview(txtField!)
        
        borderLayer.frame = rectForBorder(borderThickness, isFilled: true)
        borderLayer.backgroundColor = layerColor.cgColor
        self.layer.addSublayer(borderLayer)
        
    }
    
    // MARK: Private functions
    
    private func updateRightimage() {
        if let image = rightImage {
            txtField?.rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
//            imageView.backgroundColor = .red
            txtField?.rightView = imageView
        } else {
            txtField?.rightViewMode = UITextField.ViewMode.never
            txtField?.rightView = nil
        }
    }
    private func updateTitle() {
        lblTitle.text = title
    }

    private func updateLayerColor() {
       borderLayer.backgroundColor = layerColor.cgColor
    }
    
    private func rectForBorder(_ thickness: CGFloat, isFilled: Bool) -> CGRect {
        if isFilled {
            return CGRect(origin: CGPoint(x: 0, y: frame.height-thickness), size: CGSize(width: frame.width, height: thickness))
        } else {
            return CGRect(origin: CGPoint(x: 0, y: frame.height-thickness), size: CGSize(width: 0, height: thickness))
        }
    }

}

extension MMTextfield: UITextFieldDelegate {
        func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.mmTextFieldDidBeginEditing(textField)
    }
}
