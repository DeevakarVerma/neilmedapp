//
//  DateFormatView.swift
//  MedMobilie
//
//  Created by MAC on 07/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

func DateFormat(FormatType:String) -> String {
    
    let date = Date()
    let date_TimeFormatter = DateFormatter()
    date_TimeFormatter.dateFormat = FormatType
    let date_hour = date_TimeFormatter.string(from: date)
    return date_hour
    
}
