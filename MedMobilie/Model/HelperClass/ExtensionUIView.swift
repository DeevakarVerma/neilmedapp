//
//  ExtensionView.swift
//  MedMobilie
//
//  Created by MAC on 28/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

extension UIView{
    @IBInspectable var cornderRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
            
            layer.borderWidth = 1.2
            layer.borderColor = UIColor.init(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.6).cgColor
            layer.shadowOpacity = 0.2
            layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            layer.shadowRadius = 5.0
        }
    }
    
    func dropShadow(color: UIColor,cornerRadius: CGFloat, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
        
        // corner radius
        layer.cornerRadius = cornerRadius
        // shadow
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offSet
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.masksToBounds = false
    }
}
