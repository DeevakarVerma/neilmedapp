//
//  Constants.swift
//  TCA
//
//  Created by webdeskers on 14/02/18.
//  Copyright © 2018 webdeskers. All rights reserved.
//

import UIKit

class Constants{
    
    struct setColor {
        static var appColor: UIColor = UIColor.init(red: 242.0/255.0, green: 242.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        static let borderColor: UIColor = UIColor(red: 158.0/255.0, green: 161.0/255.0, blue: 160.0/255.0, alpha: 1.0)
        static let greyTextColourColor: UIColor = UIColor(red: 109/255, green: 109/255, blue: 120/255, alpha: 1)
        
    }
}

struct storyboardID {
    static let navigationIdentifier = "navigationID"
    static let tabBarIdentifier = "TabBarVC"
    static let kyDrawerIdentifierSegue = "kyDrawerIdentifierSegue"
    static let menuIdentifier = "menuIdentifier"
}

struct segueID {
    static let tabBarIdentifier = "tabBarIdentifier"
    static let forgotPasswordIdentifier =  "forgotPasswordIdentifier"
    static let createTaskIdentifier =  "createTaskIdentifier"
    static let popUpViewIdentifier =  "popUpViewIdentifier"
    static let teamListVCIdentifier =  "teamListVCIdentifier"
    static let tasksVCIdentifier =  "tasksVCIdentifier"
    static let TeamVC =  "TeamVC"
    
    
    
    static let dismissPopIdentifierSegue =  "dismissPopIdentifierSegue"
    static let loginIdentifierSegue =  "loginIdentifierSegue"
    static let calendarVCIdentifierSegue =  "calendarVCIdentifierSegue"
    static let kyDrawerIdentifierSegue =  "kyDrawerIdentifierSegue"
    
}
