
//
//  HomeTabBarVC.swift
//

import UIKit
import MMDrawerController

class TabBarVC: UITabBarController,UITabBarControllerDelegate {
    
     var tabBarItems = UITabBarItem()
    
    override func viewDidLoad() {
         NotificationCenter.default.addObserver(self, selector: #selector(TabIndex), name: NSNotification.Name( TabbarIndexChange), object: nil)
        
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSAttributedString.Key.font: SystemFont(FontSize: 10),
             NSAttributedString.Key.foregroundColor: UIColor.lightGray],
            for: UIControl.State())
        
        UITabBarItem.appearance().setTitleTextAttributes(
            [NSAttributedString.Key.font: SystemFont(FontSize: 10),
             NSAttributedString.Key.foregroundColor: ColorValue(Rvalue: 54, Gvalue: 143, Bvalue: 242, alphavalue: 0.7)],
            for:.selected)
        
        UITabBar.appearance().tintColor = ColorValue(Rvalue: 54, Gvalue: 143, Bvalue: 242, alphavalue: 0.9)
        UITabBar.appearance().barTintColor = UIColor.white
        
        self.tabBarController?.navigationController?.navigationBar.isHidden = true
      //  navigationItem.leftBarButtonItem = btSlider
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(true)
        self.delegate=self
        
        
        guard let tabOne = HomeViewController.instance() else{return}
        
            let nav1 = UINavigationController(rootViewController: tabOne)
        
        let tabOneBarItem = UITabBarItem(title: "HOME".languageSet, image: UIImage.ionicon(with: .androidHome, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), selectedImage: UIImage.ionicon(with: .androidHome, textColor: UIColor.blue, size: CGSize(width: 30, height: 30)))
         nav1.tabBarItem = tabOneBarItem
      //  let tabOneNavController = UINavigationController(rootViewController: tabOne)

        // Create Tab two
        guard let tabTwo = MessageVC.instance() else{return}
        let nav2 = UINavigationController(rootViewController: tabTwo)
        let tabTwoBarItem2 = UITabBarItem(title: "MESSAGE".languageSet, image: UIImage.ionicon(with: .iosEmail, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), selectedImage: UIImage.ionicon(with: .iosEmail, textColor: UIColor.blue, size: CGSize(width: 30, height: 30)))
        
        nav2.tabBarItem = tabTwoBarItem2
        
     //    let tabTwoNavController = UINavigationController(rootViewController: tabTwo)
        
         // Create Tab three
        
        guard let tabThree = ContactsVC.instance() else{return}
        let nav3 = UINavigationController(rootViewController: tabThree)
        let tabThreeBarItem2 = UITabBarItem(title: "Contacts".languageSet, image: UIImage.ionicon(with: .androidPerson, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), selectedImage: UIImage.ionicon(with: .androidPerson, textColor: UIColor.blue, size: CGSize(width: 30, height: 30)))
        nav3.tabBarItem = tabThreeBarItem2
        
      //   let tabThreeNavController = UINavigationController(rootViewController: tabThree)
        
        // Create Tab four
        
        guard let tabFour = ProductListingVC.instance() else{return}
        let nav4 = UINavigationController(rootViewController: tabFour)
        let tabFourBarItem2 = UITabBarItem(title: "PRODUCT".languageSet, image: UIImage.ionicon(with: .iosBox, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), selectedImage: UIImage.ionicon(with: .iosBox, textColor: UIColor.blue, size: CGSize(width: 30, height: 30)))
        nav4.tabBarItem = tabFourBarItem2
        
     //   let tabFourNavController = UINavigationController(rootViewController: tabFour)
        // Create Tab four
        
        guard let tabFive = CheckinVC.instance() else{return}
        let nav5 = UINavigationController(rootViewController: tabFive)
        let tabFiveBarItem2 = UITabBarItem(title: "CHECK IN".languageSet, image: UIImage.ionicon(with: .iosTimeOutline, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), selectedImage: UIImage.ionicon(with: .iosClock, textColor: UIColor.blue, size: CGSize(width: 30, height: 30)))
        nav5.tabBarItem = tabFiveBarItem2
      //  let tabFiveNavController = UINavigationController(rootViewController: tabFive)

        
        self.tabBarController?.selectedIndex  = indexvalue
        
        self.viewControllers = [nav1,nav2,nav3,nav4,nav5]
    }
    
    
    @objc func TabIndex (notfication: NSNotification)  {
    
        
        self.selectedIndex = notfication.object as! Int
        indexvalue = self.selectedIndex

        print(self.selectedIndex)
    
}
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print(selectedIndex)
        if selectedIndex == 2
        {
            
        }
        
    }
    
    @IBAction func clickedOpen(_ sender: Any) {
        // Access an instance of AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
        //elDrawer?.setDrawerState(.opened, animated: true)
        
    }
    
    
}

extension TabBarVC
    {
        class func instance()-> TabBarVC?{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as? TabBarVC
            return controller
        }
    }

