
import UIKit
import MapKit
import SwiftyJSON
import GoogleMaps
import IoniconsKit
import JJFloatingActionButton

class PlanMyDay_Visit_offline: InterfaceExtendedController {
    
    // storyBoard Outlets
    
    @IBOutlet fileprivate var tableView : UITableView?
    
    fileprivate var marker = [CustomMarker]()
    var parama = [String:Any]()
    var locationManager: CLLocationManager!
    fileprivate var placeData : [JSON]?
    fileprivate var tableData = [JSON]()
    fileprivate var isFlipped: Bool = true
    
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var category_name: String = ""
    var primary_speciality: String = ""
    var Radius: Double = 0.0
    var lattitude: Double = 0.0
    var longtitude: Double = 0.0

    var jsonArray = [JSON]()
    
    func deg2rad(deg:Double) -> Double {
        return deg * M_PI / 180
    }
    
    func rad2deg(rad:Double) -> Double
    {
        return rad * 180.0 / M_PI
    }
    
    func distance(lat1:Double, lon1:Double, lat2:Double, lon2:Double, unit:String) -> Double
    {
        let theta = lon1 - lon2
        var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
        dist = acos(dist)
        dist = rad2deg(rad: dist)
        dist = dist * 60 * 1.1515
        dist = dist * 1609.34
        return dist
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView?.delegate = self
        tableView?.dataSource  = self
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 150
        tableView?.separatorStyle = .none
        tableView?.allowsMultipleSelection = false
        
        print("category_name :----->",category_name)
        print("primary_speciality :----->",primary_speciality)
        print("Radius :----->",Radius)
        print("lattitude :----->",lattitude)
        print("longtitude :----->",longtitude)
        
        let string = primary_speciality
        var array = string.components(separatedBy: ",")
        print(array) // returns ["1", "2", "3"]
        
        array = array.filter({ $0 != ""})

        if !parama.isEmpty
        {
            if array.count == 0
            {
                self.FetchingOfflineData(speciality: "")
            }
            else
            {
                 for index in 0..<(array.count) {
                self.FetchingOfflineData(speciality: array[0])
                 }
                
                if self.jsonArray != nil
                {
                                
                    self.tableData = self.jsonArray
                    self.tableView?.reloadData()
                }
            }
        }
    }
    
    fileprivate func FetchingOfflineData(speciality:String)
    {
        DataBaseHelper.ShareInstance.FetchFilterContactRequest(categoryName:category_name,primaryspeciality:speciality) { (data) in
            
            if let tempData = data
            {
                for jsonData in tempData
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            
                            let CheckLetValue = json!["lattitude"].stringValue
                            
                            print("CheckLetValue",CheckLetValue)
                            
                            if !CheckLetValue.isEmpty {
                                
                                if (json?["lattitude"])!.isEmpty && (json?["longitude"])!.isEmpty && (json?["lattitude"])! != "" && (json?["longitude"])! != ""{
                                    
                                    
                                    print("lattitude1 ::::",json!["lattitude"])
                                    print("longitude1 ::::",json!["longitude"])
                                    
                                    let lattitude2 = Double(json!["lattitude"].stringValue)
                                    let longitude2 = Double(json!["longitude"].stringValue)
                                    
                                    print("lattitude2 ::::",lattitude2 as Any)
                                    print("longitude2 ::::",longitude2 as Any)
                                    
                                    let value    =  self.distance(lat1:self.lattitude, lon1:self.longtitude, lat2:lattitude2!, lon2:longitude2!, unit: "K")
                                                                    
                                    print("Meter Value :-------> ",value)
                                    print("Radius Value :-------> ",self.Radius)
                                    
                                    
                                    if( value < self.Radius ) {
                                        
                                        self.jsonArray.append(json!)
                                        
                                        
                                        print("With in Range")
                                        
                                    }
                                    else{
                                        
                                        
                                        print("Out of Range")
                                        
                                    }
                                                                    
                                    
                                } else {
                                    
                                    
                                    
                                    print("No lattitude/longitude")
                                    
                                }
                            }
                            else {
                                print("Failure")
                            }
                       
                        })})
                    }
                }
                print(self.jsonArray)
                
                // if self.primary_speciality == ""
                // {
                self.tableData = self.jsonArray
                
                self.tableView?.reloadData()
                // }
                
            }
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("parama",parama)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: PlanNAVBAR(), BtnColor: UIColor.white)
        
    }
        
    @IBAction fileprivate func Home_Action(_ sender : UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction fileprivate func PlanMyDay_Action(_ sender : UIButton)
    {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Plan My Day")
    }
    
    fileprivate func CurrentDate() -> String
    {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        let CurrentDate = formatter.string(from: date)
        
        return CurrentDate
    }
}
//MARK: - TableView Controller

extension PlanMyDay_Visit_offline : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let json = tableData[indexPath.row]
        
        if json["contacts"].count == 0
        {
            self.popupAlertWithSheet(title: "\(json["salutation"].stringValue) \(json["contact_name"].stringValue)", message: "\(json["address1"].stringValue),\(json["address2"].stringValue) ,\(json["city"].stringValue),\(json["state_name"].stringValue),\(json["country_name"].stringValue)", actionTitles: [ self.languageKey(key: "Create Visit")], actions:[{action1 in
                                
                self.ContactArray = ["id":json["id"].stringValue,"contact_name":json["contact_name"].stringValue,"salutation":json["salutation"].stringValue,"category_name":json["category_name"].stringValue,"temp_id" : json["temp_id"].intValue]
                
                print(self.ContactArray)
                
                self.DummyArray.add(self.ContactArray)
                                
                let jsonData = try? JSONSerialization.data(withJSONObject: self.DummyArray, options: [])
                let jsonString = String(data: jsonData!, encoding: .utf8)
                print(jsonString!)
                
                var datavalues = [String : Any]()
                
                datavalues["visit_date"] = self.CurrentDate()
                
                datavalues["description"] = ""
                datavalues["visit_contact"] = jsonString
                datavalues["is_express"] = true
                datavalues["filepath"] = []
                datavalues["AttachmentFiles"] = []
                datavalues["pending_count"] = self.DummyArray.count
                datavalues["status"] = 1
                print(datavalues)
                
                datavalues["state_id"] = ""
                datavalues["state_name"] = ""
                
                datavalues["country_id"] = ""
                datavalues["country_name"] = ""
                
                datavalues["zipcode"] = ""
                datavalues["city"] = ""
                datavalues["address2"] = ""
                datavalues["address1"] = ""
                datavalues["title"] = json["contact_name"].stringValue
                datavalues["visit_type_id"] = ""
                datavalues["visit_type"] = ""
                 datavalues["created_at"] = Int(Date().timeIntervalSince1970)
                
                self.OfflineCreate(param: datavalues)
                
                },nil])
            
        }
        
    }
    
    fileprivate func OfflineCreate(param : [String : Any])
    {
        let userdefault = UserDefaults.standard
        var id : Int = 50000
        if userdefault.value(forKey: "OfflineNewVisitCreate") as? Int != nil
        {
            let tempid = userdefault.value(forKey: "OfflineNewVisitCreate") as! Int
            
            id = tempid + 1
            
        }
        
        userdefault.set(id, forKey: "OfflineNewVisitCreate")
        userdefault.synchronize()
        var parama  = [String:Any]()
        parama = param
        
        parama["id"] = id
        let dict = JSON(parama)
        
        DataBaseHelper.ShareInstance.AddVisit(VisitData:dict){status in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert("something went wrong".languageSet)
                return
            }
            
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: "Visit Data has been inserted successfully".languageSet, actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                                
                }, nil])
            
        }
        print(param)
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? PlaceListCell else{return PlaceListCell()}
        cell.selectionStyle = .none
        let json = self.tableData[indexPath.row]
        cell.lblTitleName?.text = "\(json["salutation"].stringValue) \(json["contact_name"].stringValue)   \(json["category_name"].stringValue)"
        
        cell.lblCategoryName?.text = "\(json["address1"].stringValue),\(json["address2"].stringValue) ,\(json["city"].stringValue),\(json["state_name"].stringValue),\(json["country_name"].stringValue)"
        cell.viewShadow?.bottomViewShadow(ColorName: .gray)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension PlanMyDay_Visit_offline
{
    class func instance()->PlanMyDay_Visit_offline?{
        let storyboard = UIStoryboard(name: "OfflineStoryBoard", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "PlanMyDay_Visit_offline") as? PlanMyDay_Visit_offline
        
        return controller
    }
}
