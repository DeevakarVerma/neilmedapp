//
//  DataFetchWithParam_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 12/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataFetchWithParam_Api: NSObject {
    public func  Request( parameter : [String:Any]?,link: String, completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        var param = [String : Any]()
        param = parameter ?? [String:Any]()
        
        print(param)
        
        let url = AppConnectionConfig.webServiceURL  + link
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage,nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            guard let rawinfoData = rawInfo["data"].array
                else
            {
                completion(true,message ,nil)
                return
            }
            completion(true, message, rawinfoData)
            return
        }
        else
        {
            completion(false,message ,nil)
            return
        }
        
        
    }
    
}

