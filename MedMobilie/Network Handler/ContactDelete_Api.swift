//
//  OrderDelete_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 14/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class ContactDelete_Api : NSObject{
    
    public func  Request( idvalue : Int, url_string : String,completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        var param = [String : Any]()
        
        param["id"] = idvalue
        
        
        let url = AppConnectionConfig.webServiceURL  + url_string//"/contact_delete"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
//        guard let rawinfoData = rawInfo["data"].array
//            else
//        {
//            //completion(false,message,nil)
//            return
//        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message,nil )
            return
        }
        else
        {
            completion(false,message ,nil)
            return
        }
        
        
    }
    
}

