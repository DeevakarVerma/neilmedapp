//
//  IssueSample_Api.swift
//  MedMobilie
//
//  Created by dr.mac on 28/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON
class IssueSample_Api : NSObject
{
    public func  Order( contact_id : Int,Visit_id : Int, Product_id : [Int],Qty: [Int],completion : @escaping ((_ success : Bool,_ message : String ,_ data : [JSON]? )->())){
        
        var param = [String : Any]()
        
        param["contact_id"] = contact_id
        param["visit_id"] = Visit_id
        param["product_id"] = Product_id
        param["product_quantity"] = Qty
        
        print(param)
    
        
        let url = AppConnectionConfig.webServiceURL  + "/issuesample"
        
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            self.handleResponse(withSuccess: success, response: response, completion: completion)
        }
    }
    
    private func handleResponse(withSuccess success : Bool, response : JSON?, completion : @escaping ((_ success : Bool,_ message : String,_ data : [JSON]? )->())){
        
        //Log.echo(key: "identifier", text: "raw infos ==>  \(String(describing: response))")
        
        let errorMessage = "errorMessage";
        
        guard let rawInfo = response
            else{
                completion(false, errorMessage, nil)
                return
        }
        let successValue = rawInfo["status"].stringValue
        let message = rawInfo["message"].stringValue
        if (successValue == "fail")
        {
            
            InterfaceExtendedController().sessionExpired()
            
        }
        
        //        guard let rawinfoData = rawInfo["data"].array
        //            else
        //        {
        //            //completion(false,message,nil)
        //            return
        //        }
        
        /* raw
         let lang = rawinfoData["language"]?.stringValue*/
        if(successValue == "true"){
            completion(true, message,nil )
            return
        }
        else
        {
            completion(false,message ,nil)
            return
        }
        
        
    }
    
}
