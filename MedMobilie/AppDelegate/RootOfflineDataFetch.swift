//
//  RootOfflineDataFetch.swift
//  MedMobilie
//
//  Created by dr.mac on 01/05/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import SwiftyJSON

class RootOfflineDataFetch : NSObject
{
    var totalContacts = 0
    var offset = 0
    var count_Save = 0
    
    let disPatchGroup   = DispatchGroup()
    
    
        func FetchingAll()
        {
            //   self.FetchContact()
           self.FetchProduct()
          self.FetchSample()
                self.Listsapidata()
         self.CategoryListFetch()
            
        }
    
        func FetchingWhenLangChange()
        {
            self.FetchProduct()
            self.FetchSample()
            self.Listsapidata()
            self.CategoryListFetch()
        }
        
    func contactFetch()
    {
        DispatchQueue.main.async {
            AppDelegate.showWaitView()
        }
         self.dataFetch(offset: 0)
    }
    
    func dataFetch(offset : Int)
    {
        var param = [String : Any]()
        param["offset"] = offset
        let url = AppConnectionConfig.webServiceURL + "/contactsinchunks"
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            
            if success
            {
                if let json = response
                {
                    
                    self.GetResponse(json)
                  //  AppDelegate.hideWaitView()
                    
                }
            }
            else
            {
                DispatchQueue.main.async {
                    AppDelegate.hideWaitView()
                }
            }
            
        }
    }
    
    fileprivate func GetResponse(_ response : JSON)
    {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    AppDelegate.hideWaitView()
        }
        
        let userdefault = UserDefaults.standard
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int == nil
        {
            if !NetworkState.isConnected() {
                AppDelegate.hideWaitView()
                AppDelegate.alertViewForInterNet()
                return
            }
            
            self.offset = response["offset"].intValue
            if response["total"].intValue > 0
            {
                
                DispatchQueue.global(qos: .background).async {
                                    
                     self.disPatchGroup.enter()
                    if let jsonData = response["data"].array
                    {
                        
                        DispatchQueue.main.async {
                            
                            jsonData.forEach({
                                
                                DataBaseHelper.ShareInstance.AddContact(Object: $0)
                               // self.count_Save = self.count_Save + 1
                               // print("Count-- ",self.count_Save)
                            })
                        }
                        
                        self.disPatchGroup.leave()
                        
                    }
                    if response["total"].intValue > self.offset
                    {
                        self.dataFetch(offset: self.offset)
                    }
                    if response["total"].intValue == response["offset"].intValue
                    {
                        DispatchQueue.main.async {
                            AppDelegate.hideWaitView()
                            //self.dismiss(animated: true, completion: nil)
                        }
                        return
                    }
                }
            }
            else
            {
                DispatchQueue.main.async {
                    AppDelegate.hideWaitView()
                }
                
                return
            }
        }
    }
    
    
  
    
    
    /*fileprivate func FetchContact()
    {
        let userdefault = UserDefaults.standard
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int == nil
        {
        if !NetworkState.isConnected() {
            AppDelegate.alertViewForInterNet()
            return
        }
          
         DispatchQueue.global(qos: .background).async {
        ContactListFetch().Fetch { (status, message, data) in
            
            DispatchQueue.main.async {
               //
            }
            if !status
            {
                return
            }
            
                DataBaseHelper.ShareInstance.DeleteContext(ClassName : OfflineDataClass.Contact.rawValue)
                if let jsonData = data
                {
                    jsonData.forEach({
                        DataBaseHelper.ShareInstance.AddContact(Object: $0)
                    })
                    
            }
            }
            
        }
           
        }
    }*/
    
    fileprivate func FetchProduct()
    {
        if !NetworkState.isConnected() {
            return
        }
        
       
        DispatchQueue.global(qos: .background).async {
             ProductListingFetch().Fetch(ProductType: "product") { (status, message, data) in
                
                if !status
                {
                    return
                }
                if let tempData = data
                {
                DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.Product.rawValue)
                DataBaseHelper.ShareInstance.AddProduct(Object: tempData ){status in
                }
                }
            }
            
        }
    }
    
    
    fileprivate func FetchSample()
    {
        if !NetworkState.isConnected() {
            return
        }
        DispatchQueue.global(qos: .background).async {
            ProductListingFetch().Fetch(ProductType: "sample") { (status, message, data) in
                
                if !status
                {
                    return
                }
                if let tempData = data
                {
                    DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.Sample.rawValue)
                    DataBaseHelper.ShareInstance.AddSampleProduct(Object: tempData ){status in
                    }
                }
                
            }
            
        }
    }
    
    
    
    
    fileprivate func Listsapidata()
    {
       //
        if !NetworkState.isConnected() {
         //
            AppDelegate.alertViewForInterNet()
            return
        }
        DispatchQueue.global(qos: .background).async {
            
            
            Listsapi().Request { (status, message, data) in
                
                if !status
                {
                    return
                }
                guard let tempdata = data else
                {
                    return
                }
                DataBaseHelper.ShareInstance.DeleteContext(ClassName : OfflineDataClass.ListingData.rawValue)
                DataBaseHelper.ShareInstance.AddListing(object: tempdata)
                
            }
        }
    }
    
    fileprivate func CategoryListFetch()
    {
        if !NetworkState.isConnected() {
            
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            
            DataFetch_Api().Fetch(ID: 0 ,urlString: "/planmydaylists") { (status, message,data) in
                if !status
                {
                    return
                }
                
                guard let tempdata = data else
                {
                    return
                }
                DataBaseHelper.ShareInstance.DeleteContext(ClassName : "CategoryList")
                DataBaseHelper.ShareInstance.CategoryList(object: tempdata)
    
                
            }
        }
    }
    
}
