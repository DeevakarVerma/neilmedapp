//
//  DataSyncVC.swift
//  MedMobilie
//
//  Created by dr.mac on 17/07/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON



class DataSyncVC: InterfaceExtendedController {

var totalContacts = 0
var offset = 0
     let operationQueue1 = OperationQueue.init()
    override func viewDidLoad() {
        super.viewDidLoad()
      //  fatalError()
        AppDelegate.showWaitView()
       
        
        self.dataFetch(offset: 0)
    // oProgressView.progressLabel.font = UIFont.systemFont(ofSize: 20)

    }
    
    func dataFetch(offset : Int)
    {
        var param = [String : Any]()
        param["offset"] = offset
        let url = AppConnectionConfig.webServiceURL + "/contactsinchunks"
        ServerProcessor().request(.post, url, parameters: param, encoding: .httpBody, headers: nil, authorize: true) { (success, response) in
            
            if success{
                if let json = response
                {
                    
                     self.GetResponse(json)
                }
            }
            
        }
    }
    
    fileprivate func GetResponse(_ response : JSON)
    {
        let userdefault = UserDefaults.standard
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int == nil
        {
            if !NetworkState.isConnected() {
                AppDelegate.alertViewForInterNet()
                return
            }
            
            self.offset = response["offset"].intValue
            if response["total"].intValue > 0 && response["total"].intValue >= offset
            {
                self.dataFetch(offset: self.offset)
                DispatchQueue.global(qos: .background).async {
                   
//                    DataBaseHelper.ShareInstance.DeleteContext(ClassName : OfflineDataClass.Contact.rawValue)
                    
                    if let jsonData = response["data"].array
                    {
                        jsonData.forEach({
                            DataBaseHelper.ShareInstance.AddContact(Object: $0)
                        })
                        
                    }
                    
                    
                    
                }
            }
            else
            {
             //   operationQueue1.cancelAllOperations()
                DispatchQueue.main.async {
                    AppDelegate.hideWaitView()
//self.dismiss(animated: true, completion: nil)
                }
                return
            }
        }
    }
}
extension DataSyncVC
{
    class func instance() -> DataSyncVC?
    {
            let storyBoard = UIStoryboard.init(name: "OfflineStoryBoard", bundle: nil)
            let controller = storyBoard.instantiateViewController(withIdentifier: "DataSyncVC") as? DataSyncVC
        return controller
    }
}
