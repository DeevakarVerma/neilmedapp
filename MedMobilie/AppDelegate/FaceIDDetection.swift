//
//  FaceIDDetection.swift
//  MedMobilie
//
//  Created by dr.mac on 17/05/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import LocalAuthentication

class FaceIDDetection : NSObject
{
 func SetRootAfterAuth()
{
    let useInfo = SignedUserInfo.sharedInstance
    let ud = UserDefaults.standard
    if useInfo != nil
    {
        if ud.value(forKey:"FaceIdEnable") as? Bool == true
        {
            if LAContext().biometricType.rawValue == "faceID"
            {
//                RootControllerManager().authenticationWithTouchID()
            }
            else
            {
                RootControllerManager().setRoot()
                ud.set(false, forKey: "FaceIdEnable")
                ud.synchronize()
            }
            
        }
        else
        {
            RootControllerManager().setRoot()
        }
    }
    else if ud.value(forKey: "LoginEmailId") as? String != "" &&  ud.value(forKey: "LoginPassword") as? String != ""
    {
        if ud.value(forKey:"FaceIdEnable") as? Bool == true
        {
            if LAContext().biometricType.rawValue == "faceID"
            {
//                RootControllerManager().authenticationWithTouchID()
            }
            else
            {
                RootControllerManager().setRoot()
                ud.set(false, forKey: "FaceIdEnable")
                ud.synchronize()
            }
            
        }
        else
        {
            RootControllerManager().setRoot()
        }
        
        
        
        
        
    }
    else
    {
        RootControllerManager().setRoot()
    }
    
    }
}
