
import Foundation
import CoreData


extension OrderList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OrderList> {
        return NSFetchRequest<OrderList>(entityName: "OrderList")
    }

    @NSManaged public var id: [Int]
    @NSManaged public var json: [Data]
    
    


}
