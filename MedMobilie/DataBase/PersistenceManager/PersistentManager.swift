

import Foundation
import CoreData

final class PersistentManager
{
    
    private init(){}
   static  let  Share = PersistentManager()

// MARK: - Core Data stack

lazy var persistentContainer: NSPersistentContainer = {
    
    let container = NSPersistentContainer(name: "MedMobileDB")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        if let error = error as NSError? {
            
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    })
    return container
}()
    
//    lazy var viewContext: NSManagedObjectContext = {
//        return self.persistentContainer.viewContext
//    }()
//
//    lazy var cacheContext: NSManagedObjectContext = {
//        return self.persistentContainer.newBackgroundContext()
//    }()
//
//    lazy var updateContext: NSManagedObjectContext = {
//        let _updateContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
//        _updateContext.parent = self.viewContext
//        return _updateContext
//    }()

// MARK: - Core Data Saving support

func saveContext () {
    let context = persistentContainer.viewContext
    if context.hasChanges {
        do {
            try context.save()
        } catch {
            
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
}


}
