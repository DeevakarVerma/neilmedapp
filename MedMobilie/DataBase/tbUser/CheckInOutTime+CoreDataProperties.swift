
import Foundation
import CoreData


extension CheckInOutTime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CheckInOutTime> {
        return NSFetchRequest<CheckInOutTime>(entityName: "CheckInOutTime")
    }

    @NSManaged public var id: String?
    @NSManaged public var checkIn: String?
    @NSManaged public var checkout: String?
    @NSManaged public var date: String?

}
