
import Foundation
import CoreData


extension CheckinTime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CheckinTime> {
        return NSFetchRequest<CheckinTime>(entityName: "CheckinTime")
    }

    @NSManaged public var date: String?
    @NSManaged public var user_id: String?
    @NSManaged public var lat: String?
    @NSManaged public var lng: String?
    @NSManaged public var time: String?

}
