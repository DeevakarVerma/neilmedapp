
import Foundation
import CoreData


extension VisitList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VisitList> {
        
        return NSFetchRequest<VisitList>(entityName: "VisitList")
        
    }

    @NSManaged public var id: [Int]
    @NSManaged public var json: [Data]

}
