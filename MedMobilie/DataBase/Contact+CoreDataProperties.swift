
import Foundation
import CoreData
import SwiftyJSON



extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var id: String
    @NSManaged public var json : Data
    @NSManaged public var primaryspecialtiy : String
    @NSManaged public var secondaryspecialtiy : String
    @NSManaged public var categoryname : String
    @NSManaged public var new : Bool
    @NSManaged public var Contact_name : String
    @NSManaged public var phone : String
    @NSManaged public var salutation : String
    @NSManaged public var email : String
}
