//
//  NetworkState.swift
//  MedMobilie
//
//  Created by dr.mac on 14/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import Alamofire

class NetworkState {
    
    class func isConnected() ->Bool {
        
       var reachability = Reachability()!
        if reachability.connection == .none {
            return false
        }
        else
        {
            return true
        }
        
//        let manager = NetworkReachabilityManager(host: "https://www.google.com")
//        return manager?.isReachable ?? false
    }
}
