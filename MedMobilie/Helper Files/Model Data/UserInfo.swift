


import Foundation
import SwiftyJSON



@objc class UserInfo: NSObject {
    
    
    public var user_id : String?
    public var username : String?
    public var userImg : String?
    public var email : String?
   // public var access_token : String?
    public var language : String?
    public var gender : String?
    
    public var currency_symbol : String?
    public var currency_code : String?
    public var country_code : String?
//    public var zip_code : String?
//    public var fax : String?
//    public var country_id : Int?
//    public var country_name : String?
//    public var state_name : String?
//    public var state_id : String?
//    public var erp_id : String?
    public var phone : String?
//    public var image : String?
//    public var gender : String?
    
    
    override init(){
        super.init()
    }
    
    init(userInfoJSON : [String:JSON]?){
        super.init()
        fillInfo(info: userInfoJSON)
    }
    
    func fillInfo(info : [String:JSON]?){
        
        guard let info = info
            else{
                return
        }
        
       
       
        user_id = info["id"]?.stringValue
        username = info["name"]?.stringValue
        userImg = info["image"]?.stringValue
        email = info["email"]?.stringValue
        phone = info["phone"]?.stringValue
       // access_token = info["access_token"]?.stringValue
        language = info["language"]?.stringValue
        gender = info["gender"]?.stringValue
        currency_symbol = info["currency_symbol"]?.stringValue
        currency_code = info["currency_code"]?.stringValue
        country_code = info["country_code"]?.stringValue
//        zip_code = info["zip_code"]?.stringValue
//        fax = info["fax"]?.stringValue
//        country_id = info["country_id"]?.intValue
//        country_name = info["country_name"]?.stringValue
//        state_name = info["state_name"]?.stringValue
//        state_id = info["state_id"]?.stringValue
//        erp_id = info["erp_id"]?.stringValue
//        phone = info["phone"]?.stringValue
//        image = info["image"]?.stringValue
//        gender = info["gender"]?.stringValue
        
        
    }    
}

