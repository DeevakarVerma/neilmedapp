//
//  UIButton.swift
//  MedMobilie
//
//  Created by dr.mac on 12/04/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import UIKit

extension UIButton
{
    @IBInspectable var localizedTitle: String {
        get { return "" }
        set {
            self.setTitle(newValue.languageSet, for: .normal)
        }
    }
}
