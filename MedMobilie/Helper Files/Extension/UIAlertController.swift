//
//  UIAlertViewController.swift
//  MedMobilie
//
//  Created by dr.mac on 14/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import Foundation
import UIKit
extension UIAlertController
{
    func AlertView(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}
