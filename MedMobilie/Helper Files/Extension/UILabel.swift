//
//  UILabel.swift
//  M1 Pay
//
//  Created by Harsh garg on 21/01/18.
//  Copyright © 2016 harsh garg. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel{
    
        
        func makeOutLine(oulineColor: UIColor, foregroundColor: UIColor){
            
            let strokeTextAttributes = [
                NSAttributedString.Key.strokeColor : oulineColor,
                NSAttributedString.Key.foregroundColor : foregroundColor,
            NSAttributedString.Key.strokeWidth : -3.0,
            NSAttributedString.Key.font : self.font
                ] as [NSAttributedString.Key : Any]
            self.attributedText = NSMutableAttributedString(string: self.text ?? "", attributes: strokeTextAttributes)
        }
    
    @IBInspectable var iPhone5:CGFloat{
        get {
            return 0
        }
        set {
            if UIScreen.main.isPhone5
            {
                let currentFontName = self.font.fontName
                var calculatedFont: UIFont?
                calculatedFont = UIFont(name: currentFontName, size: newValue)
                self.font = calculatedFont
            }
            
        }
    }
    
    @IBInspectable var localizedTitle: String {
        get { return "" }
        set {
            self.text = newValue.languageSet
        }
    }

        
        func underline() {
            if let textString = self.text {
                let attributedString = NSMutableAttributedString(string: textString)
                
                attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length))
                attributedText = attributedString
            }
        }
    }



