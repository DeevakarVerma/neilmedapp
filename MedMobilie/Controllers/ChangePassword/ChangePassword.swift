//
//  ChangePassword.swift
//  MedMobilie
//
//  Created by MAC on 12/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class ChangePassword: InterfaceExtendedController {
    
    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txtOldPassword.Padding()
        txtOldPassword.textFieldBottomBorder()
        txtOldPassword.backgroundColor = UIColor.clear
        txtOldPassword.delegate = self
        
        txtNewPassword.Padding()
        txtNewPassword.textFieldBottomBorder()
        txtNewPassword.backgroundColor = UIColor.clear
        txtNewPassword.delegate = self
        
        txtConfirmPassword.Padding()
        txtConfirmPassword.textFieldBottomBorder()
        txtConfirmPassword.backgroundColor = UIColor.clear
        txtConfirmPassword.delegate = self

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)

       
    }
    
    // MARK: - Language Setting
 @objc override func LanguageSet(){
    
    NavigationBarTitleName(Title: "Change Password")
    
 
    lblOldPassword.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Enter Old Password", comment: "")
    
    lblNewPassword.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Enter New Password", comment: "")
    
    lblConfirmPassword.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Confirm New Password", comment: "")
    
    btnDone.setTitle(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Submit", comment: ""), for: .normal)
    
    
    }
    
    // MARK: - Done Button Action
    
    @IBAction func DoneAction(_ sender: Any) {
        
        guard let NewPassword = txtNewPassword.text,let ConfirmPassword = txtConfirmPassword.text else{return}
        
        if txtOldPassword.text == ""
        {
            
            self.alert(languageKey(key:"Please enter your Old Password"))
            return
        }
        else if NewPassword.containsWhitespace
        {
            
            self.alert(languageKey(key:"Your Password can't contains Blank Space!"))
            return
        }
        else if NewPassword.count < 5
        {
            self.alert(languageKey(key:"The Password must be at least 5 Characters long."))
            return
        }
            
        else if NewPassword != ConfirmPassword
        {
            
            self.alert(languageKey(key:"New password and confirmation password not match"))
            return
        }
        
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        ChangePassword_Api().request(txtOldPassword.text, txtNewPassword.text){ (status,message) in
            
            AppDelegate.hideWaitView()
            if !status{
                self.alert(message!)
                return
            }
            self.alert(self.languageKey(key:"Your password Change succesfully"))
            self.txtOldPassword.text = ""
            self.txtNewPassword.text = ""
            self.txtConfirmPassword.text = ""
            
            
        }
        
        
        
        
        
        
    }
    
   
   
}

// MARK: - TextField Delegate

extension ChangePassword : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}

