
import UIKit
import SwiftyJSON
import DGElasticPullToRefresh

class Reports: InterfaceExtendedController {
    
    fileprivate var arrItems_img : [String]?
    fileprivate var img = UIImage()
    fileprivate var BackGround = UIColor()
    fileprivate var JsonData : [JSON]?
    
    var CheckBool : Bool = false
    var datefrom : String = ""
    var dateto : String = ""
    var type : String = ""
   var numbers = ["Visit","Task","Bills","Orders","Contact","Check in Hours"]
    var SubMitReport = false
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrItems_img = ["Visit",
                        "Task",
                        "Bills",
                        "Orders",
                        "Contact","clock"]
        
        tableView.separatorStyle = .none
        
        type = "daily"
        
        AppDelegate.showWaitView()
        self.ReportDataFetch()
        NavBarFilter()
            
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: ReportNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Navigationbar Title Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Reports")
    }
    
     // MARK: - Set navigationbar Icons
    
    func NavBarFilter()
    {
        
        let Filter = UIBarButtonItem(image: UIImage.ionicon(with: .androidOptions, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(Filter_Action))
        Filter.tintColor = UIColor.white
        
        navigationItem.rightBarButtonItem = Filter
        
    }
    
    // MARK: - Filter Action

    @objc func Filter_Action(){
        
        self.popupAlertWithSheet(title: nil, message: nil, actionTitles: [ self.languageKey(key: "Daily"),self.languageKey(key: "Weekly"),self.languageKey(key: "Date range")], actions:[
            {action1 in
                
                
                self.type = "daily"
                
                AppDelegate.showWaitView()
                self.ReportDataFetch()
                
                
            },{action2 in
                
                self.type = "weekly"
                AppDelegate.showWaitView()
                self.ReportDataFetch()
                
                
            },{action3 in
                                
                guard let controller = FilterDateRange.instance()
                    else{return}
                controller.delegate = self
                controller.NavColor = ReportNAVBAR()

                self.navigationController?.pushViewController(controller, animated: true)
                
                
            }, nil])
    }
    
    // MARK: - Fetch Report Data
    
    @objc fileprivate func ReportDataFetch()
    {
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        var DataValue = [String : Any]()
        DataValue["start"] = datefrom
        DataValue["end"] = dateto
        DataValue["type"] = type
                
        VisitList_Api().fetchReport(param: DataValue) { (status, message, data) in
            
            AppDelegate.hideWaitView()
            
            if !status
            {
                
                //   self.JsonData?.removeAll()
                self.tableView.reloadData()
                
                self.tableView.dg_stopLoading()
                self.alert(message )
                return
            }
            if data != nil
            {
                
                self.JsonData = data ?? nil
                print(self.JsonData as Any)
                print(self.JsonData?.count)
                
                
                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
            }
        }
    }
    
    
}

// MARK: - Table View Delegate/DataSource Methods


extension Reports : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //print(self.JsonData?.count)
        
        if JsonData != nil
        {
            return numbers.count
        }
        
        
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        let json = JsonData?[0]
        CheckBool = false
                
        switch indexPath.row {
            
        case 0:
            guard let controller = DailyVisit.instance()
                else{return}
            controller.Titlename = "Visit"
            controller.reportKey = type
            controller.datefrom = datefrom
            controller.dateto = dateto
           
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
            
        case 1:
            
            guard let controller = TaskListVC.instance()
                else{return}
           // controller.Titlename = ""
            controller.reportKey = type
            controller.datefrom = datefrom
            controller.dateto = dateto
         
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
            
        case 2:
            
            guard let controller = BillsVc.instance()
                else{return}
            // controller.Titlename = ""
            controller.reportKey = type
            controller.datefrom = datefrom
            controller.dateto = dateto
          
            self.navigationController?.pushViewController(controller, animated: true)
           
            break
            
        case 3:
            
            guard let controller = OrderListVC.instance()
                else{return}
            // controller.Titlename = ""
            controller.reportKey = type
            controller.datefrom = datefrom
            controller.dateto = dateto
           
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
            
        case 4:
            
            guard let controller = ContactsVC.instance()
                else{return}
            // controller.Titlename = ""
            controller.reportKey = type
            controller.ComeFromReports = true
            controller.datefrom = datefrom
            controller.dateto = dateto
            
            self.navigationController?.pushViewController(controller, animated: true)
        
            break
            
        case 5:
            break
            
        default: break
            
        }
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? NotificationListingCell
            else{return NotificationListingCell()}
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        cell.lblTask?.text = numbers[indexPath.row].languageSet
        cell.imgiconNotification?.contentMode = .scaleAspectFit
        img =   UIImage(named: "\(arrItems_img![indexPath.row])")!
        
        
        guard let json = JsonData?[0]
            else{return cell}
        
        
        print(json)
        
        cell.imgiconNotification?.image = img
        
        switch indexPath.row {
            
        case 0:
            
            cell.lblTotal?.text = "Total".languageSet + " :"
            cell.lblPending?.text = "Pending".languageSet + " :"
            cell.lblClose?.text = "Completed".languageSet + " :"
            cell.lblTotal_value?.text = json["visit"]["all"].stringValue
            cell.lblPending_value?.text = json["visit"]["pending"].stringValue
            cell.lblClose_value?.text = json["visit"]["visited"].stringValue
            
            cell.viewShadow?.backgroundColor = VisitNAVBAR()
            break
            
        case 1:
            
            cell.lblTotal?.text = "Completed".languageSet + " :"
            cell.lblTotal_value?.text = json["task"]["completed"].stringValue
            
            
            cell.lblSpace1?.isHidden = true
            cell.lblPending?.text = ""
            cell.lblPending_value?.text = ""
            
            cell.lblSpace2?.isHidden = true
            cell.lblClose_value?.text = ""
            cell.lblClose?.text = ""
            
            cell.viewShadow?.backgroundColor = TaskNAVBAR()
            
            break
            
        case 2:
            
            cell.lblTotal?.text = "Pending".languageSet + " :"
            
            cell.lblTotal_value?.text = json["bill"]["submitted"].stringValue
            
            
            cell.lblPending?.text = "Paid".languageSet + " :"
            cell.lblPending_value?.text = json["bill"]["paid"].stringValue
            
            cell.lblClose?.text = "Rejected".languageSet + " :"
            
            cell.lblClose_value?.text = json["bill"]["rejected"].stringValue
            
            
            cell.viewShadow?.backgroundColor = BillNAVBAR()
            
            break
            
        case 3:
            
            
            cell.lblTotal?.text = "Pending".languageSet + " :"
            cell.lblTotal_value?.text = json["order"]["submitted"].stringValue
            
            cell.lblPending?.text = "Confirmed".languageSet + " :"
            cell.lblPending_value?.text = json["order"]["confirmed"].stringValue
            
            cell.lblClose?.text = "Paid".languageSet + " :"
            cell.lblClose_value?.text = json["order"]["paid"].stringValue
            
            cell.viewShadow?.backgroundColor = OrderNAVBAR()
            
            break
            
            
        case 4:
            
            cell.lblTotal?.text = "Total".languageSet + " :"
            cell.lblTotal_value?.text = json["contact"]["all"].stringValue
            
            cell.lblPending?.text = "New".languageSet + " :"
            cell.lblPending_value?.text = json["contact"]["new"].stringValue
                        
            cell.lblSpace2?.isHidden = true
            cell.lblClose_value?.text = ""
            cell.lblClose?.text = ""
            break
            
            
        case 5:
            cell.lblTotal?.text = "Total Hours".languageSet + " :"
            cell.lblTotal_value?.text = json["checkin"]["logged_hours"].stringValue
            cell.lblSpace1?.isHidden = true
            cell.lblPending?.text = ""
            cell.lblPending_value?.text = ""
            cell.lblSpace2?.isHidden = true
            cell.lblClose_value?.text = ""
            cell.lblClose?.text = ""
            cell.viewShadow?.backgroundColor = UIColor(red: 251.0/255.0, green: 155.0/255.0, blue: 36.0/255.0, alpha: 1.0)
            
            break
            
            
        default:
            cell.lblTotal_value?.text = "0"
            cell.lblPending_value?.text = "0"
            cell.lblClose_value?.text = "0"
        }
                
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    // set view for footer
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        footerView.backgroundColor = UIColor.clear
        
        let button:UIButton = UIButton(frame: CGRect(x: (footerView.frame.self.width-150)/2, y: 5, width: 150, height: 40))
        button.backgroundColor = VisitNAVBAR()
        button.setTitle("Report", for: .normal)
        button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
button.layer.cornerRadius = 3
        button.clipsToBounds = true
        footerView.addSubview(button)
        
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func buttonClicked() {
        print("Button Clicked")
        
        guard let controller = AddnotesVC.instance() else{return}
        controller.TitleString = "Report"
           controller.apiLink = "/submitreport"
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}

// MARK: - Pull To Refresh Methods

extension Reports
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self!.datefrom  = ""
            self!.dateto  = ""
            self!.type  = "daily"
            
            
            self?.ReportDataFetch()
            }, loadingView: loadingView)
        
        tableView.dg_setPullToRefreshFillColor(ReportNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
}

// MARK: - Class Instance


extension Reports
{
    class func instance()->Reports?{
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Reports") as? Reports
        
        return controller
    }
}
extension Reports : FilterProtocol
{
    func Filterdetails(FromDate : String , ToDate : String)
    {
        
        
        datefrom = FromDate
        dateto = ToDate
        
        print("datefrom :---> ",datefrom)
        print("dateto :---> ",dateto)
        
        AppDelegate.showWaitView()
        self.ReportDataFetch()
        
    }
}


