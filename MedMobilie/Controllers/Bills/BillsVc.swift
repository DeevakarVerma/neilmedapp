import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit

class BillsVc: InterfaceExtendedController {
   
    
    fileprivate var JsonData : [JSON]?
    
    var contact_ID : Int = 0
    var datefrom : String = ""
    var dateto : String = ""
    var type : String = ""
    var reportKey : String = ""

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    
    
    // MARK: - Class life cycle

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(BillListDataFetch), name: NSNotification.Name( "UpdateBillList"), object: nil)
        
        btnView.createCircleForView()
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        
        self.NavBarFilter()

        
        AppDelegate.showWaitView()
       BillListDataFetch()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: BillNAVBAR(), BtnColor: UIColor.white)
    }

    // MARK: - Set navigationbar Icons

    func NavBarFilter()
    {
        
        let Filter = UIBarButtonItem(image: UIImage.ionicon(with: .androidOptions, textColor: UIColor.red, size: CGSize(width: 30, height: 30)), style: .plain, target: self, action: #selector(Filter_Action))
        Filter.tintColor = UIColor.white
        
        navigationItem.rightBarButtonItem = Filter
    }
    // MARK: - Filter Action
    
    @objc func Filter_Action(){
        
        self.popupAlertWithSheet(title: nil, message: nil, actionTitles: [ self.languageKey(key: "Submitted"),self.languageKey(key: "Approved"),self.languageKey(key: "Rejected"),self.languageKey(key: "Paid"),self.languageKey(key: "Date range")], actions:[
            {action1 in
                
                
                self.type = "1"
                self.datefrom = ""
                self.dateto = ""
                
                AppDelegate.showWaitView()
                self.BillListDataFetch()
                
            },{action2 in
                
                self.type = "2"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.BillListDataFetch()
                
                
            },{action3 in
                
                self.type = "3"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.BillListDataFetch()
                
                
            },{action4 in
                
                self.type = "4"
                self.datefrom = ""
                self.dateto = ""
                AppDelegate.showWaitView()
                self.BillListDataFetch()
                
                
            },{action5 in
                
                guard let controller = FilterDateRange.instance()
                    else{return}
                controller.delegate = self
                controller.NavColor = BillNAVBAR()

                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
    }
    
    // MARK: - Navigationbar Title Language Setting
 
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Bills")
    }
    
    // MARK: - Navigate to Create Bill
    
    @IBAction func createNewVisit(_ sender: Any) {
        
        guard let controller = CreateBill.instance()
            else{return}
        controller.BackDataBlock = {infos in
            print(infos)
            self.addANDEditBill(link: infos)
        }
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func addANDEditBill(link : [JSON],newFlag : Bool = true)
    {
        if !newFlag
        {
            if let infos = self.JsonData
            {
                infos.enumerated().forEach({
                    if $0.element["id"].intValue == link[0]["id"].intValue
                    {
                        self.JsonData?[$0.offset] = link[0]
                    }
                })
            }
        }
        else
        {
            self.JsonData?.insert(link[0], at: 0)
        }
        
        self.tableView.reloadData()
    }
    
    // MARK: - Fetching Billing List

    @objc fileprivate func BillListDataFetch()
    {
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        var DataValue = [String : Any]()
        DataValue["datefrom"] = datefrom
        DataValue["dateto"] = dateto
        if reportKey != ""
        {
            DataValue["report_type"] = reportKey
        }
        else
        {
            DataValue["type"] = type
        }
            DataValue["URL"] = "/billapp_list"
            
        
        VisitList_Api().fetchData(param: DataValue) { (status, message, data) in
            
            AppDelegate.hideWaitView()
            
            if !status
            {
                AppDelegate.hideWaitView()
                self.JsonData?.removeAll()
                self.tableView.dg_stopLoading()
                 self.tableView.reloadData()
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                    
                    
                    }, nil])
                return
            }
            self.reportKey = ""
            self.JsonData = data
            self.tableView.reloadData()
            self.tableView.dg_stopLoading()
            
        }
        
    }
    
    // MARK: - Delete Bill From List
    
    fileprivate func DeleteBill(Taskid:Int , Task_delete : Bool )
    {
        
        if !NetworkState.isConnected() {
            tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        let urlstr : String = "/billapp_delete"
       
        ContactDelete_Api().Request(idvalue: Taskid ,url_string: urlstr ) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
              
            }
        }
    }
 
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
}

// MARK: - Table View Delegate/DataSource Methods

extension BillsVc : UITableViewDelegate , UITableViewDataSource , SwipeTableViewCellDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return JsonData?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       guard let list = self.JsonData?[indexPath.row] else{return }
        
        guard let controller = BillsDetails.instance()
            else{return}
        
        controller.BillId = list["id"].intValue
        
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BillsCell", for: indexPath) as? BillsCell
            else{return BillsCell()}
        
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        guard let json = JsonData?[indexPath.row],let symbol = SignedUserInfo.sharedInstance?.currency_symbol
            else{return cell}
        
        let temptext = "\(symbol) \(json["expense_amount"].stringValue)"
        
        cell.lblExpense_name?.text = json["title"].stringValue
        cell.lblDate?.text =  "\(json["expense_name"].stringValue) \(" || ") \(json["date"].stringValue)"
            
        cell.lblStatus?.text = json["status_name"].stringValue
         cell.lblExpense_amount?.font = UIFont.boldSystemFont(ofSize: 18)
         cell.lblExpense_amount?.text = temptext
        switch json["status"].intValue {
            
        case 1:
            cell.lblStatus?.textColor = UIColor.orange
            cell.delegate = self
            
        case 2:
        cell.lblStatus?.textColor = UIColor.blue
        cell.delegate = nil
        
            break
        case 3:
            cell.lblStatus?.textColor = UIColor.orange
            cell.delegate = nil

            
        case 4:
            cell.lblStatus?.textColor = UIColor.green
            cell.delegate = nil

        default:
            cell.lblStatus?.textColor = .white
            cell.delegate = nil

        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        guard let list = self.JsonData?[indexPath.row] else{return []}
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Bill?"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions: [{action1 in
                return
                },{action2 in
            
            self.JsonData?.remove(at: indexPath.row)
            self.tableView.reloadData()
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    
                self.DeleteBill(Taskid: list["id"].intValue, Task_delete: false)
                }
            }

           }, nil])
        }
        
        deleteAction.backgroundColor = UIColor.clear
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor:.white, size: CGSize(width: 32, height: 32))
        
        
        let editAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            guard let controller = EditBills.instance()
                else{return}
            
            controller.JsonDummy = [list]
            controller.BackDataBlock = {infos in
                print(infos)
                self.addANDEditBill(link: infos ,newFlag: false)
            }
            
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        editAction.backgroundColor = UIColor.lightGray
        editAction.image = UIImage.ionicon(with: .edit, textColor: .white, size: CGSize(width: 32, height: 32))
        
        return [deleteAction,editAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
}

// MARK: - PullToRefresh Method Refress List


extension BillsVc
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            // AppDelegate.showWaitView()
            
            self!.type = ""
            self!.datefrom = ""
            self!.dateto = ""
            
            
            self?.BillListDataFetch()
            }, loadingView: loadingView)
        
        tableView.dg_setPullToRefreshFillColor(BillNAVBAR())
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
}
// MaRK: - Filter Protocol Method


extension BillsVc : FilterProtocol
{
    func Filterdetails(FromDate : String , ToDate : String)
    {
        datefrom = FromDate
        dateto = ToDate
        
        print("datefrom :---> ",datefrom)
        print("dateto :---> ",dateto)
        
        AppDelegate.showWaitView()
        self.BillListDataFetch()
        
    }
    
}
// MARK: - Class Instance

extension BillsVc
{
    class func instance()->BillsVc?{
        let storyboard = UIStoryboard(name: "Billing", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BillsVc") as? BillsVc
        //self.definesPresentationContext = true
        
        return controller
    }
}
