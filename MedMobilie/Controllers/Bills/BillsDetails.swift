
import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit


class BillsDetails: InterfaceExtendedController {
    
    fileprivate var selectedContact_array : [String]?
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var ContactName  = [String]()
    fileprivate var contactId = [Int]()
    fileprivate var dataList = [JSON]()
    
    let imagePicker = UIImagePickerController()
    
    var BillId : Int = 0
    var JsonDummy = [JSON]()
    var Attach_Data_Array = [String : Any]()
    var Main_Attachment_Array = NSMutableArray ()
    var indexvalu = 0
    var RowValue = 0
    var numberOfCell = 2
    var ContactString = String()
    var Datevalue = String()
    var Timevalue = String()
    var ImgArray = [UIImage]()
    var ImgSaveArray = [String]()
    var selectedCountryId : Int = 0
    var dataPickerView = UIPickerView()
    var Time_Picker = UIDatePicker()
    var Date_Picker = UIDatePicker()
    var AttachmentArray = [String : Any]()
    var arrSctionTitle = NSArray()
    var  cell = UITableViewCell()
    var toolBar = UIToolbar()
    var delegate  : AddContactsToCreateOrderProtocol?
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        
        print(BillId)
        AppDelegate.showWaitView()
        BillDataFetch(idForDetails: BillId)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationBarTitleName(Title: "Bills Details")
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: BillNAVBAR(), BtnColor: UIColor.white)
        
    }
    // MARK: -   Language Setting
    
    func SetTitle(){
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: "")]
        
    }
    // MARK: - Fetch Bill Details data
    
    fileprivate func BillDataFetch(idForDetails : Int)
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetch_Api().Fetch(ID: BillId ,urlString: "/billapp_detail") { (status, message,data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.tableView.dg_stopLoading()
                self.alert(message)
                return
            }
            self.SetTitle()
            
            self.JsonDummy = data!
            
            print(self.JsonDummy)
            
            // checked.
            for index in 0..<(self.JsonDummy[0]["attachments"].count) {
                
                let json = self.JsonDummy[0]["attachments"][index]
                self.Attach_Data_Array = ["id":json["attachment_id"].intValue,"attachment":json["attachment"].stringValue,"filename":json["filename"].stringValue,"index":999]
                self.Main_Attachment_Array.add(self.Attach_Data_Array)
                
            }
            self.tableView.dg_stopLoading()
            self.tableView.reloadData()
        }
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension BillsDetails: UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate
{
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        return []
    }
    
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
            
        case 1:
            return Main_Attachment_Array.count
            
        default:
            return 0
        }
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = UIColor.black
        
        headerView.addSubview(label)
        
        return headerView
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return  5
        }
        else
        {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detail") as? VisitDetailsCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            
            
            cell.VisitName?.text = JsonDummy[0]["title"].stringValue
            cell.lblDetails?.text = JsonDummy[0]["description"].stringValue
            cell.lblDetails?.textColor = UIColor.black
            //  let dateStr = "\(json["visit_date"].stringValue) , \(json["visit_fromtime"].stringValue) \(self.languageKey(key: "To")) \(json["visit_totime"].stringValue)"
            cell.lblVisitDate?.text = JsonDummy[0]["date"].stringValue
            cell.lblVisitType?.text = JsonDummy[0]["expense_name"].stringValue
            
            // for address
            
            
            cell.lblAddressVisit?.text = JsonDummy[0]["location"].stringValue// CompleteAddress
            
            
            return cell
            
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            
            cell.lblUserName.text =   (self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "filename") as? String
            
            
            
            cell.imgUser.image = UIImage.fontAwesomeIcon(name: .fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
            
            return cell
            
        default:
            print("error")
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1
        {
            let filename = NSURL(string: (((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "attachment") as? String)!))
            
            guard let controller = WebViewVC.instance() else{return}
            controller.linkUrl = filename as? URL
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
}
// MARK: - Class Instance

extension BillsDetails
{
    class func instance()->BillsDetails?{
        
        let storyboard = UIStoryboard(name: "Billing", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "BillsDetails") as? BillsDetails
        
        
        return controller
    }
}
