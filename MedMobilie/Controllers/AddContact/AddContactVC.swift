//
//  AddContectVC.swift
//  MedMobilie
//
//  Created by MAC on 29/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
// Rana code // store board

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON


class AddContactVC: InterfaceExtendedController {
    
    fileprivate var temp_textField = UITextField()
    fileprivate var datavalue = [JSON]()

    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var selectedCountryIdAddress : Int = 0
    fileprivate var selectedCountryIdBilling : Int = 0
    fileprivate var selectedCountryIdShipping : Int = 0
    fileprivate var dataList = [JSON]()
    var ProductArray =   [Int]()
    fileprivate var Redline : Bool = false
    
    let OtherArray : NSMutableArray =         ["1","","","1","1","2","1","1","","","","","1","1","","","","1","1","","","","","","","", "","","1","1","","","" , "","","1","1","","",""]
    
    
    var boolvalue : Bool = false
    var Save_Btn : Bool = false
    var indexvalu = 0
    var RowValue = 0
    var CategaryIndex = 0

    var DefaultBillingSelection = true
    var DefaultShippingSelection = true
    var dataPicker = UIPickerView()
    var toolBar = UIToolbar()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var isAuthrizedContact : Bool = false
    
    var delegate  : AddContactsToCreateOrderProtocol?
    var skippedArray : NSMutableArray =         ["","","","","","","","","","","","","","","Yes","","","","","","","","Commercial","Yes","Yes",0,"","","","","","","","","","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","","","","","","","","","","","Yes","","","","","","","","Commercial","Yes","Yes",0,"","","","","","","","","","","","","",""]
    
    @IBOutlet weak var mTableview: UITableView!
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
         print("SaveArray Values:----->",SaveArray)
         print("skippedArray Values:----->",skippedArray)
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
        
        if Save_Btn == false
        {
            
            RightActionButton(Title:languageKey(key:"Save"))
            
        }
        
        print(skippedArray)
        if let billingCountry = SaveArray[17] as? Int
        {
            self.selectedCountryIdAddress = billingCountry
        }
        if let billingCountry = SaveArray[28] as? Int
        {
            self.selectedCountryIdBilling = billingCountry
        }
        if let billingCountry = SaveArray[35] as? Int
        {
            self.selectedCountryIdShipping = billingCountry
        }
        
        print(skippedArray.count)
        print(SaveArray.count)
        print(OtherArray.count)
        
        print(SaveArray)
        
        Listsapidata()
        
        createPickerView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    
    // MARK: - Create PickerView
    
    func createPickerView(){
        
        
        self.dataPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        self.dataPicker.delegate = self
        self.dataPicker.dataSource = self
        self.dataPicker.backgroundColor = UIColor.white
        
        
        
        // ToolBar
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
    }
    
    // MARK: - Fetch Data From LocalDatabase
    
    fileprivate func Listsapidata()
    {
        
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                
                AppDelegate.hideWaitView()
                
                self.dataList = jsonArray
            }
        }
        
    }
    
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        if boolvalue == false
        {
            NavigationBarTitleName(Title: "New Contact")
            
        }
        else
        {
            NavigationBarTitleName(Title: "Edit Contact")
            Redline = true
            
        }
                
        arrSctionTitle = ["User information".languageSet,"Status".languageSet,"Willing to Stock".languageSet,"Address".languageSet,"Default Billing Address".languageSet, "Default Shipping Address".languageSet]
        
        
        arrTitle = [  "Salutation".languageSet,"Contact Name".languageSet,"Attn".languageSet,"Customer Category".languageSet,"Type".languageSet,"Product To Market".languageSet,"Primary Specialty".languageSet,"Secondary Specialty".languageSet,"Company/office Name".languageSet,"Phone".languageSet,"Email".languageSet, "Website".languageSet, "Lead Status".languageSet,"Source of Lead".languageSet,  "Address 1".languageSet, "Address 2".languageSet,"Country".languageSet, "State".languageSet, "City".languageSet, "Zip/Postal Code".languageSet]
        
    }
    
    
    // MARK: - Filter Function
    
    fileprivate func gettingNameFromDataList(keyName : String , index : Int) -> String
    {
        var valueName = ""
        self.dataList[0][keyName].forEach({
            if $0.1["id"].stringValue == SaveArray[index] as? String
            {
                valueName = $0.1["name"].stringValue
                return
            }
        })
        return valueName
    }
    
    // MARK: - Scroll Tableview
    func ScrollTableview(Section:Int,Row:Int) {
        
        mTableview.reloadData()
        let indexPath = IndexPath(row: Row, section: Section)
        mTableview.scrollToRow(at: indexPath, at: .top, animated: true)
        
        
    }
    
    // MARK: - Save Button Action
    override func rightButtonAction()
    {
        Redline = true
        var datavalues = [String : Any]()
        //0
        if SaveArray[0] as? String != ""
        {
            datavalues["salutation_id"] = SaveArray[0]
            
            datavalues["salutation"] = gettingNameFromDataList(keyName: "salutation", index: 0)
        }
        else
        {
            datavalues["salutation_id"] = ""
            datavalues["salutation"] = ""
           // self.alert("please choose a saluation for contact".languageSet)
           // ScrollTableview(Section: 0, Row: 0)
           // return
        }
        //1
        if SaveArray[1] as? String != ""
        {
            datavalues["contact_name"] = SaveArray[1]
        }
        else
        {
            self.alert("please fill contact Name".languageSet)
            ScrollTableview(Section: 0, Row: 1)
            return
        }
        //2
        datavalues["attn_name"] = SaveArray[2]
        //3
        if SaveArray[3] as? String != ""
        {
            datavalues["category_id"] = SaveArray[3]
            datavalues["category_name"] = gettingNameFromDataList(keyName: "category", index: 3)
        }
        else
        {
            self.alert("please select Customer Category".languageSet)
            ScrollTableview(Section: 0, Row: 2)
            return
        }
        
        
        
//        if SaveArray[4] as? String != ""
//        {
            datavalues["contact_title_id"] = SaveArray[4]
            datavalues["contact_title_name"] = skippedArray[4]
        
        
            
            
         //   datavalues["contact_title_name"] = gettingNameFromDataList(keyName: "title", index: 4)
         //   ScrollTableview(Section: 0, Row: 4)
//        }
//        else
//        {
//            self.alert("please select Title".languageSet)
//            return
//        }
        
        
//        if SaveArray[5] as? String != ""
//        {
       //     datavalues["product_id"] = SaveArray[5]
//
//
//            datavalues["product_id"] = gettingNameFromDataList(keyName: "product", index: 4)
//        }
//        else
//        {
//            self.alert("please select product".languageSet)
//            ScrollTableview(Section: 0, Row: 4)
//            return
//        }
//
        
        //4
        
        if SaveArray[6] as? String != ""
        {
            datavalues["primary_speciality_id"] = SaveArray[6]
            datavalues["primary_speciality_name"] = skippedArray[6]
            
            
            datavalues["primary_speciality_name"] = gettingNameFromDataList(keyName: "speciality", index: 5)
        }
        else
        {
            self.alert("please select primary Specialty".languageSet)
            ScrollTableview(Section: 0, Row: 5)
            return
        }
        
        //5
        
        
        
        datavalues["secondary_speciality_id"] = SaveArray[7]
        
        
        datavalues["secondary_specialities_name"] = gettingNameFromDataList(keyName: "speciality", index: 7)
        
        
        //6
        
       
        // 7
        
        datavalues["company_name"] = SaveArray[8]
        // 8
        if SaveArray[9] as? String != ""
        {
            datavalues["phone"] = SaveArray[9]
        }
        else
        {
            self.alert("please fill phone number".languageSet)
            ScrollTableview(Section: 0, Row: 9)
            return
        }
        
        if SaveArray[10] as? String != ""
        {
            if let email = SaveArray[10] as? String
            {
                if email.EmailValidation()
                {
                    datavalues["email"] = SaveArray[10]
                }
                else
                {
                    self.alert("please fill correct email id".languageSet)
                    ScrollTableview(Section: 0, Row: 10)
                    return
                }
            }
            
        }
        else
        {
            datavalues["email"] = ""
        }
        datavalues["website"] = SaveArray[11]
        datavalues["is_express"] = true
        datavalues["is_editable"] = true
        
        
        
        if SaveArray[12] as? String != ""
        {
            datavalues["lead_status_id"] = SaveArray[12]
            datavalues["lead_status_name"] = gettingNameFromDataList(keyName: "leadstatus", index: 12)
        }
        else
        {
            self.alert("please select lead status".languageSet)
            ScrollTableview(Section: 1, Row: 0)
            return
        }
        
        if SaveArray[13] as? String != ""
        {
            datavalues["lead_source"] = SaveArray[13]
            
        }
        else
        {
//            self.alert("please select lead source".languageSet)
//            ScrollTableview(Section: 1, Row: 1)
//            return
            
            datavalues["lead_source"] = ""
        }
        
        
        
        
        if SaveArray[14] as? String != ""
        {
            datavalues["willingtostock"] = SaveArray[14]
        }
        else
        {
            datavalues["willingtostock"] = "No"
        }
        
        // 14
        if SaveArray[15] as? String != ""
        {
            datavalues["address1"] = SaveArray[15]
        }
        else
        {
            self.alert("please fill a Address 1 field".languageSet)
            ScrollTableview(Section: 3, Row: 0)
            return
        }
        
        datavalues["address2"] = SaveArray[16]
        
        
        if SaveArray[17] as? String != ""
        {
            datavalues["country_id"] = SaveArray[17]
            datavalues["country_name"] = gettingNameFromDataList(keyName: "country", index: 17)
        }
        else
        {
            self.alert("please select a country".languageSet)
            ScrollTableview(Section: 3, Row: 4)
            return
        }
        
        //19
        
        
        if SaveArray[18] as? String != ""
        {
            datavalues["state_id"] = SaveArray[18]
            datavalues["state_name"] = gettingNameFromDataList(keyName: "state", index: 18)
        }
        else
        {
            self.alert("please select a state".languageSet)
            ScrollTableview(Section: 3, Row: 5)
            return
        }
        // 16
        if SaveArray[19] as? String != ""
        {
            datavalues["city"] = SaveArray[19]
        }
        else
        {
            self.alert("please fill city field".languageSet)
            ScrollTableview(Section: 3, Row: 2)
            return
        }
        
        //17
        
        if SaveArray[20] as? String != ""
        {
            datavalues["zipcode"] = SaveArray[20]
        }
        else
        {
            self.alert("please fill Zipcode field".languageSet)
            ScrollTableview(Section: 3, Row: 3)
            return
        }
        
        //18
        
        
       
        
        // 20
        
        datavalues["fax"] = SaveArray[21]
        
        //21
        
        datavalues["type"] = SaveArray[22]
        
        //22 billing addressCheck
        if SaveArray[23] as? String == "No"
        {
            datavalues["default_billing_address"] = SaveArray[23]
            
            
            // 25
            if SaveArray[23] as? String != ""
            {
                datavalues["billing_address1"] = SaveArray[23]
            }
            else
            {
                self.alert("please fill billing Address1 Field".languageSet)
                ScrollTableview(Section: 4, Row: 0)
                return
            }
            // 26
            datavalues["billing_address2"] = SaveArray[27]
            
            // 27
            
            if SaveArray[30] as? String != ""
            {
                datavalues["billing_city"] = SaveArray[30]
            }
            else
            {
                self.alert("please fill billing City Field".languageSet)
                ScrollTableview(Section: 4, Row: 2)
                return
            }
            
            //28
            
            if SaveArray[31] as? String != ""
            {
                datavalues["billing_zip_code"] = SaveArray[31]
            }
            else
            {
                self.alert("please fill billing Zipcode Field".languageSet)
                ScrollTableview(Section: 4, Row: 3)
                return
            }
            
            //29
            
            
            if SaveArray[28] as? String != ""
            {
                datavalues["billing_country_id"] = SaveArray[28]
                datavalues["billing_country_name"] = gettingNameFromDataList(keyName: "country", index: 28)
            }
            else
            {
                self.alert("please select billing Country".languageSet)
                ScrollTableview(Section: 4, Row: 4)
                return
            }
            
            //30
            
            
            if SaveArray[29] as? String != ""
            {
                datavalues["billing_state_id"] = SaveArray[29]
                datavalues["billing_state_name"] = gettingNameFromDataList(keyName: "state", index: 29)
            }
            else
            {
                self.alert("please select billing State".languageSet)
                ScrollTableview(Section: 4, Row: 5)
                return
            }
            
            datavalues["billing_fax"] = SaveArray[32]
            
        }
        else
        {
            datavalues["default_billing_address"] = SaveArray[23]
            datavalues["billing_address1"] = SaveArray[26]
            datavalues["billing_address2"] = SaveArray[27]
            datavalues["billing_city"] = SaveArray[30]
            datavalues["billing_zipcode"] = SaveArray[31]
            datavalues["billing_country_id"] = SaveArray[28]
            datavalues["billing_country_name"] = ""
            datavalues["billing_state_id"] = SaveArray[29]
            datavalues["billing_state_name"] = ""
            datavalues["billing_fax"] = SaveArray[32]
            
        }
        
        datavalues["id"] = SaveArray[25]
        
        if SaveArray[24] as? String == "No"
        {
            datavalues["default_shipping_address"] = SaveArray[24]
            
            // 32
            if SaveArray[33] as? String != ""
            {
                datavalues["shipping_address1"] = SaveArray[33]
            }
            else
            {
                self.alert("please fill shipping Address1 Field".languageSet)
                ScrollTableview(Section: 5, Row: 0)
                return
            }
            // 33
            datavalues["shipping_address2"] = SaveArray[24]
            
            // 34
          
            
            //36
            
            
            if SaveArray[35] as? String != ""
            {
                datavalues["shipping_country_id"] = SaveArray[35]
                datavalues["shipping_country_name"] = gettingNameFromDataList(keyName: "country", index: 35)
            }
            else
            {
                self.alert("please select shipping Country".languageSet)
                ScrollTableview(Section: 5, Row: 4)
                return
            }
            
            //37
            
            
            if SaveArray[36] as? String != ""
            {
                datavalues["shipping_state_id"] = SaveArray[36]
                datavalues["shipping_state_name"] = gettingNameFromDataList(keyName: "state", index: 36)
            }
            else
            {
                self.alert("please select shipping State".languageSet)
                ScrollTableview(Section: 5, Row: 5)
                return
            }
            
            
            if SaveArray[37] as? String != ""
            {
                datavalues["shipping_city"] = SaveArray[37]
            }
            else
            {
                self.alert("please fill shipping City Field".languageSet)
                ScrollTableview(Section: 5, Row: 2)
                return
            }
            
            //35
            
            if SaveArray[38] as? String != ""
            {
                datavalues["shipping_zip_code"] = SaveArray[38]
            }
            else
            {
                self.alert("please fill shipping Zipcode Field".languageSet)
                ScrollTableview(Section: 5, Row: 3)
                return
            }
            
            datavalues["shipping_fax"] = SaveArray[39]
            
        }
        else
        {
            datavalues["default_shipping_address"] = SaveArray[24]
            datavalues["shipping_address1"] = SaveArray[33]
            datavalues["shipping_address2"] = SaveArray[34]
            datavalues["shipping_country_id"] = SaveArray[35]
            datavalues["shipping_country_name"] = ""
            datavalues["shipping_state_name"] = ""
            datavalues["shipping_state_id"] = SaveArray[36]
            datavalues["shipping_city"] = SaveArray[37]
            datavalues["shipping_zip_code"] = SaveArray[38]
            datavalues["shipping_fax"] = SaveArray[39]
        }
        
        datavalues["AttachmentFiles"] = []

        
//        let jsonData = try? JSONSerialization.data(withJSONObject: ProductArray, options: [])
//        let jsonString = String(data: jsonData!, encoding: .utf8)
//        print(jsonString!)
//
  
     //   datavalues["product_ids"] = ProductArray
        
        if datavalues["id"] as! Int == 0 {
            
            AddContactRequest(datavalues, Method: "/contactapp_create")
        }
        else
        {
            AddContactRequest(datavalues, Method: "/contactapp_edit")
        }
        
    }
    
    // MARK: - Save Offline Data
    
    fileprivate func OfflineCreateContact (parama : [String : Any])
    {
        let userdefault = UserDefaults.standard
        var id : Int = 30000
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int != nil
        {
            let tempid = userdefault.value(forKey: "OfflineNewContactCreate") as! Int
            
            id = tempid + 1
            
        }
        
        var param  = [String:Any]()
        param = parama
        
        
        param["id"] = 0
        param["temp_id"] = Int(Date().timeIntervalSince1970)
        param["created_date"] = Int(Date().timeIntervalSince1970)
        let dict = JSON(param)

        DataBaseHelper.ShareInstance.AddContact(Object: dict,NewType : true){status in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert("something went wrong".languageSet)
                return
            }
            userdefault.set(id, forKey: "OfflineNewContactCreate")
            userdefault.synchronize()
            self.navigationController?.popViewController(animated: true)
            self.delegate?.contactdetails(name: "", Contact_id: 0)
            
        }
        print(param)
    }
    
    // MARK: - Add Contact Request API
    
    fileprivate func AddContactRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            OfflineCreateContact(parama: param)
            // AppDelegate.alertViewForInterNet()
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                if param["id"] as? Int != 0
                {
                    DataBaseHelper.ShareInstance.DeleteEntry(ClassName: OfflineDataClass.Contact.rawValue, Id: param["id"] as! Int)
                }
                    
                
                if let contactJson = info
                {
                    
                    ///// new coredata code
                     DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.Contact.rawValue)
                    
                    contactJson.forEach({
                        DataBaseHelper.ShareInstance.AddContact(Object: $0)
                    })
                }
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                    self.navigationController?.popViewController(animated: true)

                     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RfressVisitList"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVisitList"), object: nil)
               
                    self.delegate?.contactdetails(name: "", Contact_id: 0)
                    
                    }, nil])
            }
            
            
            
        }
        
        
    }
    
    
    // MARK: - Click For Billing Button
    
    @objc func ClickForBillingBtn(sender : UIButton)
    {
        if SaveArray[23] as! String == "No"
        {
            
            SaveArray[23] = "Yes"
            
            
            DefaultBillingSelection = true
        }
        else
        {
            SaveArray[23] = "No"
            DefaultBillingSelection = false
        }
        
        
        mTableview.reloadSections(IndexSet.init(integer: 4), with: .none)
    }
    
    // MARK: - Click For Shipping Button
    
    @objc func ClickForShippingBtn(sender : UIButton)
    {
        if SaveArray[24] as! String == "No"
        {
            
            SaveArray[24] = "Yes"
            
            DefaultShippingSelection = true
        }
        else
        {
            SaveArray[24] = "No"
            
            DefaultShippingSelection = false
        }
        mTableview.reloadSections(IndexSet.init(integer: 5), with: .none)
        
    }
    
}

// MARK: - TableView Delegate/DataSource Methods

extension AddContactVC: UITableViewDelegate,UITableViewDataSource
{
    
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 12
        case 1:
            return 2
            
        case 2:
            return 1
        case 3:
            return 6
            
        case 4:
            if self.DefaultBillingSelection
            {
                return 0
            }
            else
            {
                return 6
            }
        case 5:
            if self.DefaultShippingSelection
            {
                return 0
            }
            else
            {
                return 6
            }
            
            
            
        default:
            return 0
        }
        
        
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 4
        {
            tableView.register(UINib(nibName: "SelectionHeaderForBilling", bundle: nil), forCellReuseIdentifier: "selectionCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as? SelectionHeaderForBilling
                else
            {return UIView()}
            cell.textLbl.text = arrSctionTitle[section] as? String
            
            
            if Save_Btn == false
            {
                cell.CheckANDUncheckBtn.addTarget(self, action: #selector(ClickForBillingBtn(sender:)), for: UIControl.Event.touchUpInside)
            }
            
            
            if DefaultBillingSelection
            {
                cell.ImageOFBtn.image = UIImage.ionicon(with: .androidCheckboxOutline, textColor: UIColor.blue, size: CGSize(width: 50, height: 50))
            }
            else
            {
                cell.ImageOFBtn.image = UIImage.ionicon(with: .androidCheckboxOutlineBlank, textColor: UIColor.blue, size: CGSize(width: 50, height: 50))
            }
            
            
            return cell.contentView
        }
        else if section == 5
        {
            tableView.register(UINib(nibName: "SelectionHeaderForBilling", bundle: nil), forCellReuseIdentifier: "selectionCell")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as? SelectionHeaderForBilling
                else
            {return UIView()}
            cell.textLbl.text = arrSctionTitle[section] as? String
            if DefaultShippingSelection
            {
                cell.ImageOFBtn.image = UIImage.ionicon(with: .androidCheckboxOutline, textColor: UIColor.blue, size: CGSize(width: 50, height: 50))
            }
            else
            {
                cell.ImageOFBtn.image = UIImage.ionicon(with: .androidCheckboxOutlineBlank, textColor: UIColor.blue, size: CGSize(width: 50, height: 50))
            }
            
            if Save_Btn == false
            {
                cell.CheckANDUncheckBtn.addTarget(self, action: #selector(ClickForShippingBtn(sender:)), for: UIControl.Event.touchUpInside)
                
            }
            return cell.contentView
            
        }
        else
        {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
            headerView.backgroundColor = OffWhiteColor()
            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
            label.text = arrSctionTitle[section] as? String
            label.font = UIFont.systemFont(ofSize: 16)
            label.textColor = UIColor.black
            //  label.backgroundColor = UIColor.yellow
            headerView.addSubview(label)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            if indexPath.row == 5 {
                return 0.0
            }
            else{
               return 75
            }
        }
        else if (indexPath.section == 1){
            if indexPath.row == 1 {
                return 0.0
            }
            else{
                return 75
            }
        }
        else{
            
        }
        
        return 75
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func ShowDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    
    func HideDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
//            cell.txtTitleAC.autocapitalizationType = .words
           
           
            
            if OtherArray[indexPath.row] as! String == "1"
            {
                if !Save_Btn
                {
                    cell.txtTitleAC.rightViewMode = .always
                    cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                }
                //  cell.accessoryView = ShowDropDownImage()
                
                self.pickUpDate(cell.txtTitleAC)
                
            }
            else if OtherArray[indexPath.row] as! String == "2"
            {
                if !Save_Btn
                {
                    cell.txtTitleAC.rightViewMode = .always
                    cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowForward)
                }
            }
                
            else
            {
                
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                
                // cell.accessoryView = HideDropDownImage()
            }
            
            if indexPath.row == 9
            {
                cell.txtTitleAC.keyboardType = .default
                cell.txtTitleAC.isUserInteractionEnabled = true
                cell.isUserInteractionEnabled = true
            }
            else if indexPath.row == 10
            {
                cell.txtTitleAC.keyboardType = .emailAddress
                cell.txtTitleAC.isUserInteractionEnabled = true
                cell.isUserInteractionEnabled = true
            }

            else
            {
                cell.txtTitleAC.keyboardType = .default
            }
            
            if Save_Btn == true
            {
                
                cell.isUserInteractionEnabled = false
                
            }
//            else if isAuthrizedContact
//            {
//                if indexPath.row == 9 || indexPath.row == 10
//                {
//                    cell.isUserInteractionEnabled = true
//                    cell.txtTitleAC.backgroundColor = UIColor.green.withAlphaComponent(0.3)
//                }
//
//                else
//                {
//                    cell.isUserInteractionEnabled = false
//                    cell.txtTitleAC.backgroundColor =  UIColor.init(hexString: "#f2f2f2")
//                }
//            }
            
            if isAuthrizedContact
            {
                cell.isUserInteractionEnabled = true
            }
            if Redline == true
            {
                
                print(indexPath.row)
                
                if indexPath.row != 0 && indexPath.row != 2 && indexPath.row != 4 && indexPath.row != 7 && indexPath.row != 8 && indexPath.row != 10 && indexPath.row != 11
                {
                    
                    if skippedArray[indexPath.row] as? String == ""
                    {
                        
                        cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 0
                    }
                }
                else
                    
                {
                    cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 0
                }
                
            }
            
            
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddLeadCell") as! AddLeadCell
            cell.lbtitleAL.text = arrTitle[indexPath.row+12] as? String
            cell.txtTitleAL.text = skippedArray[indexPath.row+12] as? String
            cell.txtTitleAL.delegate = self
            cell.txtTitleAL.tag = indexPath.row+12
            cell.selectionStyle = .none
            
            
            if !Save_Btn
            {
                cell.txtTitleAL.rightViewMode = .always
                cell.txtTitleAL.rightView = ShowDropDownImage(Image: .iosArrowDown)
            }
            
            //  cell.accessoryView = ShowDropDownImage()
            self.pickUpDate(cell.txtTitleAL)
            
            if Save_Btn == true
            {
                
                cell.isUserInteractionEnabled = false
                
            }
            
            if Redline == true
            {
                
                print(indexPath.row)
                
                if skippedArray[indexPath.row+12] as? String == ""
                {
                    
                    cell.txtTitleAL.layer.borderColor = UIColor.red.cgColor as CGColor
                    cell.txtTitleAL.layer.borderWidth = 1.0
                }
                else
                {
                    cell.txtTitleAL.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAL.layer.borderWidth = 0
                }
            }
            
            return cell
            
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactstockCell") as! AddContactstockCell
            
            
            let type = skippedArray[indexPath.row+14] as! String
            
            if (type == "Yes" )
            {
                cell.btnYes.setImage(UIImage.ionicon(with:.androidRadioButtonOn , textColor: UIColor.black, size: CGSize(width: 50, height: 50)), for: .normal)
                
                cell.btnNo.setImage(UIImage.ionicon(with:.androidRadioButtonOff , textColor: UIColor.black, size: CGSize(width: 50, height: 50)), for: .normal)
            }
            else
            {
                cell.btnYes.setImage(UIImage.ionicon(with:.androidRadioButtonOff , textColor: UIColor.black, size: CGSize(width: 50, height: 50)), for: .normal)
                
                cell.btnNo.setImage(UIImage.ionicon(with:.androidRadioButtonOn , textColor: UIColor.black, size: CGSize(width: 50, height: 50)), for: .normal)
            }
            
            cell.btnNo.addTarget(self, action: #selector(ButtonNoAction), for: .touchUpInside)
            
            cell.btnYes.addTarget(self, action: #selector(ButtonYesAction), for: .touchUpInside)
            
            cell.selectionStyle = .none
            
            
            
            
            cell.accessoryView = HideDropDownImage()
            
            if Save_Btn == true
            {
                
                cell.isUserInteractionEnabled = false
                
            }
                    
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAddress") as! AddAddress
            cell.lbtitleAD.text = arrTitle[indexPath.row+14] as? String
            cell.txtTitleAD.text = skippedArray[indexPath.row+15] as? String
            cell.txtTitleAD.tag = indexPath.row+15
            cell.txtTitleAD.delegate = self
            cell.selectionStyle = .none
            
            if OtherArray[indexPath.row+15] as! String == "1"
            {
                self.pickUpDate(cell.txtTitleAD)

                if !Save_Btn
                {
                    cell.txtTitleAD.rightViewMode = .always
                    cell.txtTitleAD.rightView = ShowDropDownImage(Image: .iosArrowDown)
                }
                //   cell.accessoryView = ShowDropDownImage()

            }
            else
            {

                cell.txtTitleAD.rightViewMode = .never
                cell.txtTitleAD.rightView = nil

                // cell.accessoryView = HideDropDownImage()
            }

            if indexPath.row == 7
            {
                cell.txtTitleAD.keyboardType = .numberPad
            }
            else
            {
                cell.txtTitleAD.keyboardType = .default
            }
            if Save_Btn == true
            {
                
                cell.isUserInteractionEnabled = false
                
            }
                    
            if Redline == true
            {
                
                print(indexPath.row)
                
                if indexPath.row != 1
                {
                    
                    if skippedArray[indexPath.row+15] as? String == ""
                    {
                        
                        cell.txtTitleAD.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAD.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAD.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAD.layer.borderWidth = 0
                    }
                }
                else
                    
                {
                    cell.txtTitleAD.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAD.layer.borderWidth = 0
                }
            }
            
            return cell
            
        case 4:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAddress") as! AddAddress
            cell.lbtitleAD.text = arrTitle[indexPath.row+14] as? String
            cell.txtTitleAD.text = skippedArray[indexPath.row+26] as? String
            cell.txtTitleAD.tag = indexPath.row+26
            cell.txtTitleAD.delegate = self
            cell.selectionStyle = .none
            
            
            if OtherArray[indexPath.row+26] as! String == "1"
            {
                self.pickUpDate(cell.txtTitleAD)
                if !Save_Btn
                {
                    cell.txtTitleAD.rightViewMode = .always
                    cell.txtTitleAD.rightView = ShowDropDownImage(Image: .iosArrowDown)
                }
                
                //   cell.accessoryView = ShowDropDownImage()
                
                
            }
            else
            {
                
                cell.txtTitleAD.rightViewMode = .never
                cell.txtTitleAD.rightView = nil
                
                //cell.accessoryView = HideDropDownImage()
            }
            
            if indexPath.row == 7
            {
                cell.txtTitleAD.keyboardType = .numberPad
            }
            else
            {
                cell.txtTitleAD.keyboardType = .default
            }
            if Save_Btn == true
            {
                
                cell.isUserInteractionEnabled = false
                
            }
            
            
            if Redline == true
            {
                
                print(indexPath.row)
                
                if indexPath.row != 1
                {
                    
                    if skippedArray[indexPath.row+26] as? String == ""
                    {
                        
                        cell.txtTitleAD.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAD.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAD.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAD.layer.borderWidth = 0
                    }
                }
                else
                    
                {
                    cell.txtTitleAD.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAD.layer.borderWidth = 0
                }
                
            }
            
            
            return cell
            
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAddress") as! AddAddress
            cell.lbtitleAD.text = arrTitle[indexPath.row+14] as? String
            cell.txtTitleAD.text = skippedArray[indexPath.row+33] as? String
            cell.txtTitleAD.tag = indexPath.row+33
            cell.txtTitleAD.delegate = self
            cell.selectionStyle = .none
            
            
            if OtherArray[indexPath.row+33] as! String == "1"
            {
                self.pickUpDate(cell.txtTitleAD)
                
                if !Save_Btn
                {
                    cell.txtTitleAD.rightViewMode = .always
                    cell.txtTitleAD.rightView = ShowDropDownImage(Image: .iosArrowDown)
                }
                
                //  cell.accessoryView = ShowDropDownImage()
                
                
            }
            else
            {
                
                cell.txtTitleAD.rightViewMode = .never
                cell.txtTitleAD.rightView = nil
                
                //  cell.accessoryView = HideDropDownImage()
            }
            
            if indexPath.row == 7
            {
                cell.txtTitleAD.keyboardType = .numberPad
            }
            else
            {
                cell.txtTitleAD.keyboardType = .default
            }
            if Save_Btn == true
            {
                cell.isUserInteractionEnabled = false
            }
            
            if Redline == true
            {
                
                print(indexPath.row)
                
                if indexPath.row != 1
                {
                    
                    if skippedArray[indexPath.row+33] as? String == ""
                    {
                        
                        cell.txtTitleAD.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAD.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAD.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAD.layer.borderWidth = 0
                    }
                }
                else
                    
                {
                    cell.txtTitleAD.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAD.layer.borderWidth = 0
                }
                
            }
            
            return cell
            
            
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ddd") as! AddContactCell
            cell.selectionStyle = .none
            cell.accessoryView = HideDropDownImage()
            
            if Save_Btn == true
            {
                
                cell.isUserInteractionEnabled = false
                
            }
            
            return cell
        }
        
        
        //  self.view .endEditing(true)
        
        
        
    }
    
    
    //Button Commercial Action
    @objc func ButtonCommercialAction() {
        
        skippedArray[22] = "Commercial"
        
        SaveArray[22] = "Commercial"
        mTableview.reloadSections(IndexSet.init(integer: 4), with: .none)
        
        
        
    }
    
    //Button Residential Action
    @objc func ButtonResidentialAction() {
        skippedArray[22] = "Residential"
        
        SaveArray[22] = "Residential"
        mTableview.reloadSections(IndexSet.init(integer: 4), with: .none)
    }
    
    //Show DropDown Image
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    // No Button Action
    @objc func ButtonNoAction() {
        
        
        skippedArray[14] = "No"
        
        SaveArray[14] = "No"
        mTableview.reloadData()
        
    }
    
    //  Yes Button Action
    @objc func ButtonYesAction() {
        skippedArray[14] = "Yes"
        
        SaveArray[14] = "Yes"
        
        mTableview.reloadData()
    }
    
    
}

// MARK: - PickerView Delegate/DataSource Methods

extension AddContactVC : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        
        print("Tag value :",textField.tag)
        
        if textField.tag == 0  || textField.tag == 3 || textField.tag == 4 || textField.tag == 5 || textField.tag == 6 || textField.tag == 7 || textField.tag == 12 || textField.tag == 13 || textField.tag == 17  || textField.tag == 18 || textField.tag == 28 || textField.tag == 31 || textField.tag == 35 || textField.tag == 36       {
            
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            
        }
            
            
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            
        }
        
        
        
    }
    @objc func doneClick() {
        
        
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 3
        {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            skippedArray[indexvalu+1] = ""
            
            SaveArray[indexvalu+1] = ""
            
            print(Pickerdatavalue)
            
            
            
            CategaryIndex = RowValue

            
        }
            
        else if indexvalu == 4
        {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            
            
            print(skippedArray)
             print(SaveArray)
            
        }
            
        else if indexvalu == 6
        {
            
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
            
            
        else if indexvalu == 7
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 12
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
            
        }
            
            
        else if indexvalu == 13
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
                        
        }
            
        else if indexvalu == 17
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryIdAddress = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 18
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 28
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryIdBilling = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 29
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
            
            
            
        else if indexvalu == 35
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryIdShipping = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 36
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
        
        
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        //  temp_textField.resignFirstResponder()
        
        self.view .endEditing(true)
        
        
        print(SaveArray)
        
        mTableview.reloadData()
        
    }
    
    
    @objc func cancelClick() {
        
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        //   temp_textField.resignFirstResponder()
        
        self.view .endEditing(true)
        // temp_textField.resignFirstResponder()
    }
    
    

    func isValidInput(Input:String) -> Bool {
        let myCharSet=CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        let output: String = Input.trimmingCharacters(in: myCharSet.inverted)
        let isValid: Bool = (Input == output)
        print("\(isValid)")

        return isValid
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textField----- did begin --> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            
            Pickerdatavalue = [self.dataList[0]["salutation"]]
            
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
            
        }
        else if textField.tag == 3
        {
            
            
            Pickerdatavalue = [self.dataList[0]["category"]]
            
            print(Pickerdatavalue)
            
            
            
            self.dataPicker.reloadAllComponents()
        }
        else if textField.tag == 4
        {
            
            if skippedArray[indexvalu-1] as! String == ""
            {
                
                
                self.alert("Please select category First")
                
                self.view .endEditing(true)

                
                Pickerdatavalue.removeAll()
                self.dataPicker.removeFromSuperview()
                self.toolBar.removeFromSuperview()

                
            }
              else{
                
                datavalue = [self.dataList[0]["category"]]

                print(datavalue[0][CategaryIndex]["types"])

                
                if datavalue[0][CategaryIndex]["types"].count == 0
                {
                    
                    self.view .endEditing(true)


                    Pickerdatavalue.removeAll()
                    self.dataPicker.removeFromSuperview()
                    self.toolBar.removeFromSuperview()
                    
                    self.alert("Type list is empty")

                }
                else
                {
                    

                    
                    print(datavalue)
                    print([self.datavalue[0]["types"]])
                    print(datavalue[0][RowValue]["types"])
                    
                    Pickerdatavalue =   [datavalue[0][0]["types"]]
                    print(Pickerdatavalue)
                    
                    self.dataPicker.reloadAllComponents()
                }
               
                }
           
        }
        else if textField.tag == 5
        {
            
            
            
            guard let controller = ProductToMarket.instance() else
            {
                return
            }
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: false)
            
            
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPicker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
            
        else if textField.tag == 6 || textField.tag == 7
        {
            Pickerdatavalue = [self.dataList[0]["speciality"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
        
        else if textField.tag == 9
        {
//            do {
//                let regex = try NSRegularExpression(pattern:".*[^A-Za-z ].*", options: [])
//                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
//                    return false
//                }
//            }
//            catch {
//                print("ERROR")
//            }
            
//            print("click on tf")
            
//
//            if isValidInput(Input: yourtextfieldOutletName.text!) == false {
////                let alert = UIAlertController(title: "", message;"Name field accepts only alphabatics", preferredStyle: UIAlertControllerStyle.alert)
////                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
////
////                self.present(alert, animated: true, completion: nil)
//            }
            
        }
            
//        else if textField.tag == 8
//        {
//            Pickerdatavalue = [self.dataList[0]["title"]]
//
//
//            print(Pickerdatavalue)
//
//            self.dataPicker.reloadAllComponents()
//        }
            
        else if textField.tag == 12
        {
            Pickerdatavalue = [self.dataList[0]["leadstatus"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
        else if textField.tag == 13
        {
            Pickerdatavalue = [self.dataList[0]["source"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
            
        else if textField.tag == 17
        {
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print("Pickerdatavalue is - \(Pickerdatavalue)")
            
            self.dataPicker.reloadAllComponents()
        }
            
        else if textField.tag == 28
        {
            Pickerdatavalue = [self.dataList[0]["country"]]
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
            
        else if textField.tag == 35
        {
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
            
        else if textField.tag == 18
        {
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryIdAddress)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                self.alert("Please select Country First")
            }
            /* Pickerdatavalue = [self.dataList[0]["state"]]
             
             print(Pickerdatavalue)
             
             self.dataPicker.reloadAllComponents()*/
        }
            
        else if textField.tag == 29
        {
            
            
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryIdBilling)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                self.alert("Please select Country First")
            }
            /* Pickerdatavalue = [self.dataList[0]["state"]]
             
             print(Pickerdatavalue)
             
             self.dataPicker.reloadAllComponents()*/
            
        }
            
        else if textField.tag == 36
        {
            
            
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryIdShipping)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                self.alert("Please select Country First")
            }
            /* Pickerdatavalue = [self.dataList[0]["state"]]
             
             print(Pickerdatavalue)
             
             self.dataPicker.reloadAllComponents()*/
            
        }
            
            
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPicker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
            
        }
        
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        print(self.dataList)
        
        
        //  let currency_name = self.dataList[data]["name"].stringValue
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        print("textField-----should change> ",textField.tag)
        print("textField-----indexvalu should change> ",indexvalu)
        indexvalu = textField.tag
        
        if indexvalu == 9
        {
//            print("typing in phone number field", indexvalu)
            
            do {
                let regex = try NSRegularExpression(pattern:".*[^0-9- ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            
        }
        
        if indexvalu == 16
        {
            textField.autocapitalizationType = .words
        }
        
       
        
        skippedArray[textField.tag] = newString
        
        SaveArray[textField.tag] = newString
        
        
        
        print(skippedArray)
        
        
        return true
    }
    
}


// MARK: - PickerView Delegate/DataSource Methods

extension AddContactVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue[0].count
            
        }
        return 0
        // return Pickerdatavalue[0].count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}


// MARK: - Class Instance

extension AddContactVC
{
    class func instance()->AddContactVC?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AddContactVC") as? AddContactVC
        
        
        return controller
    }
}



extension AddContactVC : SpecialityProtocol
{
    func Speciality_String(List_Iteam: String) {
        
    }
    
    func Speciality_String(List_Iteam : String,ProductArray:[Int]) {
        
        
        //  DropDownTextFeild?.text = List_Iteam
        
        skippedArray[indexvalu] = List_Iteam
        
        SaveArray[indexvalu] = List_Iteam
        
        self.ProductArray = ProductArray
        
        mTableview.reloadData()

        print("ProductArray :---> ",ProductArray)

        print("List_Iteam :---> ",List_Iteam)
        
    }
    
}

