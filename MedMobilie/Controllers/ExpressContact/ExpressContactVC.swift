//
//  AddContectVC.swift
//  MedMobilie
//
//  Created by MAC on 29/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
// Rana code // store board

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import CoreLocation


class ExpressContactVC: InterfaceExtendedController ,CLLocationManagerDelegate {
    
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var dataList = [JSON]()
    fileprivate var selectedCountryId : Int = 0
    
    let OtherArray : NSMutableArray =         ["1","","1","","","","","1","1","",""]
    
    var indexvalu = 0
    var RowValue = 0
    var dataPicker = UIPickerView()
    var toolBar = UIToolbar()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var delegate  : AddContactsToCreateOrderProtocol?
    var skippedArray : NSMutableArray =         ["","","","","","","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","","","","","","",""]
    
    var latvalue : Double = 0.0
    var longvalue : Double = 0.0
    var locationManager: CLLocationManager!
    
    @IBOutlet weak var mTableview: UITableView!
    
    // MARK: - Class Instance

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
        
        RightActionButton(Title:languageKey(key:"Save"))

        print(skippedArray.count)
        print(SaveArray.count)
        print(OtherArray.count)
        print(SaveArray)
        
        Listsapidata()
        CreatePickerview()
        LocationLatLong()
    }
    
    func LocationLatLong() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    // MARK: -  location Manager Method
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        latvalue = locValue.latitude
        longvalue = locValue.longitude
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    // MARK: -  Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "New Contact")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "User information", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address", comment: "")]
        
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Salutation", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Contact Name", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Primary Specialty", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Phone", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Email", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 1", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 2", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Country", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "State", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "City", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Zip/Postal Code", comment: "")]
        
    }
    
    // MARK: - Create Pickerview
    
    func CreatePickerview() {
        
        self.dataPicker.removeFromSuperview()
        self.toolBar.removeFromSuperview()
        
        // DatePicker
        //  temp_textField = textField
        
        self.dataPicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        self.dataPicker.delegate = self
        self.dataPicker.dataSource = self
        self.dataPicker.backgroundColor = UIColor.white
        
        // ToolBar
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
    }
    
    // MARK: - Fetch Contact Specialty List

    fileprivate func Listsapidata()
    {
        AppDelegate.showWaitView()
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                
                AppDelegate.hideWaitView()
                self.dataList = jsonArray
            }
        }
    }
  
    // MARK: - Filter Data

    fileprivate func gettingNameFromDataList(keyName : String , index : Int) -> String
    {
        var valueName = ""
        self.dataList[0][keyName].forEach({
            if $0.1["id"].stringValue == SaveArray[index] as! String
            {
                valueName = $0.1["name"].stringValue
                return
            }
        })
        return valueName
    }
    
    // MARK: - Save Contact Button Action
    
    override func rightButtonAction(){
        
        var datavalues = [String : Any]()
        
        if SaveArray[0] as? String != ""
        {
            datavalues["salutation_id"] = SaveArray[0]
            datavalues["salutation"] = gettingNameFromDataList(keyName: "salutation", index: 0)
        }
//        else
//        {
//            self.alert("please choose salutation for contact".languageSet)
//            return
//        }
        
        if SaveArray[1] as? String != ""
        {
             datavalues["contact_name"] = SaveArray[1]
        }
        else
        {
            self.alert("please fill contact name".languageSet)
            return
        }
        
        datavalues["primary_speciality_id"] = SaveArray[2]
            
            
        datavalues["primary_speciality_name"] = gettingNameFromDataList(keyName: "speciality", index: 2)
   
        datavalues["phone"] = SaveArray[3]
        datavalues["email"] = SaveArray[4]
        datavalues["address1"] = SaveArray[5]
        datavalues["address2"] = SaveArray[6]
        datavalues["city"] = SaveArray[9]
        datavalues["zipcode"] = SaveArray[10]
        datavalues["country_id"] = SaveArray[7]
        datavalues["is_express"] = true
        datavalues["is_editable"] = true
        datavalues["country_name"] = gettingNameFromDataList(keyName: "country", index: 9)
        datavalues["state_id"] = SaveArray[8]
        datavalues["state_name"] = gettingNameFromDataList(keyName: "state", index: 8)
         datavalues["AttachmentFiles"] = []
        datavalues["lat"] = latvalue
        datavalues["long"] = longvalue
        datavalues["created_date"] = Int(Date().timeIntervalSince1970)
        
        AddContactRequest(datavalues, Method: "/contactapp_createexpress")
        
    }
    
    // MARK: - Offline Create Contact
    
    fileprivate func OfflineCreateContact (parama : [String : Any])
    {
        let userdefault = UserDefaults.standard
        var id : Int = 30000
        if userdefault.value(forKey: "OfflineNewContactCreate") as? Int != nil
        {
            let tempid = userdefault.value(forKey: "OfflineNewContactCreate") as! Int
            
            id = tempid + 1
            
        }
       
        var param  = [String:Any]()
        param = parama
        param["id"] = 0
        param["temp_id"] = Int(Date().timeIntervalSince1970)
        let dict = JSON(param)
        
        DataBaseHelper.ShareInstance.AddContact(Object: dict,NewType : true){status in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert("something went wrong".languageSet)
                return
            }
            userdefault.set(id, forKey: "OfflineNewContactCreate")
            userdefault.synchronize()
            self.navigationController?.popViewController(animated: true)
            self.delegate?.contactdetails(name: "", Contact_id: 0)
                        
        }
        print(param)
    }
    
    // MARK: - Add Contact Request
    
    fileprivate func AddContactRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
           self.OfflineCreateContact(parama: param)
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                if let contactJson = info
                {
                    ///// new coredata code
                    DataBaseHelper.ShareInstance.DeleteContext(ClassName: OfflineDataClass.Contact.rawValue)
                    
                    contactJson.forEach({
                        DataBaseHelper.ShareInstance.AddContact(Object: $0)
                    })
                                
                }
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.contactdetails(name: "", Contact_id: 0)
                    
                    }, nil])
            }
        }
    }
}

// MARK: - Table View Delegate/DataSource Methods
extension ExpressContactVC: UITableViewDelegate,UITableViewDataSource
{
    
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 5
            
        case 1:
            return 6
            
        default:
            return 0
        }
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        //  label.backgroundColor = UIColor.yellow
        
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 75
    }
    
    func ShowDropDownImage() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
    }
    
    func HideDropDownImage() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
                        
            if indexPath.row == 0 || indexPath.row == 1
            {
                cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
            }
            else
            {
                let str2 = arrTitle[indexPath.row] as? String
                
                cell.lbtitleAC.text = str2!.replacingOccurrences(of: "*", with: "", options:
                    NSString.CompareOptions.literal, range: nil)
            }
            
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            if OtherArray[indexPath.row] as! String == "1"
            {
                cell.accessoryView = ShowDropDownImage()
                self.pickUpDate(cell.txtTitleAC)
                
            }
            else
            {
                cell.accessoryView = HideDropDownImage()
            }
            if indexPath.row == 3
            {
                cell.txtTitleAC.keyboardType = .default
            }
            else if indexPath.row == 4
            {
                cell.txtTitleAC.keyboardType = .emailAddress
            }
            else
            {
                cell.txtTitleAC.keyboardType = .default
            }
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAddress") as! AddAddress
            
            let str2 = arrTitle[indexPath.row+5] as? String
            
            cell.lbtitleAD.text = str2!.replacingOccurrences(of: "*", with: "", options:
                NSString.CompareOptions.literal, range: nil)
            
            cell.txtTitleAD.text = skippedArray[indexPath.row+5] as? String
            cell.txtTitleAD.tag = indexPath.row+5
            cell.txtTitleAD.delegate = self
            cell.selectionStyle = .none
            
            if OtherArray[indexPath.row+5] as! String == "1"
            {
                self.pickUpDate(cell.txtTitleAD)
                cell.accessoryView = ShowDropDownImage()
            }
            else
            {
                cell.accessoryView = HideDropDownImage()
            }
           
                cell.txtTitleAD.keyboardType = .default
            
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ddd") as! AddContactCell
            cell.selectionStyle = .none
            cell.accessoryView = HideDropDownImage()
            return cell
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}

// MARK: - TextField Delegate Methods

extension ExpressContactVC : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){

    }
    @objc func doneClick() {
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 2
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
            
        }
                        
        else if indexvalu == 7
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
             selectedCountryId = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 8
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
        }
            
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        //  temp_textField.resignFirstResponder()
        
        self.view .endEditing(true)
        
        print(SaveArray)
        
        mTableview.reloadData()
        
    }
    @objc func cancelClick() {
        
        self.dataPicker.reloadAllComponents()
        
        self.dataPicker.selectRow(0, inComponent: 0, animated: false)
        
        RowValue = 0
        
        //   temp_textField.resignFirstResponder()
        
        self.view .endEditing(true)
        // temp_textField.resignFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
                
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            Pickerdatavalue = [self.dataList[0]["salutation"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
            
        }
        else if textField.tag == 2
        {
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            
            Pickerdatavalue = [self.dataList[0]["speciality"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
        else if textField.tag == 7
        {
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()
        }
            
        else if textField.tag == 8
        {
            textField.inputView = self.dataPicker
            textField.inputAccessoryView = self.toolBar
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryId)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                
                print(Pickerdatavalue)
                
                self.dataPicker.reloadAllComponents()
            }
            else{
                if selectedCountryId == 0
                {
                    self.alert("Please select Country First")
                }
                else
                {
                    self.alert("There is no state available in this country")
                }
            }
            
           /* Pickerdatavalue = [self.dataList[0]["state"]]
            
            print(Pickerdatavalue)
            
            self.dataPicker.reloadAllComponents()*/
        }
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.dataPicker.removeFromSuperview()
            self.toolBar.removeFromSuperview()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        print(self.dataList)
        
        //  let currency_name = self.dataList[data]["name"].stringValue
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
                
        
        if indexvalu == 3
        {
            do {
                let regex = try NSRegularExpression(pattern:".*[^0-9- ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
        }
        
        skippedArray[textField.tag] = newString
        
        SaveArray[textField.tag] = newString
        
        print(skippedArray)
        
        return true
    }
}


// MARK: - PickerView Delegat Methods

extension ExpressContactVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue[0].count
        }
        return 0
        //return Pickerdatavalue[0].count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}


// MARK: - Class Instance
extension ExpressContactVC
{
    class func instance()->ExpressContactVC?{
        
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ExpressContactVC") as? ExpressContactVC
        
        
        return controller
    }
}
