//
//  HomeViewController.swift
//  MedMobilie
//
//  Created by dr.mac on 22/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import IoniconsKit
import MMDrawerController

class HomeViewController: InterfaceExtendedController {
    
    fileprivate var arrItems : [String]?
    fileprivate var arrItems_img : [String]?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var Top_title_LBL: UILabel!
    @IBOutlet weak var Middle_title_LBL: UILabel!
    @IBOutlet weak var Bottom_title_LBL: UILabel!
    
    @objc func NotificationView(notification: NSNotification){
        
        
        self.navigationController?.popToRootViewController(animated: false)
        
        let data = notification.object as! Dictionary<String, Any>
        
        print(data["module_type"] as! String)
        
        
        
        if data["module_type"] as! String == "bill"
        {
            guard let controller = BillsDetails.instance()
                else{return}
            controller.BillId = data["module_id"] as! Int
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if data["module_type"] as! String == "target"
        {
            guard let controller = TargetVC.instance()
                else{return}
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if data["module_type"] as! String == "visit"
        {
            guard let controller = VisitDetailsVC.instance()
                else{return}
            controller.visitId = data["module_id"] as! Int
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if data["module_type"] as! String == "task"
        {
            guard let controller = TaskDetails.instance()
                else{return}
            controller.TaskId = data["module_id"] as! Int
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if data["module_type"] as! String == "order"
        {
            guard let controller = OrderDetailsVC.instance()
                else{return}
            controller.ID = data["module_id"] as! Int
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        else if data["module_type"] as! String == "chat"
        {
            guard let controller = ChatController.instance()
                else{return}
            controller.userid = data["module_id"] as! Int
            controller.UserName = "Chat"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Bottom_title_LBL.text =  ""
        
        var parama = [String:Any]()
        parama["user_id"] = 0
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Check_Chat_ID"), object: parama)
        
        arrItems_img = ["Plan My Day","Visit","Task","Team","Bills","Target","Orders","Route","Reports","Message","Contact","product"]
        
        // self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.LanguageSet()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SwtichToOrderList), name: NSNotification.Name( "SwtichToOrderList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationView), name: NSNotification.Name( "NotificationView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(todaypending), name: NSNotification.Name( "todaypending"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SubMitReport), name: NSNotification.Name( "SubMitReport"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(SwtichToVisitPAGE), name: NSNotification.Name( "SwtichToVisitPAGE"), object: nil)
        self.tabBarController?.navigationController?.navigationBar.isHidden = true
        //paintNavigationTitle(title : "Home".languageSet,Color : .black)
        
        PaintleftSideMenu()
        //PaintNavigationBar(TitleColor: UIColor.black, BackgroundColor:WhiteColor() , BtnColor: UIColor.black)
        self.NavBarIcon()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        paintNavigationTitle(title : "Home".languageSet,Color : .black)
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    @objc func SwtichToOrderList() {
        self.navigationController?.viewControllers.removeAll(where:{ (vc) -> Bool in
            if vc.isKind(of: HomeViewController.self) {
                return false
            }
            else {
                return true
            }
        })
        
        guard let controller = OrderListVC.instance()
            else{return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func todaypending() {
        //        self.navigationController?.viewControllers.removeAll(where:{ (vc) -> Bool in
        //            if vc.isKind(of: HomeViewController.self) {
        //                return false
        //            }
        //            else {
        //                return true
        //            }
        //        })
        
        self.navigationController?.popToRootViewController(animated: false)
        
        guard let controller = DailyVisit.instance()
            else{return}
        controller.Titlename = "Pending Visit"
        controller.TodayPending = true
        self.navigationController?.pushViewController(controller, animated: true)
        
        print("Pending Visit")
        
    }
   
    @objc func SwtichToVisitPAGE() {
        
        self.navigationController?.popToRootViewController(animated: false)
        
        guard let controller = DailyVisit.instance()
            else{return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        // self.tabBarController?.navigationItem.title = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Home", comment: "")
        
        // paintNavigationTitle(title : "Home",Color : .black)
        PaintNavigationBar(TitleColor: UIColor.black, BackgroundColor:WhiteColor() , BtnColor: UIColor.black)
                
        DispatchQueue.main.async {
            self.DashBoard()
        }
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func DashBoard()
    {
        if !NetworkState.isConnected() {
            // AppDelegate.hideWaitView()
            // AppDelegate.alertViewForInterNet()
            return
        }
        NotificationFetch_Api().FetchDashBoardData { (status, message, data) in
            // AppDelegate.hideWaitView()
            if !status
            {
                self.Bottom_title_LBL.text = ""
                return
            }
            
            guard let infos = data?[0]  else{return}
            
            self.Bottom_title_LBL.text =  infos["welcometext"].stringValue
            
        }
    }
    
    @objc func OpenSlider(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerContainer?.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    @objc override func LanguageSet(){
        
        guard let info = SignedUserInfo.sharedInstance else{return}
        Top_title_LBL.text = "\(languageKey(key: "Hi")) \(info.username ?? "")"
        
        Middle_title_LBL.text = "\(languageKey(key: "Have a good Vibe."))"
        
        // Bottom_title_LBL.text = "\(languageKey(key: "You have 5 task for today."))"
        
        tabBarController?.tabBar.items?[0].title = languageKey(key: "HOME")
        tabBarController?.tabBar.items?[1].title = languageKey(key: "MESSAGE")
        
        tabBarController?.tabBar.items?[2].title = languageKey(key: "CONTACTS")
        tabBarController?.tabBar.items?[3].title = languageKey(key: "PRODUCT")
        
        tabBarController?.tabBar.items?[4].title = languageKey(key: "CHECK IN")
        
        arrItems = [languageKey(key: "Plan My Day"),languageKey(key: "Visit"),languageKey(key: "Task"),languageKey(key: "Team"),languageKey(key: "Bills"),languageKey(key: "Target"),languageKey(key: "Orders"),languageKey(key: "Route"),languageKey(key: "Reports"),languageKey(key: "Message"),languageKey(key: "Contacts"),languageKey(key: "product")]
        collectionView.reloadData()
    }
    
    
}

extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrItems?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdentifier", for: indexPath) as! CustomCollectionViewCell
        
        
        cell.backView.dropShadow(color: .lightGray, cornerRadius: 10, opacity: 0.6, offSet: CGSize(width: 0, height: 0), radius: 3)
        cell.imgView.image = UIImage(named: "\(arrItems_img![indexPath.row])")
        cell.lblTitle.text = arrItems?[indexPath.row].uppercased()
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "headerView",for: indexPath)
            return headerView
        default:
            return UICollectionReusableView ()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = collectionView.frame.size.width
        return CGSize(width: width/3, height: (width/3)-26)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //      NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
        
        
        
        //indexvalue = 0
        
        switch indexPath.row {
        case 0:
            
            if !NetworkState.isConnected() {
                
                guard let obj_PlanMyDay = PlanMyDaySearchOffline.instance()
                    else{return}
                self.navigationController?.pushViewController(obj_PlanMyDay, animated: true)
                
            }
            else
            {
                
                guard let obj_PlanMyDay = PlanMyDaySearch.instance()
                    else{return}
                self.navigationController?.pushViewController(obj_PlanMyDay, animated: true)
                
            }
            
            break
            
            
        case 1:
            if !NetworkState.isConnected()
            {
                
                guard let controller = OfflineVisitListVC.instance()
                    else{return}
                controller.Titlename = "Visit"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            else
            {
                guard let controller = DailyVisit.instance()
                    else{return}
                controller.Titlename = "Visit"
                self.navigationController?.pushViewController(controller, animated: true)
            }
            break
            
        case 2:
            if !NetworkState.isConnected() {
                //guard controller =
                
                guard let controller = OfflineTaskListVC.instance()
                    else{return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                break
                
            }
            else
            {
                guard let controller = TaskListVC.instance()
                    else{return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                
                break
                
            }
            
            
            
        case 3:
            guard let obj_TeamVC = TeamVC.instance()
                else{return}
            
            obj_TeamVC.ClassName = ""
            self.navigationController?.pushViewController(obj_TeamVC, animated: true)
            break
        case 4 :
            guard let obj_TeamVC = BillsVc.instance()
                else{return}
            self.navigationController?.pushViewController(obj_TeamVC, animated: true)
            break
        case 5:
            guard let controller = TargetVC.instance()
                else{return}
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
        case 6:
            guard let controller = OrderListVC.instance()
                else{return}
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
            
        case 7 :
            guard let controller = RouteVC.instance()
                else{return}
            self.navigationController?.pushViewController(controller, animated: true)
            
        case 8:
            
            guard let controller = Reports.instance()
                else{return}
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
            
            
        case 9:
            
            self.tabBarController?.selectedIndex = 1
            
            break
            
        case 10:
            
            self.tabBarController?.selectedIndex = 2
            break
            
        case 11:
            
            self.tabBarController?.selectedIndex = 3
            break
            
        default:
            break
        }
    }
    
    
    @objc func SubMitReport()
    {
        
        self.navigationController?.popToRootViewController(animated: false)
        
        guard let controller = Reports.instance()
            else{return}
        controller.SubMitReport = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

// MARK: - Class Instance
extension HomeViewController
{
    class func instance()->HomeViewController?{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        
        
        return controller
    }
}


