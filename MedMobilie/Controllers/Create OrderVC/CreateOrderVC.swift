//
//  CreateOrderVCViewController.swift
//  MedMobilie
//
//  Created by dr.mac on 08/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit
import SwiftyJSON
import IoniconsKit
import SDWebImage
import DGElasticPullToRefresh
import iOSDropDown
import Stripe
class CreateOrderVC:InterfaceExtendedController,UITextFieldDelegate {
    
     var OrderDataValues = [String : Any]()
    var ProductCartData : [JSON]?
    fileprivate let sectionCount = 2
    var datePicker : UIDatePicker!
   fileprivate var temp_textField = UITextField()
    @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var ContactTXT: UITextField!
    var contactFlag : Bool = false
    var contact_id : Int = 0
    var ContactName  = [String]()
    
    var Contactname : String = ""
    var contactId = [Int]()
    var flagsCheck = false // come From productDetails screen
    var totalValue : Double = 0.0
    var selectedContact_id : Int? = 0
    var comingFromCartBtn : Bool = false
    fileprivate var order_id : Int? = 0
    
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         print("click")
        
        guard let objContactsVC = SelectContactVC.instance() else{return}
        objContactsVC.ClassType = "Express"
        
        objContactsVC.SelectedContactDetail = { param in
            let json = JSON(param)
            
            
            self.ContactTXT.text = "\(json["salutation"].stringValue) " + json["contact_name"].stringValue
            
            print("Data Value:----->",param)
            
            
             self.Contactname = "\(json["salutation"].stringValue) " + json["contact_name"].stringValue

            self.selectedContact_id = json["id"].intValue

            
        }
        self.navigationController?.pushViewController(objContactsVC, animated: true)
        
        
        
        textField.resignFirstResponder()
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
      ContactTXT.delegate = self
        ContactTXT.text = ""
        ContactTXT.placeholder = "Contact".languageSet
        
//        dropDown.placeholder = "Contact Name".languageSet
//        dropDown.setImageRightPaddingPoints(40,img : .iosContact )
//        dropDown.isUserInteractionEnabled = false
//        dropDown?.didSelect{(selectedText , index ,id) in
//            self.selectedContact_id = id
//            self.Contactname = selectedText
//        }
        
        
        if !flagsCheck
        {
            AppDelegate.showWaitView()
        self.ProductCartDataFetch()
        }
        else
        {
            
            if !NetworkState.isConnected() {
                AppDelegate.hideWaitView()
                
                self.FetchingOfflineData()
                
            }
            else
            
            {
                
                if self.contact_id != 0
                {
                AppDelegate.showWaitView()
                self.contactListRequest()
                }
                
            }
            
            
            
        
            
        }
        
      
        
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        self.LanguageSet()
        
    }
    
    
    fileprivate func FetchingOfflineData()
    {
        if !NetworkState.isConnected() {
            
            DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.Contact.rawValue) { (data) in
                AppDelegate.hideWaitView()
                if let object = data
                {
                    var temp = [JSON]()
                    for jsonData in object
                    {
                        if let jsondata = jsonData.value(forKey: "json") as? Data {
                            
                            jsondata.retrieveJSON(completion: { (json) in
                                temp.append(json!)
                            })
                
                        }
                    }
                    
                   // guard let temp = jsonArray else{return}

                    
                    var index_value = 0
                    for index in 0...temp.count - 1
                    {
                        if self.contact_id != 0
                        {
                            if  temp[index]["id"].intValue == self.contact_id
                            {
                                index_value = index
                                
                            }
                        }
                        
                        self.ContactName.append("\(temp[index]["salutation"].stringValue) \(temp[index]["contact_name"].stringValue)")
                        //self.ContactName.append(temp[index]["contact_name"].stringValue)
                        if temp[index]["id"].intValue != 0
                        {
                            self.contactId.append(temp[index]["id"].intValue)
                        }
                        else
                        {
                             self.contactId.append(temp[index]["temp_id"].intValue)
                        }
                       
                        
                    }
//                    self.dropDown.optionArray = self.ContactName
//                    self.dropDown.isUserInteractionEnabled = true
//                    self.dropDown.optionIds = self.contactId
                    
                    if self.contactFlag
                    {
//                        self.dropDown.text = "\(temp[index_value]["salutation"].stringValue) \(temp[index_value]["contact_name"].stringValue)"
//                        self.dropDown.selectedIndex = index_value
//
//                        if temp[index_value]["id"].intValue != 0
//                        {
//                            self.selectedContact_id = temp[index_value]["id"].intValue
//                        }
//                        else
//                        {
//                            self.selectedContact_id = temp[index_value]["temp_id"].intValue
//                        }
                        }
                    
                }
                // self.dropDown.selectedIndex =
                print(self.ContactName)
                }
          
            
                
                    
                    
                }
            }
    
        
    
    
    fileprivate func ProductCartDataFetch()
    {
        
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ProductCartListingFetch().Fetch(completion: { (status, message,data) in
            
            if !status
            {
                self.alert(message)
                return
            }
            
            self.contactListRequest()
            self.ProductCartData = data
            self.tableView.reloadData()
            //self.tableView.dg_stopLoading()
        })
        
    }
    
    fileprivate func contactListRequest()
    {
        
        
        
        guard  let infos = DataBaseHelper.ShareInstance.DataOfContactWithID(Id: self.contact_id , type: .Fetch) else{
            
             AppDelegate.hideWaitView()
            return
        }
        
     
        self.Contactname = infos["salutation"].stringValue + " " + infos["contact_name"].stringValue
        
        self.ContactTXT.text = self.Contactname
        
        self.selectedContact_id = infos["id"].intValue

        
        AppDelegate.hideWaitView()

//
//
//
//        if !NetworkState.isConnected() {
//            AppDelegate.hideWaitView()
//            AppDelegate.alertViewForInterNet()
//            return
//        }
//        DataFetchWithParam_Api().Request(parameter: nil, link: "/contactrestrictedlist") { (status, message, data) in
//            AppDelegate.hideWaitView()
//            if !status
//            {
//                self.alert(message)
//                return
//            }
//            guard let temp = data else{return}
//            var index_value = 0
//            for index in 0...temp.count - 1
//            {
//                if self.contact_id != 0
//                {
//                    if  temp[index]["id"].intValue == self.contact_id
//                    {
//                        index_value = index
//
//                    }
//                }
//                self.ContactName.append("\(temp[index]["salutation"].stringValue) \(temp[index]["contact_name"].stringValue)")
//                //self.ContactName.append(temp[index]["contact_name"].stringValue)
//                self.contactId.append(temp[index]["id"].intValue)
//
//            }
////            self.dropDown.optionArray = self.ContactName
////            self.dropDown.isUserInteractionEnabled = true
////            self.dropDown.optionIds = self.contactId
//
//            if self.contactFlag
//            {
////                self.dropDown.text = "\(temp[index_value]["salutation"].stringValue) \(temp[index_value]["contact_name"].stringValue)"
////                self.dropDown.selectedIndex = index_value
////                self.selectedContact_id = temp[index_value]["id"].intValue
//            }
//
//            }
//           // self.dropDown.selectedIndex =
//            print(self.ContactName)
//
        
            
        }
    
    
    
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Create Order")
        
        
    }
    
    
    
    
    
}
extension CreateOrderVC : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if section == 0
       {
        return ProductCartData?.count ?? 0
        }
//       else if section == 1 {
//        return 1
//        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       /* if indexPath.section == 1
        {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactList") as? CreateOrderCell1 else{return CreateOrderCell1()}
           /* cell.dateIcon?.text = String.ionicon(with: .calendar)
            cell.timeIcon?.text = String.ionicon(with: .clock)
            cell.lblDate?.delegate = self
            cell.lblTime?.delegate = self
             self.pickUpDate(cell.lblDate)
             self.pickUpDate(cell.lblTime)
             cell.lblDate.text = self.dateStr
             cell.lblTime.text = self.timeStr
            */
           
            cell.dropDown?.optionArray = self.ContactName ?? ["a"]
            cell.dropDown?.optionIds = self.contactId ?? [0]
           
            
            cell.dropDown?.didSelect{(selectedText , index ,id) in
                print ("Selected String: \(selectedText) \n index: \(index)")
            }
            return cell
        }*/
        if indexPath.section == 0
        {
            
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductList", for: indexPath) as? ShowCartCell
            else{return ShowCartCell()}
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)

        guard let infos = ProductCartData?[indexPath.row] else{return cell}
//        cell.viewShadow?.bottomViewShadow()
            guard let symbol = SignedUserInfo.sharedInstance?.currency_symbol else{return cell}
            let price = calculation(info: infos)
            
            cell.Price?.text = "\(symbol) \(String(format: "%.2f", price))"
        cell.Units?.text = "\(languageKey(key: "No. Of Cases:")) \(infos["no_of_cases"].stringValue)"
        cell.lblProductName?.text = infos["product_name"].stringValue
        
        
        cell.imgProductimg?.sd_setImage(with: infos["product_image"].url, placeholderImage: UIImage(imageLiteralResourceName: "Product_PH"), options:.cacheMemoryOnly, completed: nil)
        //cell.imgProductimg?
        
        // cell.viewShadow?.backgroundColor = navBackgroundColor
        
        // cell.lblTask?.text = numbers[indexPath.row]
        
        cell.selectionStyle = .none
        
        return cell
    }
        return ShowCartCell()
    }
    func calculation(info : JSON) -> Double
    {
        let temp = info["discountpercase"].doubleValue.round(to: 2)
       // let discount = Double(temp ?? 0)
       let price = info["pricepercase"].doubleValue.round(to: 2)
        let cases = Double(info["no_of_cases"].intValue)
        
        let result = (price - temp) * cases
        
        return result
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return UITableView.automaticDimension
        }
        else if indexPath.section == 1
        {
            return UITableView.automaticDimension
        }
        return 0
    }
    
    
    
    
}



extension CreateOrderVC
{
    class func instance()->CreateOrderVC?{
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateOrderVC") as? CreateOrderVC
        
        
        return controller
    }
    
    
    
}
extension CreateOrderVC
{
    @IBAction fileprivate func btnAddContactAction(_ sender : UIButton)
    {
        guard let controller = AddContactVC.instance()
        else{
            return
        }
        //controller.delegate = self
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction fileprivate func btnOrderNowAction(_ sender : UIButton)
    {
        if Contactname != ""
        {
        
                        if totalValue <= 0.0
                        {
                            if flagsCheck
                            {
                                CreateSingleOrder(url : "/ordercreatesingle")
                            }
                            else
                            {
                                CreateSingleOrder(url : "/ordercreate")
                            }
                            }
                        else
                        {
                            if !NetworkState.isConnected() {
                                
                                
                                let actionsheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                                
                                actionsheet.addAction(UIAlertAction(title: "Pay Later".languageSet, style: UIAlertAction.Style.default, handler: {action1 in
                                    
                                    self.OrderDataValues["contact_name"] = self.Contactname
                                    self.OrderDataValues["contact_id"] = 0
                                    self.OrderDataValues["temp_id"] =  self.selectedContact_id
                                    
                                    self.OrderDataValues["created_date"] = Int(Date().timeIntervalSince1970)
                                    self.OrderDataValues["status"] = 1
                                    self.OrderDataValues["order_price"] = self.totalValue
                                    self.OrderDataValues["order_total"] = self.totalValue
                                    self.OrderDataValues["order_no"] = ""
                                    
                                    
                                    
                                    let infos = self.ProductCartData?[0]
                                    self.OrderDataValues["product_id"] = infos!["product_id"].intValue
                                    self.OrderDataValues["product_price"] = "\(infos!["pricepercase"].doubleValue.round(to: 2))"
                                    
                                    self.OrderDataValues["no_of_cases"] = "\(infos!["no_of_cases"].stringValue)"
                                    
                                    
                                    self.OrderDataValues["no_of_cases"] = "\(infos!["no_of_cases"].stringValue)"
                                    
                                    
                                    ////bhushan
                                    
                                    self.OrderDataValues["discountpercase"] = infos!["discountpercase"].doubleValue
                                    
                                    self.OrderDataValues["product_name"] = infos!["product_name"].stringValue
                                    
                                    
                                    let ids : Int = 888
                                    
                                    
                                    print(self.OrderDataValues)
                                    let dict = JSON(self.OrderDataValues)
                                    
                                    
                                    print(dict)
                                    DataBaseHelper.ShareInstance.CreateOrder(OrderData: dict, OrderId: ids){ status in
                                        
                                        if !status
                                        {
                                            self.alert("something Went wrong")
                                            return
                                        }
                                        
                                        self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: "Order Data has been inserted successfully".languageSet, actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                                            
                                            
                                            
                                            self.tabBarController?.selectedIndex = 0
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwtichToOrderList"), object: nil)
                                            
                                            }, nil])
                                    }
                                    
                                    
                                    
                                    
                                }))
                                
                                actionsheet.addAction(UIAlertAction(title: LocalizationSystem.SharedInstance.localizedStingforKey(key: "Dismiss", comment: ""), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
                                    
                                }))
                                
                                if UI_USER_INTERFACE_IDIOM() != .phone {
                                    actionsheet.popoverPresentationController?.permittedArrowDirections = .any
                                    
                                    actionsheet.popoverPresentationController?.sourceView = sender
                                    actionsheet.popoverPresentationController?.sourceRect = sender.bounds
                                }
                                // self.presentViewController(alertController, animated: true, completion: nil)
                                
                                
                                
                                actionsheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                                
                                self.present(actionsheet, animated: true) {
                                    print("option menu presented")
                                }
                                
                                
                            }
                            else
                            
                            {


                                
                                
                                let actionsheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
                                
                                actionsheet.addAction(UIAlertAction(title: "Pay Later".languageSet, style: UIAlertAction.Style.default, handler: { (action) -> Void in
                                    if self.flagsCheck
                                    {
                                        self.CreateSingleOrder(url : "/ordercreatesingle")
                                    }
                                    else
                                    {
                                        self.CreateSingleOrder(url : "/ordercreate")
                                    }
                                    
                                }))
                                
                                actionsheet.addAction(UIAlertAction(title: "Pay Now".languageSet, style: UIAlertAction.Style.default, handler: { (action) -> Void in
                                    
                                    self.paymentProcees()

                                    
                                }))
                                actionsheet.addAction(UIAlertAction(title: LocalizationSystem.SharedInstance.localizedStingforKey(key: "Dismiss", comment: ""), style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
                                    
                                }))
                                
                                if UI_USER_INTERFACE_IDIOM() != .phone {
                                    actionsheet.popoverPresentationController?.permittedArrowDirections = .any
                                   
                                    actionsheet.popoverPresentationController?.sourceView = sender
                                    actionsheet.popoverPresentationController?.sourceRect = sender.bounds
                                }
                                // self.presentViewController(alertController, animated: true, completion: nil)
                                
                                
                                
                                actionsheet.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                                
                                self.present(actionsheet, animated: true) {
                                    print("option menu presented")
                                }
                                
                                

                          
                }
        
            }
        }
        else
        {
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Please select a contact to continue"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                
                return
                }, nil])
           
            return
        }
    }
    
    
    

    
    
    
    
    
    fileprivate func paymentProcees()
    {
        let addCardViewController = STPAddCardViewController()
        
        addCardViewController.delegate = self
        PaintNavigationBar(TitleColor: .black, BackgroundColor: .white, BtnColor: .black)
        self.navigationController?.pushViewController(addCardViewController, animated: true)
    }
    
    fileprivate func CreateSingleOrder(url : String)
    {
         AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        var parama = [String:Any]()
        if flagsCheck
        {
        guard let singleJson = self.ProductCartData?[0] else{return}
        parama = getParam(jsonData: singleJson)
        }
        
        parama["contact_id"] = self.selectedContact_id ?? 0
        parama["order_total"] = totalValue
        
        
        CreateSingleOrder_Api().Request(parameter : parama, link: url) { (status, message , order_id) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            self.order_id = order_id
            
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
           //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
                
                self.tabBarController?.selectedIndex = 0
                //self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwtichToOrderList"), object: nil)
                
                let VCArray = self.navigationController?.viewControllers
                self.navigationController?.viewControllers.removeAll(where: {(VC) -> Bool in
                    
                    if VC == VCArray?.first
                    {
                        return false
                    }
                    else
                    {
                        return true
                    }
                    
                })
//               if !self.comingFromCartBtn
//               {
//                if !self.contactFlag
//                {
//                self.navigationController?.viewControllers.removeAll(where:{ (vc) -> Bool in
//                    if vc.isKind(of: ProductListingVC.self) {
//                        return false
//                    }
//                    else {
//                        return true
//                    }
//                    })
//                }
//                else
//                {
//
//                    self.navigationController?.viewControllers.removeAll(where:{ (vc) -> Bool in
//                        if vc.isKind(of: ContactsVC.self) {
//                            return false
//                        }
//                        else {
//                            return true
//                        }
//                    })
//                }
//                }
//                else
//                {
//                    let VCArray = self.navigationController?.viewControllers
//
//
//                    for VC in 0..<VCArray!.count - 1
//                    {
//                        self.navigationController?.viewControllers.remove(at: VC)
//                    }
//
//                }
                return
                }, nil])
                
           
        }
        
    }
    
    fileprivate func getParam(jsonData : JSON) -> [String : Any]
    {
       
        var param = [String : Any]()
        param["product_id"] = jsonData["product_id"].intValue
        param["no_of_cases"] = Int(jsonData["no_of_cases"].stringValue)
        param["discountpercase"] = jsonData["discountpercase"].doubleValue.round(to: 2)
        
        
        return param
    }
    
    
    fileprivate func getParamOffLine(jsonData : JSON) -> [String : Any]
    {
        
        var param = [String : Any]()
        param["product_id"] = ""
        param["no_of_cases"] = 25
        param["discountpercase"] = 22.5
        
        
        return param
    }
    
    
    
   
    
    
}
extension CreateOrderVC: STPAddCardViewControllerDelegate {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController,
                               didCreateToken token: STPToken,
                               completion: @escaping STPErrorBlock) {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        PaymentByStripe(StripeTkn: token)
        
        
        
        
        
    }
    
    
    fileprivate func PaymentByStripe(StripeTkn : STPToken)
    {
        var parama = [String : Any]()
        if flagsCheck
        {
            guard let singleJson = self.ProductCartData?[0] else{return}
            parama = getParam(jsonData: singleJson)
            parama["order_type"] = "single"
        }
        else
        {
            parama["order_type"] = "cart"
        }
        
        parama["contact_id"] = self.selectedContact_id ?? 0
        parama["order_total"] = totalValue
        parama["stripe_token"] = StripeTkn
        print(parama)
        StripePayByCardNow().Request(params: parama) { (status, message) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles:["ok"], actions:[{action1 in
                    self.navigationController?.popViewController(animated: true)
                    }, nil])
                return
            }
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: message, actionTitles: ["ok"], actions:[{action1 in
                
                self.tabBarController?.selectedIndex = 0
                //self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwtichToOrderList"), object: nil)
                if !self.contactFlag
                {
                    self.navigationController?.viewControllers.removeAll(where:{ (vc) -> Bool in
                        if vc.isKind(of: ProductListingVC.self) {
                            return false
                        }
                        else {
                            return true
                        }
                    })
                }
                else
                {
                    self.navigationController?.viewControllers.removeAll(where:{ (vc) -> Bool in
                        if vc.isKind(of: ContactsVC.self) {
                            return false
                        }
                        else {
                            return true
                        }
                    })
                }
                
                return
                }, nil])
            
            
        }
    }
}
extension CreateOrderVC : AddContactsToCreateOrderProtocol
{
    func contactdetails(name: String, Contact_id: Int) {
        
        
    }
    
    
}
