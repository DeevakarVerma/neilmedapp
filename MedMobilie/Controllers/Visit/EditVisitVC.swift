

import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit
import FontAwesome_swift


class EditVisitVC: InterfaceExtendedController {
    fileprivate var jsonMain : [JSON]?
    fileprivate var jsonAddress : [String : JSON]?
    fileprivate var jsonContact : [JSON]?
    fileprivate var jsonAttachment : [JSON]?
    fileprivate var selectedContact_array : [String]?
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var selectedCountryId : Int = 0
    fileprivate var ContactName  = [String]()
    fileprivate var contactId = [Int]()
    fileprivate var dataList = [JSON]()
     var BackDataBlock: ((_ json : [JSON]) -> ())?
    let OtherArray : NSMutableArray =         ["1","","","","","","1","1","","1","","",""]
    let imagePicker = UIImagePickerController()
    
    var visitId : Int = 0
    var indexvalu = 0
    var RowValue = 0
    var Contact_Data_Array = [String : Any]()
    var Attach_Data_Array = [String : Any]()
    var Main_Contact_Array = NSMutableArray ()
    
    var Main_Attachment_Array = NSMutableArray ()
    var ContactTotal = 0
    var AttachmentTotal = 0
    var numberOfCell = 2
    var ContactString = String()
    var Datevalue = String()
    var Timevalue = String()
    var ImgArray = [UIImage]()
    var ImgSaveArray = [String]()
    var dataPickerView = UIPickerView()
    var Time_Picker = UIDatePicker()
    var Date_Picker = UIDatePicker()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var toolBar = UIToolbar()
    var delegate  : AddContactsToCreateOrderProtocol?
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var AttachmentArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    var skippedArray : NSMutableArray =         ["","","","","","","","","","","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","","","","","","","","","","",""]
    
    var Redline : Bool = false
    
    @IBOutlet weak var mTableview: UITableView!
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        createPickerView()
        imagePicker.delegate = self
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
        RightActionButton(Title:languageKey(key:"Update"))
        
        AppDelegate.showWaitView()
        
        Listsapidata()
        VisitListDataFetch()
        
          NotificationCenter.default.addObserver(self, selector: #selector(RfressVisitList), name: NSNotification.Name( "RfressVisitList"), object: nil)
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
    }
    
    // data fetching
    @objc fileprivate func RfressVisitList()
    {
        AppDelegate.showWaitView()
        
        Listsapidata()
        VisitListDataFetch()
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Edit Visit")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Visit", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Meeting With", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Meeting Point", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
        
//
//        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Type", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Title", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 1", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 2", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "City", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Zip/Postal Code", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Country", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "State", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Phone Number", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
        
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Type", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Title", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 1", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 2", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Country", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "State", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "City", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Zip/Postal Code", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Phone Number", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
       
        
    }
    
    // MARK: - Fetch Visit Data

    fileprivate func VisitListDataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetch_Api().Fetch(ID: visitId ,urlString: "/visitdetail") { (status, message,data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.mTableview.dg_stopLoading()
                self.alert(message)
                return
            }
            if data != nil
            {
                self.jsonMain = data
                
                guard let tempjsonContact =  self.jsonMain?[0]["visit_contact"].arrayValue else{return}
                
                self.jsonContact = tempjsonContact
                
                
                print(self.jsonContact?.count as Any)
                
                guard let tempjsonAttachment =  self.jsonMain?[0]["visit_attachment"].arrayValue else{return}
                self.jsonAttachment = tempjsonAttachment
                
                print(self.jsonAttachment?.count as Any)
                
                guard let tempjsonAddress =  self.jsonMain?[0]["visit_address"].dictionaryValue else{return}
                self.jsonAddress = tempjsonAddress
                
                print("visit_address :---->",self.jsonAddress as Any)
                
                print(self.jsonAddress?.count as Any)
                print(self.jsonAddress as Any)
                
                self.ContactTotal = (self.jsonContact?.count)!
                self.AttachmentTotal = (self.jsonAttachment?.count)!
                
                 self.Main_Contact_Array.removeAllObjects()
    
                // checked.
                for index in 0..<(self.jsonContact!.count) {
                    
                     let infos = self.jsonContact?[index]
                    
                    print("ContactDeatils :---->",infos as Any)
                    
                    self.Contact_Data_Array = ["salutation":infos!["salutation"].stringValue,"is_express":infos!["is_express"].int ?? 0,"salutation_id": infos!["salutation_id"].intValue,"is_editable" : infos!["is_editable"].boolValue,"contact_title_id": infos!["contact_title_id"].intValue,"category_id": infos!["category_id"].intValue,"state_id" : infos!["state_id"].intValue,"shipping_state_id": infos!["shipping_state_id"].intValue, "lead_status_id": infos!["lead_status_id"].intValue,"billing_country_id": infos!["billing_country_id"].intValue, "billing_state_id": infos!["billing_state_id"].intValue,"secondary_speciality_id": infos!["secondary_speciality_id"].intValue, "primary_speciality_id": infos!["primary_speciality_id"].intValue,"shipping_country_id": infos!["shipping_country_id"].intValue,"country_id": infos!["country_id"].intValue,"contact_name":infos!["contact_name"].stringValue,"attn_name":infos!["attn_name"].stringValue,"category_name":infos!["category_name"].stringValue,"primary_speciality_name": infos!["primary_speciality_name"].stringValue,"secondary_specialities_name": infos!["secondary_specialities_name"].stringValue,"contact_title_name":infos!["contact_title_name"].stringValue,"company_name":infos!["company_name"].stringValue,"phone":infos!["phone"].stringValue,"email":infos!["email"].stringValue,"website":infos!["website"].stringValue,"lead_status_name":infos!["lead_status_name"].stringValue,"lead_source":infos!["lead_source"].stringValue,"willingtostock":infos!["willingtostock"].stringValue,"address1":infos!["address1"].stringValue,"address2":infos!["address2"].stringValue,"city":infos!["city"].stringValue,"zipcode":infos!["zipcode"].stringValue,"country_name":infos!["country_name"].stringValue,"state_name":infos!["state_name"].stringValue,"fax":infos!["fax"].stringValue,"type":infos!["type"].stringValue,"default_billing_address":infos!["default_billing_address"].stringValue,"default_shipping_address":infos!["default_shipping_address"].stringValue,"billing_address1":infos!["billing_address1"].stringValue,"billing_address2":infos!["billing_address2"].stringValue,"billing_city":infos!["billing_city"].stringValue,"billing_zipcode":infos!["billing_zipcode"].stringValue,"billing_country_name":infos!["billing_country_name"].stringValue,"billing_state_name":infos!["billing_state_name"].stringValue,"billing_fax":infos!["billing_fax"].stringValue,"shipping_address1":infos!["shipping_address1"].stringValue,"shipping_address2":infos!["shipping_address2"].stringValue,"shipping_city":infos!["shipping_city"].stringValue,"shipping_zip_code":infos!["shipping_zip_code"].stringValue,"shipping_country_name":infos!["shipping_country_name"].stringValue,"shipping_state_name":infos!["shipping_state_name"].stringValue,"shipping_fax":infos!["shipping_fax"].stringValue,"id":infos!["id"].intValue,"contact_id":infos!["contact_id"].intValue,"product_ids":infos!["product_ids"].arrayObject]
                    
            self.Main_Contact_Array.add(self.Contact_Data_Array)
                        
                    print(self.Contact_Data_Array)
                    
                  }
                
                // checked.
                for index in 0..<(self.jsonAttachment!.count) {
                    
                    let json = self.jsonAttachment?[index]
                    self.Attach_Data_Array = ["id":json!["id"].intValue,"filename":json!["filename"].stringValue,"filepath":json!["filepath"].stringValue,"index":999]
                    self.Main_Attachment_Array.add(self.Attach_Data_Array)
                    
                    print(self.Attach_Data_Array)
                   
                }
                
                self.skippedArray[0] = self.jsonMain?[0]["visit_type"].stringValue ?? ""
                self.skippedArray[1] = self.jsonMain?[0]["title"].stringValue ?? ""
                 self.skippedArray[2] = self.jsonAddress!["address1"]?.stringValue ?? ""
                 self.skippedArray[3] = self.jsonAddress!["address2"]?.stringValue ?? ""
            
                 
                 self.skippedArray[4] = self.jsonAddress!["country_name"]?.stringValue ?? ""
              //  self.selectedCountryId =
                 self.skippedArray[5] = self.jsonAddress!["state_name"]?.stringValue ?? ""
                
                self.skippedArray[6] = self.jsonAddress!["city"]?.stringValue ?? ""
                self.skippedArray[7] = self.jsonAddress!["zipcode"]?.stringValue ?? ""
                
                self.skippedArray[8] = self.jsonAddress!["phonenumber"]?.stringValue ?? ""

                self.skippedArray[9] = self.jsonMain?[0]["visit_date"].stringValue ?? ""
               
                self.skippedArray[10] = self.jsonMain?[0]["description"].stringValue ?? ""
                
                self.SaveArray[0] = self.jsonMain?[0]["visit_type_id"].stringValue ?? ""
                self.SaveArray[1] = self.jsonMain?[0]["title"].stringValue ?? ""
                self.SaveArray[2] = self.jsonAddress!["address1"]?.stringValue ?? ""
                self.SaveArray[3] = self.jsonAddress!["address2"]?.stringValue ?? ""
                self.SaveArray[4] = self.jsonAddress!["city"]?.stringValue ?? ""
                self.SaveArray[5] = self.jsonAddress!["zipcode"]?.stringValue ?? ""
                self.SaveArray[6] = self.jsonAddress!["country_id"]?.stringValue ?? ""
                self.selectedCountryId = Int(self.jsonAddress!["country_id"]?.stringValue ?? "") ?? 0
                self.SaveArray[7] = self.jsonAddress!["state_id"]?.stringValue ?? ""
                self.SaveArray[8] = self.jsonAddress!["phonenumber"]?.stringValue ?? ""

                self.SaveArray[9] = self.jsonMain?[0]["visit_date"].stringValue ?? ""
               
                 self.SaveArray[10] = self.jsonMain?[0]["description"].stringValue ?? ""
                
                self.mTableview.dg_stopLoading()
                self.mTableview.reloadData()
            }
        }
    }
    
    // MARK: - createPickerView
    func createPickerView(){
        dataPickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        dataPickerView.backgroundColor = UIColor.white
        dataPickerView.showsSelectionIndicator = true
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        if #available(iOS 13.4, *) {
            if #available(iOS 14.0, *) {
                Date_Picker.preferredDatePickerStyle = .wheels
            } else {
                Date_Picker.preferredDatePickerStyle = .wheels
            }
        } else {
            // Fallback on earlier versions
        }
                
        Date_Picker.datePickerMode = UIDatePicker.Mode.date
        Date_Picker.addTarget(self, action: #selector(datePickerChanged), for: UIControl.Event.valueChanged)
                
        Time_Picker.datePickerMode = UIDatePicker.Mode.time
        Time_Picker.addTarget(self, action: #selector(TimePickerChanged), for: UIControl.Event.valueChanged)
        
    }
    
    // MARK: - Lable Border Line

    func DashedLineBorder (title: String)-> UILabel {
        
        let ContainerView = UILabel(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 55))
        ContainerView.layer.addSublayer(Border(YourLable: ContainerView))
        ContainerView.textAlignment = .center
        ContainerView.font = UIFont.boldSystemFont(ofSize: 16)
        ContainerView.textColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        ContainerView.text = title
        return ContainerView
        
    }
    
    // MARK: - contact List Request
    
    fileprivate func contactListRequest()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        // AppDelegate.showWaitView()
        DataFetchWithParam_Api().Request(parameter: nil, link: "/contactrestrictedlist") { (status, message, data) in
          //  AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            guard let temp = data else{return}
            
            for index in 0..<temp.count
            {
                self.ContactName.append("\(temp[index]["salutation"].stringValue) \(temp[index]["contact_name"].stringValue)")
               
                self.contactId.append(temp[index]["id"].intValue)
                
            }
            self.mTableview.reloadData()
            
        }
        // self.dropDown.selectedIndex =
        print(self.ContactName)
        
    }
    
    // MARK: - Create textView
    
    func textView (Title: String)-> UITextView
    {
        let view = UITextView(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 80))
        view.delegate = self
        view.backgroundColor = OffWhiteColor()
        view.text = Title
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.gray.cgColor
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }
    
    // MARK: - Getting data from local database

    
    fileprivate func Listsapidata()
    {
        
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                self.dataList = jsonArray
                self.contactListRequest()
            }
        }
    }

    // MARK: - Update Visit Button

    override func rightButtonAction(){
        
        if ContactString != ""
        {
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":"","primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0,"contact_id":0]
            ContactString = ""
            print(ContactArray)
            
            Main_Contact_Array.add(ContactArray)
            
        }
        
        ContactString = ""
        
        if Main_Contact_Array.count == 0
        {
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Please select a contact to continue"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                
                return
                }, nil])
            
            return
        }
        else
        {
       
            var dictArray = Main_Contact_Array
            dictArray.remove("product_ids")
            print(dictArray)
            
            let jsonData = try? JSONSerialization.data(withJSONObject: dictArray, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            print(jsonString!)
            
            var datavalues = [String : Any]()
            
            datavalues["visit_type_id"] = SaveArray[0]
            datavalues["title"] = SaveArray[1]
            datavalues["address1"] = SaveArray[2]
            datavalues["address2"] = SaveArray[3]
            datavalues["country_id"] = SaveArray[4]
            datavalues["state_id"] = SaveArray[5]
            datavalues["city"] = SaveArray[6]
            datavalues["zipcode"] = SaveArray[7]
            datavalues["phonenumber"] = SaveArray[8]
            datavalues["visit_date"] = SaveArray[9]
            datavalues["description"] = SaveArray[10]
            datavalues["id"] = visitId
            datavalues["visit_contact"] = jsonString
            datavalues["AttachmentFiles"] = ImgArray
            
            print(datavalues)
            
            AddContactRequest(datavalues, Method: "/visitedit")
        }
    }
    
    // MARK: -UpdateVisit Function
    fileprivate func AddContactRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    
                    if let editedVisit = info
                    {
                        self.BackDataBlock?(editedVisit)
                    }
                    self.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVisitList"), object: nil)
                    
                    }, nil])
            }
            
        }
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension EditVisitVC: UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate
{
    
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            self.mTableview.separatorColor = UIColor.black
            return 2
            
        case 1:
            
            self.mTableview.separatorColor = UIColor.clear
            return Main_Contact_Array.count  + numberOfCell
            
        case 2:
            
            return 7
        case 3:
            
            return 1
        case 4:
            
            return Main_Attachment_Array.count + 1
        case 5:
            
            self.mTableview.separatorColor = UIColor.black
            return 1
        default:
            return 0
        }
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        //  label.backgroundColor = UIColor.yellow
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 1 {
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row < totalRows - 2 {
                
                return  100
            }
            else
            {
                return  60
            }
        }
        if indexPath.section == 5 {
            return  100
        }
        else
        {
            return 75
        }
    }
    
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
    }
    
    func HideDropDownImage() -> UIImageView {
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
           // let str2 = arrTitle[indexPath.row] as? String
            
            cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            if indexPath.row == 0
            {
            
            if skippedArray[indexPath.row] as? String == ""
            {
                
                cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                cell.txtTitleAC.layer.borderWidth = 1.0
            }
            else
            {
                cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                cell.txtTitleAC.layer.borderWidth = 0
            }
        
            }
            else
            {
                cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                cell.txtTitleAC.layer.borderWidth = 0
            }
            
            if OtherArray[indexPath.row] as! String == "1"
            {
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                
                
                self.pickUpDate(cell.txtTitleAC)
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                //cell.accessoryView = HideDropDownImage()
            }
            return cell
        }
        else  if indexPath.section == 1
        {
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                let Mycell = tableView.dequeueReusableCell(withIdentifier: "AddMoreCell") as! AddMoreCell
                
                Mycell.AddNewAction .addTarget(self, action: #selector(AddAction), for: .touchUpInside)
                
                Mycell.ChooseAction .addTarget(self, action: #selector(ChooseAction), for: .touchUpInside)
                Mycell.selectionStyle = .none
                return Mycell
                
            }
            else
            {
                if indexPath.row < totalRows - 2 {
                                        
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
                    cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                                        
                    cell.imgUser?.setImage(string: (self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "contact_name") as? String, color: nil, circular: true)
                    print("isExisting:------->",(self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "is_express") as? Int as Any)
                                        
                    cell.lblUserName.text = "\((self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "salutation") as? String ?? "") \((self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "contact_name") as? String ?? "")"
                   cell.lblMessage.text = (self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "category_name") as? String
                    cell.delegate = self
                    cell.selectionStyle = .none
                    
                    if  (self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "is_express") as? Bool ?? true
                    {
                        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
                    }
                    else
                    {
                        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
                    }
                    
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AddContact_Visite_Cell") as! AddContact_Visite_Cell
                    cell.ContactTF.backgroundColor = UIColor.clear
                    
                    
                    cell.ContactTF.tag = 100 + indexPath.row
                    cell.ContactTF.setBottomBorder(color: "#3EFE46")
                    cell.ContactTF.placeholder = "New Contact name".languageSet
                    cell.selectionStyle = .none
                    cell.ContactTF.rightViewMode = .never
                    cell.ContactTF.rightView = nil
                    cell.ContactTF.delegate = self
                    cell.ContactTF.text = ContactString
                    cell.ContactTF.isUserInteractionEnabled = true
                    return cell
                }
            }
        }
        else  if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
          //  let str2 = arrTitle[indexPath.row+2] as? String
            
            cell.lbtitleAC.text = arrTitle[indexPath.row+2] as? String
            cell.txtTitleAC.text = skippedArray[indexPath.row+2] as? String
            cell.txtTitleAC.tag = indexPath.row+2
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            if indexPath.row == 0
            {
                cell.txtTitleAC.autocapitalizationType = .words
            }
            if indexPath.row == 1
            {
                cell.txtTitleAC.autocapitalizationType = .words
            }
            
            
            if indexPath.row == 2 || indexPath.row == 3
            {
                self.pickUpDate(cell.txtTitleAC)
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                
            }
            else
            {
                
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                // cell.accessoryView = HideDropDownImage()
            }
            
            if indexPath.row == 6
            {
                cell.txtTitleAC.keyboardType = .numberPad
            }
            else
            {
                cell.txtTitleAC.keyboardType = .default
                
            }
            
                print(indexPath.row)
                
                if indexPath.row != 1 && indexPath.row != 6
                {
                    
                    if skippedArray[indexPath.row+2] as? String == ""
                    {
                        
                        cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 0
                    }
                }
                else
                    
                {
                    cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 0
                }
            
            return cell
            
        }
        else  if indexPath.section == 3
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            cell.lbtitleAC.text = arrTitle[indexPath.row+9] as? String
            cell.txtTitleAC.text = skippedArray[indexPath.row+9] as? String
            cell.txtTitleAC.tag = indexPath.row+9
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            if OtherArray[indexPath.row+9] as! String == "1"
            {
                self.pickUpDate(cell.txtTitleAC)
                if indexPath.row == 0
                {
                    
                    cell.txtTitleAC.rightViewMode = .always
                    cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosCalendarOutline)
                    
                    //  cell.accessoryView = ShowDropDownImage(Image: .iosCalendarOutline)
                }
                else
                {
                    cell.txtTitleAC.rightViewMode = .always
                    cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosTimeOutline)
                    //  cell.accessoryView = ShowDropDownImage(Image: .iosTimeOutline)
                }
                
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                //cell.accessoryView = HideDropDownImage()
            }
            
            cell.txtTitleAC.keyboardType = .default
            
            if skippedArray[indexPath.row+9] as? String == ""
            {
                
                cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                cell.txtTitleAC.layer.borderWidth = 1.0
            }
            else
            {
                cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                cell.txtTitleAC.layer.borderWidth = 0
            }
            
            return cell
        }
            
        else  if indexPath.section == 4
        {
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                myCell.backgroundColor = UIColor.white
                
                myCell.contentView .addSubview(DashedLineBorder(title: "Attached file here".languageSet))
                myCell.selectionStyle = .none
                return myCell
            }
            else
            {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                    else
                {
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
               
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                
                cell.lblUserName.text = (self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "filename") as? String
                                    
                   // json["filename"].stringValue
                let fileEx = ((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "filepath") as? String)?.fileName()
                                
                var fileImage : FontAwesome = .file
                
                switch fileEx
                {
                case "pdf" :
                    fileImage = .filePdf
                    
                    break
                case "jpg" :
                    fileImage = .fileImage
                    break
                case "jpeg" :
                    fileImage = .fileImage
                    break
                case "doc" :
                    fileImage = .fileWord
                    break
                default :
                     fileImage = .fileImage
                    break
                    
                }
                cell.imgUser.image = UIImage.fontAwesomeIcon(name: fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
                
                 cell.delegate = self
                return cell
             
            }
            return UITableViewCell()
        }
        else  if indexPath.section == 5
        {
            
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            myCell.selectionStyle = .none
            
            myCell.contentView .addSubview(textView(Title: SaveArray[indexPath.row+10] as! String))
            return myCell
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            if indexPath.section == 1
            {
                if ((self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "id") as? Int) != 0
                {
                    self.visitDelete(Contactid:  ((self.Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "id") as? Int)!,Urlvalue: "/visitcontactdelete")
                }
                
                self.Main_Contact_Array.removeObject(at: indexPath.row)
                
            }
            else if indexPath.section == 4
            {
                if ((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "index") as? Int) == 999
                {
                    self.visitDelete(Contactid: ((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "id") as? Int)!,Urlvalue: "/visitattachmentdelete")
                     self.Main_Attachment_Array.removeObject(at: indexPath.row)
                }
                else
                {
                    print(self.Main_Attachment_Array)
                     print(((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "index") as? Int)!)
                    
                    self.ImgArray.remove(at: ((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "index") as? Int)!)
                     self.Main_Attachment_Array.removeObject(at: indexPath.row)
                    
                }
            }
            self.mTableview.reloadData()
        }
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        
        return [deleteAction]
    }
    
    fileprivate func visitDelete(Contactid:Int,Urlvalue:String)
    {
        if !NetworkState.isConnected() {
            
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ContactDelete_Api().Request(idvalue: Contactid ,url_string: Urlvalue) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    func getparamForSkippedArray(_ temp1 : NSDictionary) -> NSMutableArray
    {
        
        let jsonData = JSON(temp1)

        var productStr = ""
        var ProductToMarket_Array  = [Int]()
        
        if jsonData["product_ids"].exists()
        {
            
            for index in 0..<((jsonData["product_ids"].arrayValue).count) {
                
                let product = (jsonData["product_ids"][index])
                productStr += "\(product["product_name"])\(",")"
                            
                print(productStr)
            }
            
            if productStr.count != 0
            {
                productStr.removeLast()
            }
        }
       
       return  [temp1.object(forKey: "salutation") as? String,temp1.object(forKey: "contact_name") as? String,temp1.object(forKey: "attn_name") as? String,temp1.object(forKey: "category_name") as? String,temp1.object(forKey: "contact_title_name") as? String,productStr,temp1.object(forKey: "primary_speciality_name") as? String,temp1.object(forKey: "secondary_specialities_name") as? String,temp1.object(forKey: "company_name") as? String,temp1.object(forKey: "phone") as? String,temp1.object(forKey: "email") as? String,temp1.object(forKey: "website") as? String,temp1.object(forKey: "lead_status_name") as? String,temp1.object(forKey: "lead_source") as? String,temp1.object(forKey: "willingtostock") as? String,temp1.object(forKey: "address1") as? String,temp1.object(forKey: "address2") as? String,temp1.object(forKey: "country_name") as? String,temp1.object(forKey: "state_name") as? String,temp1.object(forKey: "city") as? String,temp1.object(forKey: "zipcode") as? String,temp1.object(forKey: "fax") as? String,temp1.object(forKey: "type") as? String,temp1.object(forKey: "default_billing_address") as? String,temp1.object(forKey: "default_shipping_address") as? String,temp1.object(forKey: "contact_id") as? Int,temp1.object(forKey: "billing_address1") as? String,temp1.object(forKey: "billing_address2") as? String,temp1.object(forKey: "billing_country_name") as?   String,temp1.object(forKey: "billing_state_name") as?  String,temp1.object(forKey: "billing_city") as? String,temp1.object(forKey: "billing_zipcode") as? String,temp1.object(forKey: "billing_fax") as? String,temp1.object(forKey: "shipping_address1") as? String,temp1.object(forKey: "shipping_address2") as? String,temp1.object(forKey: "shipping_country_name") as?  String,temp1.object(forKey: "shipping_state_name") as? String,temp1.object(forKey: "shipping_city") as? String,temp1.object(forKey: "shipping_zip_code") as? String,temp1.object(forKey: "shipping_fax") as? String ?? ""]
        
    }
    
    func getparamForSaveArray(_ temp1 : NSDictionary) -> NSMutableArray
    {
                let jsonData = JSON(temp1)
        
        
        var productStr = ""
        var ProductToMarket_Array  = [Int]()
        
        if jsonData["product_ids"].exists()
        {
            
            for index in 0..<((jsonData["product_ids"].arrayValue).count) {
                
                let product = (jsonData["product_ids"][index])
              
                let idvalue = product["product_id"].intValue
                
                ProductToMarket_Array.append(idvalue)
                
            }
            
            if productStr.count != 0
            {
                productStr.removeLast()
            }
        }
        
        return [jsonData["salutation_id"].intValue , jsonData["contact_name"].stringValue,jsonData["attn_name"].stringValue,jsonData["category_id"].intValue,jsonData["contact_title_id"].stringValue,ProductToMarket_Array,jsonData["primary_speciality_id"].intValue,jsonData["secondary_speciality_id"].intValue,jsonData["company_name"].intValue,jsonData["phone"].stringValue,jsonData["email"].stringValue,jsonData["website"].stringValue,jsonData["lead_status_id"].intValue,jsonData["lead_source"].stringValue,jsonData["willingtostock"].stringValue,jsonData["address1"].stringValue,jsonData["address2"].stringValue,jsonData["country_id"].stringValue,jsonData["state_id"].stringValue,jsonData["city"].intValue,jsonData["zipcode"].intValue,jsonData["fax"].stringValue,jsonData["type"].stringValue,jsonData["default_billing_address"].stringValue,jsonData["default_shipping_address"].stringValue,jsonData["contact_id"].intValue,jsonData["billing_address1"].stringValue,jsonData["billing_address2"].stringValue,jsonData["billing_country_id"].stringValue,jsonData["billing_state_id"].stringValue,jsonData["billing_city"].intValue,jsonData["billing_state_id"].intValue,jsonData["billing_zipcode"].stringValue,jsonData["shipping_address1"].intValue,jsonData["shipping_address2"].stringValue,jsonData["shipping_country_id"].stringValue,jsonData["shipping_state_id"].stringValue,jsonData["shipping_city"].intValue,jsonData["shipping_zip_code"].intValue,jsonData["shipping_fax"].stringValue]
//        return t,temp1.object(forKey: "shipping_country_id") as? String,temp1.object(forKey: "shipping_state_id") as? String,temp1.object(forKey: "shipping_fax") as? String ?? ""]
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            if NetworkState.isConnected() {
            
            let temp1 = Main_Contact_Array.object(at: indexPath.row) as! NSDictionary
            
            guard let AddContactViewController = AddContactVC.instance() else{return}
            AddContactViewController.Save_Btn = false
            AddContactViewController.boolvalue = true
            AddContactViewController.isAuthrizedContact = !(temp1.object(forKey: "is_editable") as? Bool ?? false)
                
            AddContactViewController.skippedArray = self.getparamForSkippedArray(temp1)
          
              AddContactViewController.SaveArray = self.getparamForSaveArray(temp1)
                                    
                let jsonData = JSON(temp1)
                
                var productStr = ""
                var ProductToMarket_Array  = [Int]()
                
                if jsonData["product_ids"].exists()
                {
                    for index in 0..<((jsonData["product_ids"].arrayValue).count) {
                        
                        let product = (jsonData["product_ids"][index])
                        
                        let idvalue = product["product_id"].intValue
                        
                        ProductToMarket_Array.append(idvalue)
                        
                    }
                    
                    if productStr.count != 0
                    {
                        productStr.removeLast()
                    }
                }
                
                AddContactViewController.ProductArray = ProductToMarket_Array
                
                print(self.getparamForSkippedArray(temp1))
                 print(self.getparamForSaveArray(temp1))
            
            if (Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "default_billing_address") as! String == "No"
            {
                
                AddContactViewController.DefaultBillingSelection = false
            }
            else
            {
                AddContactViewController.DefaultBillingSelection = true
            }
            
            if (Main_Contact_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "default_shipping_address") as! String == "No"
            {
                
                AddContactViewController.DefaultShippingSelection = false
            }
            else
            {
                AddContactViewController.DefaultShippingSelection = true
                
            }
             self.navigationController?.pushViewController(AddContactViewController, animated: true)
        }
    }
        else if indexPath.section == 4
        {
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                 self.ActionSheet()
            }
            else
            {
                
                if ((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "index") as? Int) == 999
                {
                guard let controller = WebViewVC.instance() else{return}
                let urlString = NSURL(string: (((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "filepath") as? String)!))
                    
                    controller.linkUrl = urlString as URL?
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else
                {
                    
                    guard let controller = ShowImage.instance() else{return}
                    controller.img_Var = self.ImgArray[(((self.Main_Attachment_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "index") as? Int)!)]
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                }
            }
        }
    
    @IBAction func ChooseAction() {
        
        if ContactString != ""
        {
            
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":"","primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0,"contact_id":0]
            ContactString = ""
            
            print(ContactArray)
            
            Main_Contact_Array.add(ContactArray)
            
            print(Main_Contact_Array)
            print(Main_Contact_Array.count)
            
        }
                
        guard let objContactsVC = SelectContactVC.instance() else{return}
        objContactsVC.delegate = self
        objContactsVC.CheckArray = Main_Contact_Array
        self.navigationController?.pushViewController(objContactsVC, animated: true)
    }
    @IBAction func AddAction() {
        
        if ContactString != ""
        {
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":"","primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0,"contact_id":0]
            ContactString = ""
            
            print(ContactArray)
            
            Main_Contact_Array.add(ContactArray)
            
            print(Main_Contact_Array)
            print(Main_Contact_Array.count)
            
            mTableview.reloadData()
        }
    }
    
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message: languageKey(key: "Choose option"), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Take Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.openCamera()
            
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Choose Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.photoLibrary()
            
        }))
            
        alert.addAction(UIAlertAction(title: languageKey(key: "Cancel") , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alert, animated: true) {
            print("option menu presented")
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            // imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
}

// MARK: - TextField Delegate Methods

extension EditVisitVC : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
       
    }
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
    }
    
    func CurrentTime()->String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: currentDateTime)
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        Datevalue = dateFormatter.string(from: datePicker.date)
        print(Datevalue)
        
    }
    @objc func TimePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        Timevalue = dateFormatter.string(from: datePicker.date)
        print(Timevalue)
        
    }
    
    @objc func doneClick() {
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
        }
        else if indexvalu == 4
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryId = Pickerdatavalue[0][RowValue]["id"].intValue
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 5
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 9
        {
            if Datevalue == ""
            {
                skippedArray[9] = CurrentDate()
                SaveArray[9] = CurrentDate()
                
            }
            else
            {
                skippedArray[9] = Datevalue
                SaveArray[9] = Datevalue
            }
      
        }
        
        if indexvalu == 0 || indexvalu == 6 || indexvalu == 7
        {
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
        }
        
            Date_Picker.reloadInputViews()
            Time_Picker.reloadInputViews()
      
        Timevalue = ""
        
        RowValue = 0
        
        mTableview.reloadData()
        
        self.view .endEditing(true)
        
    }
    @objc func cancelClick() {
                
        Timevalue = ""
        
        if indexvalu == 0 || indexvalu == 6 || indexvalu == 7
        {
            
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
        }
        
        Date_Picker.reloadInputViews()
        Time_Picker.reloadInputViews()
        RowValue = 0
        self.view .endEditing(true)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
                
        print("textField-----> ",textField.tag)
        
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            
            Pickerdatavalue = [self.dataList[0]["visit_types"]]
            
            
            print(Pickerdatavalue)
            
            self.dataPickerView.reloadAllComponents()
            
        }
        else if textField.tag == 4
        {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print(Pickerdatavalue)
            
            self.dataPickerView.reloadAllComponents()
        }
        else if textField.tag == 5
        {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryId)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                Pickerdatavalue = [filteredarray]
                
                print(Pickerdatavalue)
                
                self.dataPickerView.reloadAllComponents()
            }
            else{
                if selectedCountryId == 0
                {
                    self.alert("Please select Country First")
                }
                else
                {
                    self.alert("There is no state available in this country")
                }
            }
            
        }
        else if textField.tag == 9
        {
            textField.inputView = self.Date_Picker
            textField.inputAccessoryView = self.toolBar
            Date_Picker.reloadInputViews()
            
        }
        else if textField.tag == 10 || textField.tag == 11
        {
            textField.inputView = self.Time_Picker
            textField.inputAccessoryView = self.toolBar
            Time_Picker.reloadInputViews()
            
        }
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
            self.Date_Picker.removeFromSuperview()
            self.Time_Picker.removeFromSuperview()
            self.dataPickerView.removeFromSuperview()
            self.toolBar.removeFromSuperview()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        
        if textField.tag < 100
            
        {
            skippedArray[textField.tag] = newString
            SaveArray[textField.tag] = newString
        }
        else
        {
            ContactString = newString as String
        }
        
        print(skippedArray)
        
        return true
    }
    
}

// MARK: - TextView Delegate Methods


extension EditVisitVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        print("Text:------>  ",text)
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        skippedArray[10] = changedText
        SaveArray[10] = changedText
        
        print("TextView:------>  ",changedText)
        
        return true
    }
    
}

// MARK: - Protocol Methods

extension EditVisitVC : SelectedContactProtocol
{
    func selected(selected_array: NSMutableArray) {
        
        
        print(selected_array)
        print(selected_array.count)
         print(Main_Contact_Array)
        Main_Contact_Array = selected_array
        print(Main_Contact_Array)
        mTableview.reloadData()
        
    }
}

// MARK: - PickerView Delegat/DataSource Methods

extension EditVisitVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue[0].count
            
        }
        return 0
        //return Pickerdatavalue[0].count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        RowValue = row
        
    }
}

// MARK: - Set Lable Border


extension EditVisitVC
{
    func Border(YourLable: UILabel) -> CAShapeLayer{
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = YourLable.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: YourLable.bounds).cgPath
        
        return yourViewBorder
        
    }
    
}

// MARK: - Image PickerController Delegat Methods

extension EditVisitVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
                
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            
        }
        
        let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
        
        ImgArray.append(selectedImage!)
        
        if fileUrl == nil
        {
          
            AttachmentArray = ["id":0,"filename":"attachment"+String(format:"%d",Main_Attachment_Array.count+1)+String(format:".%@","png"),"filepath":"png","index":ImgArray.count-1]
           
            
            print(AttachmentArray)
            
            Main_Attachment_Array.add(AttachmentArray)
         
        }
        else
        {
            AttachmentArray = ["id":0,"filename":"attachment"+String(format:"%d",Main_Attachment_Array.count+1)+String(format:".%@",fileUrl!.pathExtension),"filepath":String(format:".%@",fileUrl!.pathExtension),"index":ImgArray.count-1]
            
            
            print(AttachmentArray)
            Main_Attachment_Array.add(AttachmentArray)
        
        }

        mTableview.reloadData()
       dismissPicker(picker: picker)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismissPicker(picker: picker)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
        
    }
    private func dismissPicker(picker : UIImagePickerController){
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
}

// MARK: - Class Instance

extension EditVisitVC
{
    class func instance()->EditVisitVC?{
        
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "EditVisitVC") as? EditVisitVC
        
        return controller
    }
}
