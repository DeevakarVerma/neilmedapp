//
//  RescheduleVisitVC.swift
//  MedMobilie
//
//  Created by dr.mac on 30/01/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class RescheduleVisitVC: InterfaceExtendedController {

    fileprivate let datePicker = UIDatePicker()
    fileprivate var tags : Int = 0
    
    var delegate : RescheduledDateTime?
    var idNo : Int = 0
    var title_name : String = ""
    var date : String  = ""
    var fromTime : String = ""
    var toTime : String = ""
    
    
    
    @IBOutlet fileprivate var txtRescheduled_date : UITextField!
     @IBOutlet fileprivate var txtFromTime : UITextField!
     @IBOutlet fileprivate var txtToTime : UITextField!
    @IBOutlet fileprivate var outletSubmitBtn : UIButton!
 
    // MARK: - Class life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        outletSubmitBtn.backgroundColor = VisitNAVBAR()
        outletSubmitBtn.setTitleColor(.white, for: .normal)
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(addTapped))
        
        
        print("date :---> ",date)
         print("fromTime :---> ",fromTime)
        print("toTime :---> ",toTime)
        
        txtRescheduled_date.text = date
        txtFromTime.text = fromTime
        txtToTime.text = toTime
        //txtRescheduled_date.text =
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting

    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: title_name)
    }
    
    
    // MARK: - Back Button

    @objc fileprivate func addTapped()
    {
     self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Submit Button

    @IBAction fileprivate func submitBtn(sender : UIButton)
    {
        if txtRescheduled_date.text != "" && txtFromTime.text != "" && txtToTime.text != ""
        {
           
                self.delegate?.rescheduledTimePass(id : self.idNo, RescheduleDate: self.txtRescheduled_date.text ?? self.date, FromTime: self.txtFromTime.text ?? self.fromTime, ToTime: self.txtToTime.text ?? self.toTime)
            self.navigationController?.popViewController(animated: true)
        }
    }
}


// MARK: - TextField Delegate Methods

extension RescheduleVisitVC : UITextFieldDelegate
{
   
    func showTimePicker(txt : UITextField){
        //Formate Date
        
        tags = txt.tag
        datePicker.minimumDate = Date()
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
       if txt.tag == 1
       { datePicker.datePickerMode = .date
        txtRescheduled_date?.inputAccessoryView = toolbar
        txtRescheduled_date?.inputView = datePicker
        
       }
        else if txt.tag == 2
        {
            datePicker.datePickerMode = .time
            txtFromTime?.inputAccessoryView = toolbar
            txtFromTime?.inputView = datePicker
        }
        else if txt.tag == 3
        {
            datePicker.datePickerMode = .time
            txtToTime?.inputAccessoryView = toolbar
             txtToTime?.inputView = datePicker
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textField-----> ",textField.tag)
        showTimePicker(txt: textField)
        //tags = textField.tag
    }
    
    @objc func donedatePicker(){
        
        
        let formatter = DateFormatter()
        switch tags {
        case 1:
            formatter.dateFormat = "MMM dd,yyyy"
            
            txtRescheduled_date?.text = formatter.string(from: datePicker.date)
            break
        case 2:
            formatter.dateFormat = "hh:mm a"
            txtFromTime?.text = formatter.string(from: datePicker.date)
            break
        case 3:
            formatter.dateFormat = "hh:mm a"
            txtToTime?.text = formatter.string(from: datePicker.date)
            break
        default:
            print("invalid")
        } 
        
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}

// MARK: - Class Instance

extension RescheduleVisitVC
{
    class func instance()->RescheduleVisitVC?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RescheduleVisitVC") as? RescheduleVisitVC
        //self.definesPresentationContext = true
        
        return controller
    }
}


