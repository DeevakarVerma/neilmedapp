//
//  ShowImage.swift
//  MedMobilie
//
//  Created by MAC on 01/02/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

class ShowImage: UIViewController {
    
    var img_Var : UIImage?

    @IBOutlet fileprivate var Attachment_Img: UIImageView!
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let img = img_Var
        {
            Attachment_Img.image = img
            Attachment_Img.contentMode = .scaleAspectFit
        }
        else
        {
            self.popupAlert(title: "", message: "Image not found".languageSet, actionTitles: ["ok".languageSet] ,actions: [{ (action1) in
                self.navigationController?.popViewController(animated: true)
            },nil])
        }
    }
}


// MARK: - Class Instance

extension ShowImage
{
    class func instance()->ShowImage?{
        
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ShowImage") as? ShowImage
        
        return controller
    }
    
}
