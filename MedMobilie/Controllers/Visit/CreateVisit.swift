import UIKit
import IoniconsKit
import MMDrawerController
import SwiftyJSON
import Photos
import SwipeCellKit


class CreateVisit: InterfaceExtendedController {
    
    fileprivate var selectedContact_array : [String]?
    fileprivate var temp_textField = UITextField()
    fileprivate var Pickerdatavalue = [JSON]()
    fileprivate var dataList = [JSON]()
    
    var BackDataBlock: ((_ json : [JSON]) -> ())?
    
    let imagePicker = UIImagePickerController()
    let OtherArray : NSMutableArray =         ["1","","","","","","1","1","","1","","",""]
    
    var indexvalu = 0
    var RowValue = 0
    var numberOfCell = 2
    var ContactString = String()
    var Datevalue = String()
    var Timevalue = String()
    var ImgArray = [UIImage]()
    var filePathName = [String]()
    var ImgSaveArray = [String]()
    var selectedCountryId : Int = 0
    var Redline : Bool = false
    var contact_id : Int = 0
    var contact_name : String = ""
    var salutation : String = ""
    var category_name : String = ""
    var dataPickerView = UIPickerView()
    var Date_Picker = UIDatePicker()
    var arrSctionTitle = NSArray()
    var arrTitle = NSArray()
    var  cell = UITableViewCell()
    var toolBar = UIToolbar()
    var delegate  : AddContactsToCreateOrderProtocol?
    var DummyArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var dicSet = NSMutableDictionary()
    var skippedArray : NSMutableArray =         ["","","","","","","","","","","","","","",""]
    var SaveArray : NSMutableArray =         ["","","","","","","","","","","","","","",""]
    
    @IBOutlet weak var mTableview: UITableView!
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPickerView()
        imagePicker.delegate = self
        self.mTableview.delegate = self
        self.mTableview.dataSource = self
        self.mTableview.tableFooterView = UIView()
        
        RightActionButton(Title:languageKey(key:"Save"))
        Listsapidata()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Create Visit")
        
        arrSctionTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Visit", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Meeting With", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Meeting Point", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
        
        arrTitle = [LocalizationSystem.SharedInstance.localizedStingforKey(key: "Type", comment: "") + "*",LocalizationSystem.SharedInstance.localizedStingforKey(key: "Title", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 1", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Address 2", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Country", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "State", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "City", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Zip/Postal Code", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Phone Number", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Attached Files", comment: ""),LocalizationSystem.SharedInstance.localizedStingforKey(key: "Description", comment: "")]
        
    }
    
    // MARK: - createPickerView
    
    func createPickerView(){
        dataPickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216)
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        dataPickerView.backgroundColor = UIColor.white
        dataPickerView.showsSelectionIndicator = true
        
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        if #available(iOS 13.4, *) {
            if #available(iOS 14.0, *) {
                Date_Picker.preferredDatePickerStyle = .wheels
            } else {
                Date_Picker.preferredDatePickerStyle = .wheels
            }
        } else {
            // Fallback on earlier versions
        }
        
        Date_Picker.datePickerMode = UIDatePicker.Mode.date
        Date_Picker.addTarget(self, action: #selector(datePickerChanged), for: UIControl.Event.valueChanged)
   
    }
    
    
    // MARK: - Label Line Border
    
    func DashedLineBorder (title: String)-> UILabel {
        
        let ContainerView = UILabel(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 55))
        ContainerView.layer.addSublayer(Border(YourLable: ContainerView))
        ContainerView.textAlignment = .center
        ContainerView.font = UIFont.boldSystemFont(ofSize: 16)
        ContainerView.textColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        ContainerView.text = title
        return ContainerView
        
    }
    
    // MARK: - Create textView
    func textView (Title: String)-> UITextView
    {
        let view = UITextView(frame: CGRect(x: 10, y: 10, width: mTableview.frame.size.width-20, height: 80))
        view.delegate = self
        view.backgroundColor = OffWhiteColor()
        view.text = Title
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.gray.cgColor
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }
    
    // MARK: - Save Data in Local database
    
    fileprivate func Listsapidata()
    {
        
        AppDelegate.showWaitView()
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.ListingData.rawValue) { (data) in
            
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? [Data] {
                        jsondata.forEach({$0.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })})
                    }
                }
                
                AppDelegate.hideWaitView()
                
                self.dataList = jsonArray
            }
        }
    }
    // MARK: - Filter Data
    
    fileprivate func gettingNameFromDataList(keyName : String , index : Int) -> String
    {
        var valueName = ""
        self.dataList[0][keyName].forEach({
            if $0.1["id"].stringValue == SaveArray[index] as? String
            {
                valueName = $0.1["name"].stringValue
                return
            }
        })
        return valueName
    }
    
    // MARK: - Scroll Tableview
    func ScrollTableview(Section:Int,Row:Int) {
        
        mTableview.reloadData()
        let indexPath = IndexPath(row: Row, section: Section)
        mTableview.scrollToRow(at: indexPath, at: .top, animated: true)
        
    }
    
    // MARK: - Save Button Action
    override func rightButtonAction(){
        Redline = true
      
        if ContactString != ""
        {
        
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":"","primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0]
            
            
            //            ContactArray = ["id":0,"contact_name":ContactString,"salutation":"","category_name":""]
            //
            
            print(ContactArray)
            
            DummyArray.add(ContactArray)
            
        }
        
        if contact_id != 0
        {
         
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":category_name,"primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0]
            
            print(ContactArray)
            
            DummyArray.add(ContactArray)
            
        }
        
        ContactString = ""
       
        if DummyArray.count == 0
        {
            self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Please select a contact to continue"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                
                return
                }, nil])
            
            return
        }
        else
        {
         
            var datavalues = [String : Any]()
            if SaveArray[0] as? String != ""
            {
                datavalues["visit_type_id"] = SaveArray[0]
                datavalues["visit_type"] = gettingNameFromDataList(keyName: "visit_types", index: 0)
            }
            else
            {
                self.alert("please select a type of Visit".languageSet)
                
                ScrollTableview(Section: 0, Row: 0)
                
                return
            }
            //        if SaveArray[1] as? String != ""
            //        {
            datavalues["title"] = SaveArray[1]
            
            
            //        }
            //        else
            //        {
            //            self.alert("please fill a visit title name".languageSet)
            //
            //
            //
            //            ScrollTableview(Section: 0, Row: 1)
            //
            //
            //            return
            //
            //        }
            if SaveArray[2] as? String != ""
            {
                datavalues["address1"] = SaveArray[2]
            }
            else
            {
                self.alert("please fill a Address 1 field".languageSet)
                
                ScrollTableview(Section: 2, Row: 0)
                return
            }
            
            datavalues["address2"] = SaveArray[3]
            
            
            if SaveArray[4] as? String != ""
            {
                datavalues["country_id"] = SaveArray[4]
                datavalues["country_name"] = gettingNameFromDataList(keyName: "country", index: 4)
            }
            else
            {
                self.alert("please select a country".languageSet)
                ScrollTableview(Section: 2, Row: 2)
                return
            }
            
            if SaveArray[5] as? String != ""
            {
                datavalues["state_id"] = SaveArray[5]
                datavalues["state_name"] = gettingNameFromDataList(keyName: "state", index: 5)
            }
            else
            {
                self.alert("please select a state".languageSet)
                
                ScrollTableview(Section: 2, Row: 3)
                
                return
            }
            
            if SaveArray[6] as? String != ""
            {
                datavalues["city"] = SaveArray[6]
            }
            else
            {
                self.alert("please fill city field".languageSet)
                
                ScrollTableview(Section: 2, Row: 4)
                
                return
            }
            if SaveArray[7] as? String != ""
            {
                datavalues["zipcode"] = SaveArray[7]
            }
            else
            {
                self.alert("please fill Zipcode field".languageSet)
                
                ScrollTableview(Section: 2, Row: 5)
                
                return
            }
            
            
            
            datavalues["phonenumber"] = SaveArray[8]
            
            
            if SaveArray[9] as? String != ""
            {
                datavalues["visit_date"] = SaveArray[9]
                
            }
            else
            {
                self.alert("you must be fill Date for create a visit".languageSet)
                
                
                ScrollTableview(Section: 3, Row: 0)
                return
            }
            
            datavalues["description"] = SaveArray[10]
            datavalues["is_express"] = true
            datavalues["filepath"] = ImgSaveArray
            datavalues["AttachmentFiles"] = ImgArray
            datavalues["pending_count"] = DummyArray.count
            datavalues["status"] = 1
            datavalues["created_at"] = Int(Date().timeIntervalSince1970)
            print(datavalues)
            let jsonData = try? JSONSerialization.data(withJSONObject: DummyArray, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            print(jsonString!)
            
            datavalues["visit_contact"] = jsonString
            
            AddVisitRequest(datavalues, Method: "/visitcreate")
            
        }
    }
    
    // MARK: -Save Offline Data
    fileprivate func OfflineCreate(param : [String : Any])
    {
        let userdefault = UserDefaults.standard
        var id : Int = 50000
        if userdefault.value(forKey: "OfflineNewVisitCreate") as? Int != nil
        {
            let tempid = userdefault.value(forKey: "OfflineNewVisitCreate") as! Int
            
            id = tempid + 1
            
        }
        
        var parama  = [String:Any]()
        parama = param
        
        if let imgarray = parama["AttachmentFiles"] as? [UIImage]
        {  var imgArrayData = [String]()
            imgarray.forEach({
                if let imgData = $0.jpegData(compressionQuality: 0.5)
                {
                    imgArrayData.append(imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters))
                }
                
            })
            parama["AttachmentFiles"] = ""
            parama["visit_attachment"] = imgArrayData
        }
        parama["id"] = 0
        let dict = JSON(parama)
        
        DataBaseHelper.ShareInstance.AddVisit(VisitData:dict){status in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert("something went wrong".languageSet)
                return
            }
            userdefault.set(id, forKey: "OfflineNewVisitCreate")
            userdefault.synchronize()
            
            self.navigationController?.popViewController(animated: true)
            
        }
        print(param)
    }
    
    // MARK: -Add Visit Request
    
    fileprivate func AddVisitRequest(_ param : [String : Any],Method :String)
    {
        AppDelegate.showWaitView()
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            self.OfflineCreate(param: param)
            
            return
        }
        
        AddContactApi().Fetch(param: param,Url: Method) { (status, message, info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.alert(message)
                return
            }
            else
            {
                self.ContactString = ""
                
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: message), actionTitles: [self.languageKey(key: "Ok")], actions: [{action1 in
                    if let CreatedVisit = info
                    {
                        self.BackDataBlock?(CreatedVisit)
                    }
                    self.navigationController?.popViewController(animated: true)
                    
                    //
                    //                    if self.contact_id != 0
                    //                    {
                    //                    //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
                    //
                    //                           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwtichToVisitPAGE"), object: nil)
                    //                    }
                    //                    else
                    //                    {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateVisitList"), object: nil)
                    //                    }
                    
                    }, nil])
            }
        }
    }
}

// MARK: - Table View Delegate/DataSource Methods

extension CreateVisit: UITableViewDelegate,UITableViewDataSource,SwipeTableViewCellDelegate
{
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            self.mTableview.separatorColor = UIColor.black
            return 2
            
        case 1:
            
            self.mTableview.separatorColor = UIColor.clear
            
            if contact_id != 0
            {
                return 1
            }
            else
                
            {
                return DummyArray.count  + numberOfCell
            }
            
        case 2:
            
            self.mTableview.separatorColor = UIColor.black
            return 7
        case 3:
            
            self.mTableview.separatorColor = UIColor.black
            return 1
        case 4:
            
            self.mTableview.separatorColor = UIColor.black
            return (self.ImgArray.count) + 1
        case 5:
            
            self.mTableview.separatorColor = UIColor.black
            return 1
        default:
            return 0
        }
        
        //return arrTitle.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return arrSctionTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = OffWhiteColor()
        
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: tableView.frame.width, height: 30)
        label.text = arrSctionTitle[section] as? String
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        //  label.backgroundColor = UIColor.yellow
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.section == 1 {
            
            if contact_id != 0
            {
                return  100
            }
            else
                
            {
                
                let totalRows = tableView.numberOfRows(inSection: indexPath.section)
                
                if indexPath.row < totalRows - 2 {
                    
                    return  100
                    
                }
                else
                {
                    return  60
                }
            }
        }
        if indexPath.section == 5 {
            return  100
        }
        else
        {
            return 75
        }
    }
    
    func ShowDropDownImage(Image: Ionicons) -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 40, width: 30, height: 30)
        imageView.image = UIImage.ionicon(with: Image, textColor: UIColor.black, size: CGSize(width: 50, height: 50))
        
        return imageView
        
    }
    
    func HideDropDownImage() -> UIImageView {
        
        
        let imageView = UIImageView()
        imageView.frame  = CGRect(x: 20, y: 20, width: 1, height: 1)
        imageView.image = UIImage.ionicon(with: .iosArrowDown, textColor: .clear, size: CGSize(width: 1, height: 1))
        
        return imageView
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            
            cell.lbtitleAC.text = arrTitle[indexPath.row] as? String
            
            
            //str2!.replacingOccurrences(of: "*", with: "", options:
            //    NSString.CompareOptions.literal, range: nil)
            cell.txtTitleAC.text = skippedArray[indexPath.row] as? String
            cell.txtTitleAC.tag = indexPath.row
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            if OtherArray[indexPath.row] as! String == "1"
            {
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                
                self.pickUpDate(cell.txtTitleAC)
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                cell.txtTitleAC.keyboardType = .default
                //cell.accessoryView = HideDropDownImage()
            }
            
            if Redline == true
            {
                if indexPath.row == 0
                {
                    
                    if skippedArray[indexPath.row] as? String == ""
                    {
                        
                        cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 0
                    }
                    
                }
                else
                    
                {
                    cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 0
                }
            }
            return cell
            
        }
            
        else  if indexPath.section == 1
        {
            
            if contact_id != 0
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                
                cell.imgUser?.setImage(string: contact_name, color: nil, circular: true)
                
                cell.lblUserName.text = "\(salutation) \(contact_name)"
                cell.lblMessage.text = category_name
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
                
            }
                
            else
            {
                let totalRows = tableView.numberOfRows(inSection: indexPath.section)
                
                if indexPath.row == totalRows - 1 {
                    let Mycell = tableView.dequeueReusableCell(withIdentifier: "AddMoreCell") as! AddMoreCell
                    
                    if contact_id != 0
                    {
                        Mycell.AddNewAction.isUserInteractionEnabled = false
                        Mycell.AddNewAction.isUserInteractionEnabled = false
                    }
                    else{
                        
                        Mycell.AddNewAction .addTarget(self, action: #selector(AddAction), for: .touchUpInside)
                        
                        Mycell.ChooseAction .addTarget(self, action: #selector(ChooseAction),for: .touchUpInside)
                        
                    }
                    Mycell.selectionStyle = .none
                    return Mycell
                    
                }
                else
                {
                
                    if indexPath.row < totalRows - 2 {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
                        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                        
                        cell.imgUser?.setImage(string: ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "contact_name") as! String), color: nil, circular: true)
                        
                        cell.lblUserName.text = "\( ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "salutation") as! String)) \(((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "contact_name") as! String))"
                        cell.lblMessage.text = ((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "category_name") as! String)
                        cell.delegate = self
                        cell.selectionStyle = .none
                        
                        return cell
                        
                    }
                    else
                    {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AddContact_Visite_Cell") as! AddContact_Visite_Cell
                        cell.ContactTF.backgroundColor = UIColor.clear
                        
                        cell.ContactTF.tag = 100 + indexPath.row
                        cell.ContactTF.setBottomBorder(color: "#3EFE46")
                        cell.ContactTF.placeholder = "New Contact name".languageSet
                        cell.selectionStyle = .none
                        cell.ContactTF.rightViewMode = .never
                        cell.ContactTF.rightView = nil
                        cell.ContactTF.delegate = self
                        cell.ContactTF.text = ContactString
                        cell.ContactTF.isUserInteractionEnabled = true
                        return cell
                    }
                }
            }
        }
            
        else  if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            cell.lbtitleAC.text = arrTitle[indexPath.row+2] as? String
            
            cell.txtTitleAC.text = skippedArray[indexPath.row+2] as? String
            cell.txtTitleAC.tag = indexPath.row+2
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            if indexPath.row == 2 || indexPath.row == 3
            {
                self.pickUpDate(cell.txtTitleAC)
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
                
            }
            else
            {
                
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                // cell.accessoryView = HideDropDownImage()
            }
            
            
//            if OtherArray[indexPath.row+2] as! String == "1"
//            {
//                self.pickUpDate(cell.txtTitleAC)
//                cell.txtTitleAC.rightViewMode = .always
//                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosArrowDown)
//            }
//            else
//            {
//                cell.txtTitleAC.rightViewMode = .never
//                cell.txtTitleAC.rightView = nil
//            }
            if indexPath.row == 0
            {
                cell.txtTitleAC.autocapitalizationType = .words
            }
            if indexPath.row == 1
            {
                cell.txtTitleAC.autocapitalizationType = .words
            }
            
            if indexPath.row == 6
            {
                cell.txtTitleAC.keyboardType = .default
            }
            else
            {
                cell.txtTitleAC.keyboardType = .default
            }
            
            if Redline == true
            {
                
                print(indexPath.row)
                
                if indexPath.row != 1 && indexPath.row != 6
                {
                    
                    if skippedArray[indexPath.row+2] as? String == ""
                    {
                        
                        cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 1.0
                    }
                    else
                    {
                        cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                        cell.txtTitleAC.layer.borderWidth = 0
                    }
                }
                else
                    
                {
                    cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 0
                }
            }
            return cell
            
        }
        else  if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactCell") as! AddContactCell
            
            cell.lbtitleAC.text = arrTitle[indexPath.row+9] as? String
            cell.txtTitleAC.text = skippedArray[indexPath.row+9] as? String
            cell.txtTitleAC.tag = indexPath.row+9
            cell.txtTitleAC.delegate = self
            cell.selectionStyle = .none
            
            
            if OtherArray[indexPath.row+9] as! String == "1"
            {
                self.pickUpDate(cell.txtTitleAC)
                
                cell.txtTitleAC.rightViewMode = .always
                cell.txtTitleAC.rightView = ShowDropDownImage(Image: .iosCalendarOutline)
            }
            else
            {
                cell.txtTitleAC.rightViewMode = .never
                cell.txtTitleAC.rightView = nil
                //cell.accessoryView = HideDropDownImage()
            }
            if Redline == true
            {
                
                if skippedArray[indexPath.row+9] as? String == ""
                {
                    
                    cell.txtTitleAC.layer.borderColor = UIColor.red.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 1.0
                }
                else
                {
                    cell.txtTitleAC.layer.borderColor = UIColor.gray.cgColor as CGColor
                    cell.txtTitleAC.layer.borderWidth = 0
                }
                
            }
            cell.txtTitleAC.keyboardType = .default
            
            return cell
        }
        else  if indexPath.section == 4
        {
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                myCell.contentView .addSubview(DashedLineBorder(title: "Attached file here".languageSet))
            }
                
            else
            {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                    else
                {
                    return UITableViewCell()
                }
                cell.selectionStyle = .none
                
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
                cell.lblUserName.text = ImgSaveArray[indexPath.row]
                cell.imgUser.image = UIImage.fontAwesomeIcon(name: .fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
                cell.delegate = self
                
                return cell
                
            }
            
            myCell.selectionStyle = .none
            return myCell
        }
        else  if indexPath.section == 5
        {
            
            
            let myCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            myCell.backgroundColor = UIColor.white
            myCell.selectionStyle = .none
            
            myCell.contentView .addSubview(textView(Title: SaveArray[indexPath.row+10] as! String))
            return myCell
            
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            
            if indexPath.section == 1
            {
                if self.DummyArray.count > 0
                {
                    self.DummyArray.removeObject(at: indexPath.row)
                }
            }
            else if indexPath.section == 4
            {
                
                self.ImgArray.remove(at: indexPath.row)
                
            }
            self.mTableview.reloadData()
        }
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        
        return [deleteAction]
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1
        {
            
            
            //                guard let AddContactViewController = AddContactVC.instance() else{return}
            //                AddContactViewController.Save_Btn = true
            //
            //
            //
            //
            //
            //            AddContactViewController.skippedArray = [((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "salutation") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "contact_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "attn_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "category_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "contact_title_name") as! String),"",((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "primary_speciality_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "secondary_specialities_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "company_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "phone") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "email") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "website") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "lead_status_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "lead_source") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "willingtostock") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "address1") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "address2") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "city") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "zipcode") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "country_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "state_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "fax") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "type") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "default_billing_address") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "default_shipping_address") as! String),0,((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_address1") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_address2") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_city") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_zipcode") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_country_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_state_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "billing_fax") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_address1") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_address2") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_city") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_zip_code") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_country_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_state_name") as! String),((DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "shipping_fax") as! String)]
            //
            //
            //                if (DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "default_billing_address") as! String == "No"
            //                {
            //
            //                    AddContactViewController.DefaultBillingSelection = false
            //                }
            //                else
            //                {
            //                    AddContactViewController.DefaultBillingSelection = true
            //
            //                }
            //
            //                if (DummyArray.object(at: indexPath.row) as! NSDictionary).object(forKey: "default_shipping_address") as! String == "No"
            //                {
            //
            //                    AddContactViewController.DefaultShippingSelection = false
            //                }
            //                else
            //                {
            //                    AddContactViewController.DefaultShippingSelection = true
            //
            //                }
            //
            //
            //
            //
            //
            //                self.navigationController?.pushViewController(AddContactViewController, animated: true)
            
        }
        else if indexPath.section == 4
        {
            
            let totalRows = tableView.numberOfRows(inSection: indexPath.section)
            
            if indexPath.row == totalRows - 1 {
                
                self.ActionSheet()
            }
            else
            {
                guard let controller = ShowImage.instance() else{return}
                controller.img_Var = ImgArray[indexPath.row]
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
           
        }
    }
    
    @IBAction func ChooseAction() {
        
        if ContactString != ""
        {
            
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":"","primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0,"contact_id" : 0]
            
            
            ContactString = ""
            print(ContactArray)
            DummyArray.add(ContactArray)
            print(DummyArray)
            print(DummyArray.count)
           
        }
        
        guard let objContactsVC = SelectContactVC.instance() else{return}
        objContactsVC.delegate = self
        objContactsVC.CheckArray = DummyArray
        objContactsVC.ClassType = ""
        self.navigationController?.pushViewController(objContactsVC, animated: true)
    }
    @IBAction func AddAction() {
        
        if ContactString != ""
        {
            
            ContactArray = ["salutation":"","contact_name":ContactString,"attn_name":"","category_name":"","primary_speciality_name": "","secondary_specialities_name": "","contact_title_name":"","company_name":"","phone":"","email":"","website":"","lead_status_name":"","lead_source":"","willingtostock":"","address1":"","address2":"","city":"","zipcode":"","country_name":"","state_name":"","fax":"","type":"","default_billing_address":"","default_shipping_address":"","billing_address1":"","billing_address2":"","billing_city":"","billing_zipcode":"","billing_country_name":"","billing_state_name":"","billing_fax":"","shipping_address1":"","shipping_address2":"","shipping_city":"","shipping_zip_code":"","shipping_country_name":"","shipping_state_name":"","shipping_fax":"","id":0,"contact_id" : 0]
            ContactString = ""
            
            print(ContactArray)
            DummyArray.add(ContactArray)
            print(DummyArray)
            print(DummyArray.count)
            
            mTableview.reloadData()
        }
        
    }
    
    func ActionSheet()
    {
        let alert = UIAlertController(title: nil, message: languageKey(key: "Choose option"), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Take Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Choose Photo"), style: .default , handler:{ (UIAlertAction)in
            
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction(title: languageKey(key: "Cancel") , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        
        self.present(alert, animated: true) {
            print("option menu presented")
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            // imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            navigationController!.setNavigationBarHidden(true, animated: false)
            // Add it as a subview
            addChild(imagePicker)
            view.addSubview(imagePicker.view)
            self.tabBarController?.tabBar.isHidden = true
        }
    }
}


// MARK: - TextField Delegate Methods
extension CreateVisit : UITextFieldDelegate
{
    // datepickerView
    
    func pickUpDate(_ textField : UITextField){
        
        print("TagValue:----->>>",textField.tag)
    }
    
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
        
    }
    
    func CurrentTime()->String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: currentDateTime)
        
    }
    
    
    @objc func datePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        Datevalue = dateFormatter.string(from: datePicker.date)
        
        print(Datevalue)
        
    }
    
    
    @objc func TimePickerChanged(datePicker:UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        
        Timevalue = dateFormatter.string(from: datePicker.date)
        
        print(Timevalue)
        
    }
    
    @objc func doneClick() {
        
        
        if indexvalu == 0 {
            
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            
            print(Pickerdatavalue)
            
        }
            
        else if indexvalu == 4
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            skippedArray[indexvalu + 1] = ""
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            SaveArray[indexvalu + 1] = ""
            selectedCountryId = Pickerdatavalue[0][RowValue]["id"].intValue
            
            print(Pickerdatavalue)
        }
            
        else if indexvalu == 5
        {
            skippedArray[indexvalu] = Pickerdatavalue[0][RowValue]["name"].stringValue
            
            SaveArray[indexvalu] = Pickerdatavalue[0][RowValue]["id"].stringValue
            print(Pickerdatavalue)
            
        }
        else if indexvalu == 9
        {
            if Datevalue == ""
            {
                skippedArray[9] = CurrentDate()
                SaveArray[9] = CurrentDate()
              
            }
            else
            {
                skippedArray[9] = Datevalue
                SaveArray[9] = Datevalue
            }
         
        }
            
        else if indexvalu == 10
        {
            
            if Timevalue == ""
            {
                skippedArray[10] = CurrentTime()
                SaveArray[10] = CurrentTime()
            }
            else
            {
                skippedArray[10] = Timevalue
                SaveArray[10] = Timevalue
            }
            
        }
            
        else if indexvalu == 11
        {
            if Timevalue == ""
            {
                skippedArray[11] = CurrentTime()
                SaveArray[11] = CurrentTime()
            }
            else
            {
                skippedArray[11] = Timevalue
                SaveArray[11] = Timevalue
            }
            
        }
        
        if indexvalu == 0 || indexvalu == 6 || indexvalu == 7
        {
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
            
        }
        
        Timevalue = ""
        RowValue = 0
        
        mTableview.reloadData()
        
        self.view .endEditing(true)
        
    }
    @objc func cancelClick() {
        
        Timevalue = ""
        
        if indexvalu == 0 || indexvalu == 6 || indexvalu == 7
        {
            self.dataPickerView.reloadAllComponents()
            
            self.dataPickerView.selectRow(0, inComponent: 0, animated: false)
            
        }
        RowValue = 0
        self.view .endEditing(true)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("textField-----> ",textField.tag)
        
        indexvalu = textField.tag
        
        if textField.tag == 0 {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            Pickerdatavalue = [self.dataList[0]["visit_types"]]
            
            print(Pickerdatavalue)
            
            self.dataPickerView.reloadAllComponents()
            
        }
        else if textField.tag == 4
        {
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            
            Pickerdatavalue = [self.dataList[0]["country"]]
            
            print(Pickerdatavalue)
            self.dataPickerView.reloadAllComponents()
        }
        else if textField.tag == 5
        {
            
            textField.inputView = self.dataPickerView
            textField.inputAccessoryView = self.toolBar
            
            // edit by harsh
            let stateDict = self.dataList[0]["state"]
            
            let filteredArrayBool = stateDict.filter{$0.1["country_id"].stringValue == "\(selectedCountryId)"}
            let temp = filteredArrayBool.map{$0.1}
            print(temp)
            let filteredarray  = JSON(temp)
            
            //filteredArray
            if temp.count > 0{
                
                Pickerdatavalue = [filteredarray]
                print(Pickerdatavalue)
                
                self.dataPickerView.reloadAllComponents()
            }
            else{
                if selectedCountryId == 0
                {
                    self.alert("Please select Country First")
                    self.view .endEditing(true)
                }
                else
                {
                    self.alert("There is no state available in this country")
                    self.view .endEditing(true)
                }
            }
        }
        else if textField.tag == 9
        {
            textField.inputView = self.Date_Picker
            textField.inputAccessoryView = self.toolBar
            Date_Picker.reloadInputViews()
        }
        else
        {
            textField.inputView = nil
            textField.inputAccessoryView = nil
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        print("shouldChangeCharactersIn :---",textField.tag)
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        indexvalu = textField.tag
        
        if indexvalu == 8
        {
            do {
                let regex = try NSRegularExpression(pattern:".*[^0-9- ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
        }
        
        
        if textField.tag < 100
            
        {
            skippedArray[textField.tag] = newString
            
            SaveArray[textField.tag] = newString
        }
        else
        {
            
            ContactString = newString as String
            
        }
        
        print(skippedArray)
        
        return true
    }
    
}

// MARK: - TextView Delegate Methods
extension CreateVisit : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        print("Text:------>  ",text)
        
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        skippedArray[10] = changedText
        SaveArray[10] = changedText
        
        print("TextView:------>  ",changedText)
        
        
        return true
    }
    
}

// MARK: - Contact Protocol Methods

extension CreateVisit : SelectedContactProtocol
{
    func selected(selected_array: NSMutableArray) {
        
        print(selected_array)
        print(selected_array.count)
        DummyArray = selected_array
        print(DummyArray)
        mTableview.reloadData()
        
    }
}

// MARK: - PickerView Delegate/DataSource

extension CreateVisit : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if !Pickerdatavalue.isEmpty
        {
            return Pickerdatavalue[0].count
            
        }
        return 0
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Pickerdatavalue[0][row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        
        RowValue = row
        
    }
}

// MARK: - Lable Border extension

extension CreateVisit
{
    func Border(YourLable: UILabel) -> CAShapeLayer{
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = YourLable.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: YourLable.bounds).cgPath
        
        
        return yourViewBorder
        
    }
    
}

// MARK: - ImagePicker Controller Delegate

extension CreateVisit: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            
        }
        
        
        let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL
        
        ImgArray.append(selectedImage!)
        
        
        if fileUrl == nil
        {
            ImgSaveArray.append("attachment"+String(format:"%d",ImgArray.count)+String(format:".%@","png"))
            
        }
        else
        {
            ImgSaveArray.append("attachment"+String(format:"%d",ImgArray.count)+String(format:".%@",fileUrl!.pathExtension))
            
        }
     
        mTableview.reloadData()
        dismissPicker(picker: picker)
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismissPicker(picker: picker)
        // self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
        
    }
    // MARK: - dismissPicker
    
    
    private func dismissPicker(picker : UIImagePickerController){
        picker.view!.removeFromSuperview()
        picker.removeFromParent()
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.isStatusBarHidden = false
    }
    
}


// MARK: - Create TextField

extension UITextField {
    func setBottomBorder(color:String) {
        self.borderStyle = UITextField.BorderStyle.none
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.clear.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}

// MARK: - Class Instance

extension CreateVisit
{
    class func instance()->CreateVisit?{
        
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateVisit") as? CreateVisit
        
        
        return controller
    }
}


