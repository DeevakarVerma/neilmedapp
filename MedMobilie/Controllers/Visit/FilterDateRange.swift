
import UIKit

class FilterDateRange: InterfaceExtendedController {
    
    fileprivate let datePicker = UIDatePicker()
    fileprivate var tags : Int = 0
    
    var NavColor = UIColor()
    var delegate : FilterProtocol?
    
    @IBOutlet fileprivate var txtFromDate : UITextField!
    @IBOutlet fileprivate var Btn_Filter : UIButton!
    @IBOutlet fileprivate var txtToDate : UITextField!
    @IBOutlet fileprivate var FromDateLBL : UILabel!
    @IBOutlet fileprivate var ToDateLBL : UILabel!
    
    

    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        FromDateLBL.text = self.languageKey(key: "From Date")
        ToDateLBL.text = self.languageKey(key: "To Date")
        Btn_Filter.setTitle(self.languageKey(key: "Filter"), for: .normal)
        Btn_Filter.layer.cornerRadius = 5
        Btn_Filter.clipsToBounds = true
        Btn_Filter.backgroundColor = NavColor

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: NavColor, BtnColor: UIColor.white)
        
    }
    
    // MARK: - Filter Action
    
    @IBAction fileprivate func FilterBtn(sender : UIButton)
    {
        if txtFromDate.text != "" && txtFromDate.text != ""
        {
                self.delegate?.Filterdetails(FromDate : self.txtFromDate.text! , ToDate : self.txtToDate.text!)
            
            self.navigationController?.popViewController(animated: true)

                
            }
        }
    }
    
// MARK: - TextField Delegate Methods

    extension FilterDateRange : UITextFieldDelegate
    {
        
        func showTimePicker(txt : UITextField){
            //Formate Date
            
            
            if txt.tag == 100
            {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd,yyyy" //Your date format
                guard let date = dateFormatter.date(from: "Apr 03,1905") else {
                    fatalError()
                }
                print(date) //Convert String to Date
                
                datePicker.minimumDate = date
                
                
                txtToDate.text! = ""
                
               
            }
            else
            
            {
                if txtFromDate.text! != ""
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd,yyyy" //Your date format
                    guard let date = dateFormatter.date(from: txtFromDate.text!) else {
                        fatalError()
                    }
                    print(date) //Convert String to Date
                    
                    datePicker.minimumDate = date
                    
                }
                else
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd,yyyy" //Your date format
                    guard let date = dateFormatter.date(from: "Apr 03,1905") else {
                        fatalError()
                    }
                    print(date) //Convert String to Date
                    
                    datePicker.minimumDate = date
                    
                }
            }
          
            //ToolBar
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
            
            toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
            datePicker.datePickerMode = .date
                txtFromDate?.inputAccessoryView = toolbar
                txtFromDate?.inputView = datePicker
            txtFromDate.tag = 100
                
            txtToDate?.inputAccessoryView = toolbar
            txtToDate?.inputView = datePicker
            
             txtToDate.tag = 200
            
        }
        func textFieldDidBeginEditing(_ textField: UITextField) {
            
            print("textField-----> ",textField.tag)
            showTimePicker(txt: textField)
            tags = textField.tag
        }
        
        @objc func donedatePicker(){
            
            
            let formatter = DateFormatter()
            switch tags {
            case 100:
                formatter.dateFormat = "MMM dd,yyyy"
                
                txtFromDate?.text = formatter.string(from: datePicker.date)
                break
            case 200:
                formatter.dateFormat = "MMM dd,yyyy"
                
                txtToDate?.text = formatter.string(from: datePicker.date)
                break
           
            default:
                print("invalid")
            }
            
            self.view.endEditing(true)
        }
        
        @objc func cancelDatePicker(){
            self.view.endEditing(true)
        }
    }

// MARK: - Class life cycle

extension FilterDateRange
{
    class func instance()->FilterDateRange?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "FilterDateRange") as? FilterDateRange
        //self.definesPresentationContext = true
        
        return controller
    }
}


