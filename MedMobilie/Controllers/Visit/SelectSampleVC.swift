
//
//  ContactsVC.swift
//  MedMobilie
//
//  Created by dr.mac on 20/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//
import UIKit
import DGElasticPullToRefresh
import SwiftyJSON
import SDWebImage
import SwipeCellKit
import Letters
class SelectSampleVC: InterfaceExtendedController {
    
    
    fileprivate var sampleData : [JSON]?
    
    var contactId : Int = 0
    var visitId : Int = 0
    var checked = [Bool]()
    var SaveArray = [Int]()
    var selectedJson : [JSON]? = [JSON]()
    var delegate : SelectedContactProtocol?
    
    @IBOutlet weak var mtableview: UITableView!
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        RightActionButton(Title:languageKey(key:"Order"))
        
        
        self.mtableview.delegate = self
        self.mtableview.dataSource = self
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
        mtableview.rowHeight = UITableView.automaticDimension
        mtableview.estimatedRowHeight = 100
        AppDelegate.showWaitView()
        DataFetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        
        
        
    }
    
    // MARK: - Save Button Action
    
    override func rightButtonAction(){
        
        print(checked)
        
        self.selectedJson = [JSON]()
        // checked.
        checked.enumerated().filter { (index,value) -> Bool in
            if value
            {
                guard let dict = sampleData?[index] else{return false}
                
                self.selectedJson?.append(dict)
                
            }
            return true
        }
        if selectedJson?.count ?? 0 > 0
        {
            
            guard let controller = SampleCartVC.instance()
                else
            {
                return
            }
            controller.ProductCartData = selectedJson
            controller.contactId = self.contactId
            controller.visitId = self.visitId
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            
            self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: "you must select atleast one product to order place"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                print("ok")
                return
                }, nil])
            
        }
        // self.delegate?.selected(selected_array: SaveArray)
        
        
    }
    
    // MARK: - Sample Product List
    
    fileprivate func DataFetch()
    {
        if !NetworkState.isConnected() {
            self.mtableview.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        ProductListingFetch().Fetch(ProductType: "sample") { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message )
                
                self.mtableview.dg_stopLoading()
                return
            }
            
            self.mtableview.dg_stopLoading()
            self.sampleData = data
            
            self.checked = Array(repeating: false, count: self.sampleData?.count ?? 0)
            
            self.mtableview.reloadData()
            
            
        }
    }
    
    
    @objc override func LanguageSet(){
        
        
    }
    
}


// MARK: - Table View Delegate/DataSource Methods
extension SelectSampleVC : UITableViewDelegate,UITableViewDataSource
{
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ProductListingCell
        
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        
        guard let infos = sampleData?[indexPath.row]  else{return cell}
        
        
        
        
        let str = "\(SignedUserInfo.sharedInstance?.currency_symbol ?? "") \(String(infos["pricepercase"].doubleValue.round(to: 2)))"
        cell.Price?.text = str
        cell.Units?.text = infos["unitspercase"].stringValue
        cell.lblProductName?.text = infos["name"].stringValue
        cell.lblCategoryName?.text = infos["category_name"].stringValue
        
        
        cell.imgProductimg?.sd_setImage(with: infos["image"].url, placeholderImage: UIImage(imageLiteralResourceName: "Product_PH"), options:.cacheMemoryOnly, completed: nil)
        
        cell.selectionStyle = .none
        
        if !checked[indexPath.row] {
            cell.accessoryType = .none
        } else if checked[indexPath.row] {
            cell.accessoryType = .checkmark
        }
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
                checked[indexPath.row] = false
            } else {
                cell.accessoryType = .checkmark
                checked[indexPath.row] = true
            }
        }
    }
    
    
    
    
    
}


// MARK: - Pull To Refresh
extension SelectSampleVC
{
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.DataFetch()
            //            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            //                self?.mtableview.dg_stopLoading()
            //            })
            
            }, loadingView: loadingView)
        mtableview.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        mtableview.dg_setPullToRefreshBackgroundColor(mtableview.backgroundColor!)
    }
}

// MARK: - Class Instance

extension SelectSampleVC
{
    class func instance()->SelectSampleVC?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectSampleVC") as? SelectSampleVC
        
        
        return controller
    }
}

