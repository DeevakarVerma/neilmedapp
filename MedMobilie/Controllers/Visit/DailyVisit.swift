
import UIKit
import IoniconsKit
import DGElasticPullToRefresh
import SwiftyJSON
import SwipeCellKit

class DailyVisit: InterfaceExtendedController ,UISearchResultsUpdating{
    
    fileprivate var JsonData : [JSON]?
    fileprivate var mainJsonData : [JSON]?
    fileprivate var resultSearchController = UISearchController()
    
    var TodayPending : Bool = false
    
    var Titlename : String = ""
    var datefrom : String = ""
    var dateto : String = ""
    var type : String = ""
    var contactId : Int = 0
    var EditTime : Int = 0
    
    var reportKey : String = ""
    var comeFromContact : Bool = false
    var SwipCell : Bool = false
    var FilterBtn = SSBadgeButton()
    var ReloadBtn = SSBadgeButton()
    
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnView.createCircleForView()
        
        if comeFromContact == true
        {
            
            btnView.isHidden = true
            
        }
        
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        
        
        if contactId == 0
        {
            if TodayPending == false
            
            {
                
                self.NavBarFilter()
                
            }
            
        }
        AppDelegate.showWaitView()
        
        VisitListDataFetch()
        
        NotificationCenter.default.addObserver(self, selector: #selector(VisitListDataFetch), name: NSNotification.Name( "UpdateVisitList"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.SetUpSearchBar()
        
        
        if  Titlename == "Visit" {
            
            PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: VisitNAVBAR(), BtnColor: UIColor.white)
        }
        else
        {
            PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
            btnView.isHidden = false
        }
        
        
        if TodayPending == true
        {
            AppDelegate.showWaitView()
            VisitListDataFetch()
            btnView.isHidden = true
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: Titlename)
    }
    
    
    // MARK: - Set navigationbar Icons
    
    func NavBarFilter()
    {
        
        FilterBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 16)
        FilterBtn.setImage(UIImage.ionicon(with: .androidOptions, textColor: .white, size: CGSize(width: 30, height: 30)), for: .normal)
        FilterBtn.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 5)
        
        FilterBtn.badgeLabel.isHidden = true
        FilterBtn.addTarget(self, action: #selector(Filter_Action), for: .touchUpInside)
        let Button1 = UIBarButtonItem(customView: FilterBtn)
        
        
        
        ReloadBtn.frame = CGRect(x: 0, y: 0, width: 18, height: 16)
        
        let image = UIImage(named: "exchange") as UIImage?
        
        
        ReloadBtn.setImage(image, for: .normal)
        ReloadBtn.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 5)
        
        ReloadBtn.badgeLabel.isHidden = true
        ReloadBtn.addTarget(self, action: #selector(Reload_Action), for: .touchUpInside)
        let Button2 = UIBarButtonItem(customView: ReloadBtn)
        
        self.navigationItem.rightBarButtonItems = [Button2,Button1]
        
    }
    
    // MARK: - Filter Action
    
    
    @objc func Filter_Action(){
        
        self.popupAlertWithSheet(title: nil, message: nil, actionTitles: [ self.languageKey(key: "New"),self.languageKey(key: "Pending"),self.languageKey(key: "Date range")], actions:[
                                    {action1 in
                                        
                                        
                                        self.type = "new"
                                        self.datefrom = ""
                                        self.dateto = ""
                                        
                                        AppDelegate.showWaitView()
                                        self.VisitListDataFetch()
                                        
                                        
                                    },{action2 in
                                        
                                        
                                        self.type = "pending"
                                        self.datefrom = ""
                                        self.dateto = ""
                                        AppDelegate.showWaitView()
                                        self.VisitListDataFetch()
                                        
                                        
                                    },{action3 in
                                        
                                        guard let controller = FilterDateRange.instance()
                                        else{return}
                                        controller.delegate = self
                                        controller.NavColor = VisitNAVBAR()
                                        self.navigationController?.pushViewController(controller, animated: true)
                                        
                                    }, nil])
    }
    
    // MARK: - SearchBar Setup
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.placeholder = self.languageKey(key: "Search Visits".languageSet)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    // MARK: - Update Search Results
    
    func updateSearchResults(for searchController: UISearchController) {
        // filteredTableData.removeAll(keepingCapacity: false)
        
        if searchController.searchBar.text != ""
        {
            let Filterarray = self.mainJsonData?.filter({ (element) -> Bool in
                if element["title"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "") || element["visit_type"].stringValue.lowercased().contains(searchController.searchBar.text?.lowercased() ?? "")
                {
                    return true
                }
                return false
            })
            
            self.JsonData = Filterarray
            self.tableView.reloadData()
        }
        else
        {
            self.JsonData = mainJsonData
            self.tableView.reloadData()
            
        }
        
    }
    
    // MARK: - Reload Action
    
    @objc func Reload_Action()
    {
        
        if SwipCell == true
        {
            SwipCell = false
        }
        else
        {
            SwipCell = true
            
        }
        SwipCellAction()
        
    }
    
    
    // MARK: - Swip Cell Action
    
    func SwipCellAction()  {
        
        
        if SwipCell == true
        {
            self.tableView.isEditing = true
            self.tableView.allowsSelectionDuringEditing = true
            // tableView.allowsSelectionDuringEditing = true
            
        }
        else
        
        {
            self.tableView.isEditing = false
            self.tableView.allowsSelectionDuringEditing = false
            // tableView.allowsSelectionDuringEditing = false
            
        }
        
        tableView.reloadData()
        
    }
    
    // MARK: - fetch Visit List Data
    @objc fileprivate func VisitListDataFetch()
    {
        if !NetworkState.isConnected() {
            self.tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        
        var DataValue = [String : Any]()
        DataValue["contact_id"] = contactId
        DataValue["datefrom"] = datefrom
        DataValue["dateto"] = dateto
        if reportKey != ""
        {
            DataValue["report_type"] = reportKey
        }
        else
        {
            DataValue["type"] = type
        }
        DataValue["URL"] = "/visitlist"
        
        
        if TodayPending == true
        {
            DataValue["datefrom"] = CurrentDate()
            DataValue["type"] = "todaypending"
            
        }
        
        
        VisitList_Api().fetchData(param: DataValue) { (status, message, data) in
            
            AppDelegate.hideWaitView()
            
            if !status
            {
                self.JsonData?.removeAll()
                self.tableView.reloadData()
                
                self.tableView.dg_stopLoading()
                self.alert(message )
                return
            }
            if data != nil
            {
                
                self.reportKey = ""
                self.mainJsonData = data ?? nil
                self.JsonData = self.mainJsonData
                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
                
                
                if self.TodayPending == true
                {
                    if self.JsonData?.count != 0
                    {
                        if self.EditTime == 0
                        {
                            self.EditTime = 1
                            self.SwitchtoEditpage(index: 0)
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
        
    }
    
    // MARK: - Request visit Delete
    
    fileprivate func visitDelete(Contactid:Int)
    {
        
        if !NetworkState.isConnected() {
            tableView.dg_stopLoading()
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        ContactDelete_Api().Request(idvalue: Contactid ,url_string: "/visitdelete" ) { (status, message, data) in
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
            }
            
        }
        
    }
    
    
    // MARK: - Create New Visit Button Action
    @IBAction func createNewVisit(_ sender: Any) {
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [ self.languageKey(key: "Create Visit"),self.languageKey(key: "Express Visit")], actions:[{action1 in
            
            // NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
            
            guard let controller = CreateVisit.instance()
            else{return}
            
            controller.BackDataBlock = {infos in
                print(infos)
                self.addANDEditVisit(link: infos)
            }
            
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            
        },{action2 in
            
            //  NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 0)
            
            guard let controller = ExpressVisit.instance()
            else{return}
            
            controller.BackDataBlock = {infos  in
                print(infos)
                self.addANDEditVisit(link: infos)
                
            }
            self.navigationController?.pushViewController(controller, animated: true)
            
        }, nil])
        
    }
    deinit {
        tableView.dg_removePullToRefresh()
    }
    
    
    
    func addANDEditVisit(link : [JSON],newFlag : Bool = true)
    {
        if !newFlag
        {
            if let infos = self.JsonData
            {
                infos.enumerated().forEach({
                    if $0.element["id"].intValue == link[0]["id"].intValue
                    {
                        self.JsonData?[$0.offset] = link[0]
                        
                    }
                })
                
            }
        }
        else
        {
            self.JsonData?.insert(link[0], at: 0)
        }
        
        self.tableView.reloadData()
    }
    
}

// MARK: - Table View Delegate/DataSource Methods

extension DailyVisit : UITableViewDelegate , UITableViewDataSource , SwipeTableViewCellDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return JsonData?.count ?? 0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let json = JsonData?[indexPath.row]
        else{return}
        guard let controller = VisitDetailsVC.instance()
        else{return}
        controller.visitId = json["id"].intValue
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? VisitListingCell
        else{return VisitListingCell()}
        
        guard let json = JsonData?[indexPath.row]
        else{return cell}
        
        
        if json["is_express"].boolValue
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
        }
        
        
        cell.lblTask?.text = json["title"].stringValue
        cell.lbldate?.text = json["visit_date"].stringValue
        //cell.lblToTime?.text = json["visit_totime"].stringValue
        //cell.lblFromTime?.text = json["visit_fromtime"].stringValue
        cell.lblVisitType?.text = json["visit_type"].stringValue
        cell.lblVisitedCount?.text = String(json["visited_count"].intValue)
        cell.lblPendingCount?.text = String(json["pending_count"].intValue)
        cell.lblRescheduleCount?.text = String(json["rescheduled_count"].intValue)
        cell.selectionStyle = .none
        if !comeFromContact
        {
            cell.delegate = self
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if !comeFromContact
        {
            return true
        }
        return false
    }
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        
        let deleteAction = SwipeAction(style:.destructive, title: nil) { action, indexPath in
            
            guard let list = self.JsonData?[indexPath.row] else{return }
            
            if !list["is_editable"].boolValue
            {
                self.alert(self.languageKey(key: "You are not authorized to Delete this contact"))
                // self.DeleteContacts(Contactid: list["id"].intValue)
            }
            else
            {
                self.popupAlert(title: self.languageKey(key: Bundle.appName()), message: self.languageKey(key: "Are you sure you want to delete this Visit?"), actionTitles: [self.languageKey(key: "NO"),self.languageKey(key: "YES")], actions: [{action1 in
                    return
                },{action2 in
                    
                    self.JsonData?.remove(at: indexPath.row)
                    self.tableView.reloadData()
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                            self.visitDelete(Contactid: list["id"].intValue)
                        }
                    }
                    
                }, nil])
            }
            
        }
        
        // customize the action appearance
        
        let editAction = SwipeAction(style:.destructive, title:nil) { action, indexPath in
            
            
            self.SwitchtoEditpage(index: indexPath.row)
        }
        
        // customize the action appearance
        
        deleteAction.backgroundColor = hexStringToUIColor(hex: "#731E16")
        deleteAction.image = UIImage.ionicon(with: .androidDelete, textColor: .white, size: CGSize(width: 32, height: 32))
        editAction.backgroundColor = UIColor.lightGray
        editAction.image = UIImage.ionicon(with: .edit, textColor: .white, size: CGSize(width: 32, height: 32))
        
        
        return [deleteAction,editAction]
        
    }
    
    
    func SwitchtoEditpage(index:Int)  {
        
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                
                guard let json = self.JsonData?[index]
                else{return}
                guard let controller = EditVisitVC.instance()
                else{return}
                controller.visitId = json["id"].intValue
                controller.BackDataBlock = {infos in
                    print(infos)
                    self.addANDEditVisit(link: infos,newFlag: false)
                }
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        
        print("sourceIndexPath",sourceIndexPath.row)
        print("destinationIndexPath",destinationIndexPath.row)
        
        var isupcheck:String = "2"
        
        if sourceIndexPath.row < destinationIndexPath.row
        {
            isupcheck = "2"
        }
        else
        {
            isupcheck = "1"
        }
        let json = JsonData?[sourceIndexPath.row] ?? 0
        
        
        let jsonsource = JsonData?[sourceIndexPath.row]
        let jsondestination = JsonData?[destinationIndexPath.row]
        
        if jsonsource!["id"].intValue != jsondestination!["id"].intValue
        {
            
            
            DispatchQueue.global(qos: .background).async {
                
                
                
                DispatchQueue.main.async {
                    
                    
                    self.MoveRowindex(source:  jsonsource!["id"].intValue, destination:jsondestination!["id"].intValue,isup:isupcheck)
                    
                }
                
            }
        }
        
        
        
        
        let mutDict : NSMutableDictionary = NSMutableDictionary(dictionary: (json.dictionary)!)
        
        
        mutDict["is_express"] = json["is_express"].boolValue
        mutDict["title"] = json["title"].stringValue
        mutDict["visit_date"] = jsondestination!["visit_date"].stringValue
        mutDict["visit_type"] = json["visit_type"].stringValue
        mutDict["visited_count"] = json["visited_count"].stringValue
        mutDict["pending_count"] = json["pending_count"].stringValue
        mutDict["rescheduled_count"] = json["rescheduled_count"].stringValue
        
        print("DataValue :---->",mutDict)
        
        
        JsonData?.remove(at: sourceIndexPath.row)
        
        JsonData?.insert(JSON.init(mutDict)
                         , at: destinationIndexPath.row)
        tableView.reloadData()
        
        
    }
    
    
    func MoveRowindex(source:Int,destination:Int,isup:String)
    {
        print("MoveRowIndex")
        
        if !NetworkState.isConnected() {
            
            return
        }
        
        var DataValue = [String : Any]()
        DataValue["source"] = source
        DataValue["destination"] = destination
        DataValue["isup"] = isup
        
        DataValue["URL"] = "/swapvisitorder"
        
        
        VisitList_Api().fetchData(param: DataValue) { (status, message, data) in
            
            
        }
    }
    
    
    
}


// MARK: - PullToRefresh Method

extension DailyVisit
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self!.datefrom  = ""
            self!.dateto  = ""
            self!.type  = ""
            
            
            self?.VisitListDataFetch()
        }, loadingView: loadingView)
        if  Titlename == "Visit" {
            
            tableView.dg_setPullToRefreshFillColor(VisitNAVBAR())
        }
        else
        {
            tableView.dg_setPullToRefreshFillColor(SettingNAVBAR())
        }
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}

// MARK: - Filter Protocol

extension DailyVisit : FilterProtocol
{
    func Filterdetails(FromDate : String , ToDate : String)
    {
        
        
        datefrom = FromDate
        dateto = ToDate
        
        print("datefrom :---> ",datefrom)
        print("dateto :---> ",dateto)
        
        AppDelegate.showWaitView()
        self.VisitListDataFetch()
        
    }
    
}


// MARK: - Class Instance
extension DailyVisit
{
    class func instance()->DailyVisit?{
        let storyboard = UIStoryboard(name: "Visit", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "DailyVisit") as? DailyVisit
        //self.definesPresentationContext = true
        
        return controller
    }
}
