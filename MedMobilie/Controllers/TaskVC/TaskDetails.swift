

import UIKit
import SwiftyJSON
import Letters
import IoniconsKit
import DGElasticPullToRefresh
import SwipeCellKit
import EPSignature
import FontAwesome_swift

class TaskDetails: InterfaceExtendedController {
    fileprivate var selectedContactId : Int = 0
    fileprivate var jsonMain : [JSON]?
    fileprivate var jsonAddress : [String : JSON]?
    fileprivate var jsonContact : [JSON]?
    fileprivate var jsonAttachment : [JSON]?
    fileprivate var pickerDataHours = [Array(0...24),Array(0...60)]
    fileprivate var waitingHours  : Int = 0
    fileprivate var waitingMinutes  : Int = 0
    fileprivate let sections = ["","Visit","Attachments","Notes"]
    
    var TaskId : Int = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet fileprivate var btnShowTask : UIButton?
   
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 150
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        btnShowTask?.backgroundColor = VisitNAVBAR()
       
        AppDelegate.showWaitView()
        TaskListDataFetch()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: TaskNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        NavigationBarTitleName(Title: "Task Details")
    }
    
    // MARK: - Fetch Task List Data API
    fileprivate func TaskListDataFetch()
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetch_Api().Fetch(ID: TaskId ,urlString: "/taskappdetails") { (status, message,data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.tableView.dg_stopLoading()
                self.alert(message)
                return
            }
            if data != nil
            {
                self.jsonMain = data
                
                print(data)
                guard let tempjsonContact =  self.jsonMain?[0]["task_visits"].arrayValue else{return}
                
                self.jsonContact = tempjsonContact
                guard let tempjsonAttachment =  self.jsonMain?[0]["task_attachments"].arrayValue else{return}
                self.jsonAttachment = tempjsonAttachment
                
            
                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
            }
        }
        
        
    }
    
}

// MARK: - Table View Delegate/DataSource Methods
extension TaskDetails : UITableViewDelegate,UITableViewDataSource , SwipeTableViewCellDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1
        {
            
            guard let json = jsonContact?[indexPath.row]
                else{return}
            guard let controller = VisitDetailsVC.instance()
                else{return}
            controller.visitId = json["id"].intValue
            controller.ClassType = "TaskView"
            self.navigationController?.pushViewController(controller, animated: true)
        }
       
        else if indexPath.section == 2
        {
            guard let json = jsonAttachment?[indexPath.row] else{return}
            let filename = json["filepath"].url
            
            guard let controller = WebViewVC.instance() else{return}
            controller.linkUrl = filename
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        case 1 :
            return jsonContact?.count ?? 0
            
        case 2:
            return jsonAttachment?.count ?? 0
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0 :
            return 0.0
        case 1 :
            if jsonContact?.count ?? 0 > 0
            {
                return UITableView.automaticDimension
            }
            else
            {
                return 0.0
            }
            
        case 2:
            if jsonAttachment?.count ?? 0 > 0
            {
                return 40.0
            }
            else
            {
                return 0.0
            }
        case 3:
            return UITableView.automaticDimension
            
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch section {
        case 1 :
            return sections[section]
            
        case 2:
            return sections[section]
        case 3:
            return sections[section]
        default:
            return ""
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if jsonMain != nil
        {
            if jsonAttachment != nil
            {
                return sections.count
            }
            else
            {
                return sections.count - 1
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "detail") as? VisitDetailsCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            guard let json = jsonMain?[0] else{return cell}
            cell.VisitName?.text = json["name"].stringValue
            cell.lblDetails?.text = json["description"].stringValue
            cell.lblDetails?.textColor = UIColor.black
            let dateStr = json["deadline"].stringValue
            cell.lblVisitDate?.text = dateStr
            cell.lblVisitType?.text =  "\("Assign by: ") \(json["assign_by"].stringValue)"
            
            
                cell.lblAddressVisit?.text = ""
                
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? VisitListingCell
                else{return VisitListingCell()}
            
            guard let json = jsonContact?[indexPath.row]
                else{return cell}
            
            
            if json["is_express"].boolValue
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
            }
            else
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
            }
            
    
            cell.lblTask?.text = json["title"].stringValue
            cell.lbldate?.text = json["visit_date"].stringValue
            //cell.lblToTime?.text = json["visit_totime"].stringValue
            //cell.lblFromTime?.text = json["visit_fromtime"].stringValue
            cell.lblVisitType?.text = json["visit_type"].stringValue
            cell.lblVisitedCount?.text = String(json["visited_count"].intValue)
            cell.lblPendingCount?.text = String(json["pending_count"].intValue)
            cell.lblRescheduleCount?.text = String(json["rescheduled_count"].intValue)
            cell.selectionStyle = .none
            
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttachmentCell", for: indexPath) as? TeamCell
                else
            {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            guard let json = jsonAttachment?[indexPath.row] else{return cell}
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            cell.lblUserName.text = json["filename"].stringValue
            let fileEx = json["filepath"].url?.pathExtension
            var fileImage : FontAwesome = .file
            
            switch fileEx
            {
            case "pdf" :
                fileImage = .filePdf
                
                break
            case "jpg" :
                fileImage = .fileImage
                break
            case "jpeg" :
                fileImage = .fileImage
                break
            case "doc" :
                fileImage = .fileWord
                break
            default :
                fileImage = .fileImage
                break
                
            }
            
            cell.imgUser.image = UIImage.fontAwesomeIcon(name: fileImage, style: .regular, textColor: VisitNAVBAR(), size: CGSize(width: 40.0, height: 40.0))
            
            return cell
            
        case 3:
        
                guard let notesCell = tableView.dequeueReusableCell(withIdentifier: "NotesCell") as? VisitDetailsCell
                    else{return VisitDetailsCell()}
                 guard let json = jsonMain?[0] else{return notesCell}
                notesCell.selectionStyle = .none
                notesCell.lblDetails?.text = json["notes"].stringValue
                
            return notesCell
            
        default:
            print("error")
        }
        return UITableViewCell()
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]?
    {
        return[]
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    
    
}

// MARK: - PickerView Delegate/DataSource Methods

extension TaskDetails :  UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // edit by harsh
        
        return pickerDataHours[component].count
        //return Pickerdatavalue[0].count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.text = String(row)
        label.textAlignment = .center
        return label
    }
    
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerDataHours[component][row])
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int)
    {
        if let label = pickerView.view(forRow: row, forComponent: component) as? UILabel {
            
            if component == 0, row > 1 {
                label.text = String(row) + " hours"
                waitingHours = pickerDataHours[component][row]
                
            }
            else if component == 0 {
                label.text = String(row) + " hour"
                waitingHours = pickerDataHours[component][row]
            }
            else if component == 1 {
                label.text = String(row) + " min"
                waitingMinutes = pickerDataHours[component][row]
            }
            
        }
        /*
         if component == 0
         {
         waitingHours = pickerDataHours[component][row]
         }
         else if component == 1
         {
         waitingMinutes = pickerDataHours[component][row]
         }*/
        // RowValue = row
        
    }
}




// MARK: - PullToRefresh

extension TaskDetails
{
    override func loadView() {
        super.loadView()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(TaskNAVBAR())
        
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
}

// MARK: - Class Instance

extension TaskDetails
{
    class func instance()->TaskDetails?{
        let storyboard = UIStoryboard(name: "Task", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TaskDetails") as? TaskDetails
        
        return controller
    }
}
