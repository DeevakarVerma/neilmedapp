//
//  CheckinVC.swift
//  MedMobilie
//
//  Created by dr.mac on 23/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseDatabase
import SwiftyJSON

class CheckinVC: InterfaceExtendedController,MTSlideToOpenDelegate,CLLocationManagerDelegate {
    
    let objAppDelegate = UIApplication.shared.delegate as? AppDelegate
    let ref = Database.database().reference()
    
    var VisitRouteListData : [JSON]?
    var visit_id = 0
    var latvalue1 = 0.0
    var longvalue1 = 0.0
    var latvalue2 = 0.0
    var longvalue2 = 0.0
    var id_values = ""
    var Check_location_start: Bool = false
    var Selected_Game_Array = [String : Any]()
    var CheckinoutArray = [String : Any]()
    var checkStatus:Bool = false
    var Distance_Status = false
    var locationManager: CLLocationManager!
    var timer = Timer()
    
    @IBOutlet var slide : MTSlideToOpenView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var Check_In_Out_LBL : UILabel!
    @IBOutlet weak var Time_title_LBL : UILabel!
    @IBOutlet weak var Current_time_LBL : UILabel!
    @IBOutlet weak var Current_date_LBL : UILabel!
    
    // MARK: - Class life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Current_time_LBL.text = DateFormat(FormatType: "h:mm a")
        Current_date_LBL.text = "\(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: "") ) \(DateFormat(FormatType: "MMMM dd, yyyy"))"
        self.tabBarController?.navigationController?.navigationBar.isHidden = true
        paintNavigationTitle(title : "Check In".languageSet,Color : .black)
        self.NavBarIcon()
        PaintleftSideMenu()
        slideToOpen()
        // middleView.addSubview(slideToOpen)
        LocationLatLong()
        objAppDelegate?.Time_delegate = self
        if UserDefaults.standard.string(forKey: "Check_in") == "yes"
        {
            Check_location_start = true
            self.checkStatus = true
            Slider_Check_in()
        }
        else
        {
            Check_location_start = false
            self.checkStatus = false
            Slider_Check_out()
        }
        
        OfflineCheckinData()
        OfflineCheckinoutData()
        
        self.locationManager.startUpdatingLocation()
        self.locationManager.stopUpdatingLocation()
    }
    
    @objc func updateTime() {
        
        Current_time_LBL.text = DateFormat(FormatType: "h:mm a")
        Current_date_LBL.text = "\(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Date", comment: "") ) \(DateFormat(FormatType: "MMMM dd, yyyy"))"
    }
    
    func endTimer()
    {
        timer.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
        Check_In_Out_LBL.textColor = ColorValue(Rvalue: 0.0/255.0, Gvalue: 0.0/255.0, Bvalue: 0.0/255.0, alphavalue: 1.0)
        Time_title_LBL.textColor = ColorValue(Rvalue: 0.0/255.0, Gvalue: 0.0/255.0, Bvalue: 0.0/255.0, alphavalue: 1.0)
        
        Current_time_LBL.textColor = ColorValue(Rvalue: 0.0/255.0, Gvalue: 0.0/255.0, Bvalue: 0.0/255.0, alphavalue: 1.0)
        
        Current_date_LBL.textColor = ColorValue(Rvalue: 0.0/255.0, Gvalue: 0.0/255.0, Bvalue: 0.0/255.0, alphavalue: 1.0)
        
        PaintNavigationBar(TitleColor: UIColor.black, BackgroundColor: navBackgroundColor ?? WhiteColor(), BtnColor: UIColor.black)
        
        self.visitroutelist(url: "/visitroutelist", type: "")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {        endTimer()
    }
    
    // MARK: - visit route list API
    fileprivate func visitroutelist(url : String,type:String)
    {
        if !NetworkState.isConnected()
        {
            AppDelegate.hideWaitView()
            return
        }
        var parama = [String:Any]()
        AppDelegate.showWaitView()
        
        CheckinCheckout_API().Fetch(param : parama, Url: url) { (status, message,info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                return
            }
            self.VisitRouteListData = info

            print(self.VisitRouteListData)
        }
    }
    // MARK: - slideToOpen
    
    func slideToOpen()
    {
        // slide.frame =  CGRect(x: 20, y: middleView.frame.origin.y+60, width: middleView.frame.size.width-20, height: 56)
        slide.sliderViewTopDistance = 0
        slide.sliderCornerRadious = 28
        slide.thumnailImageView.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        slide.draggedView.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        slide.delegate = self
        slide.sliderHolderView.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        slide.defaultLabelText = "Slide to check in"
        
        slide.thumnailImageView.image =  UIImage.ionicon(with: .iosCheckmark, textColor: UIColor.white, size: CGSize(width: 50, height: 50))
        
    }
    
    // MARK: - SlideToOpen Delegate Method
    
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        if self.checkStatus == false
        {
            self.locationManager.startUpdatingLocation()
        }
        
        UIView.animate(withDuration: 0.7, animations: {
            sender.resetStateWithAnimation(false)
            
            if self.checkStatus == false
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                    self.CheckinCheckout(url: "/checkincheckout", type: "checkin")
                }
            }
            else
            {
                self.CheckinCheckout(url: "/checkincheckout", type: "checkout")
                
                self.locationManager.stopUpdatingLocation()
            }
        })
    }
    
    // MARK: - Slider Check_in Action
    
    func Slider_Check_in()
    {

        self.slide.thumnailImageView.backgroundColor = UIColor(red: 34.0/255.0, green: 139.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        self.slide.draggedView.backgroundColor = UIColor(red: 34.0/255.0, green: 139.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        self.slide.sliderHolderView.backgroundColor = UIColor(red: 34.0/255.0, green: 139.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        self.slide.thumnailImageView.image = UIImage.ionicon(with: .power, textColor: UIColor.white, size: CGSize(width: 50, height: 50))
        self.slide.defaultLabelText = "Slide to check out"
        self.checkStatus = true
        self.Check_location_start = true
        self.locationManager.startUpdatingLocation()
        
        if !NetworkState.isConnected()
        {
            let checkinTimeIntervale = Date().timeIntervalSince1970
            let ConvertTimeIntervale = Int(checkinTimeIntervale)
            
            id_values = String(ConvertTimeIntervale)
            
            DataBaseHelper.ShareInstance.CheckInOutTime(id_value: String(ConvertTimeIntervale)
                , date_value: String(ConvertTimeIntervale), checkIn_value: String(ConvertTimeIntervale), checkout_value: "")
        }
    }
    // MARK: - Slider Check_out Action
    
    func Slider_Check_out()
    {
        print("Call Check out Funcation")
        self.slide.thumnailImageView.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        self.slide.draggedView.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        self.slide.sliderHolderView.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        self.slide.defaultLabelText = "Slide to check in"
        self.slide.thumnailImageView.image = UIImage.ionicon(with: .iosCheckmark, textColor: UIColor.white, size: CGSize(width: 50, height: 50))
        self.Check_location_start = false
        self.checkStatus = false
        
        self.latvalue2 = 0.0
        self.longvalue2 = 0.0
        self.locationManager.stopUpdatingLocation()
        
        
        if !NetworkState.isConnected() {
            
            let checkinTimeIntervale = Date().timeIntervalSince1970
            
            let ConvertTimeIntervale = Int(checkinTimeIntervale)
            DataBaseHelper.ShareInstance.updateRecord(id_value: id_values, checkout: String(ConvertTimeIntervale))
            
            id_values = ""
        }
    }
    // MARK: - Checkin Checkout API
    
    fileprivate func CheckinCheckout(url : String,type:String)
    {
        if !NetworkState.isConnected()
        {
            AppDelegate.hideWaitView()
            if type == "checkin"
            {
                self.Check_location_start = true
                self.checkStatus = true
                UserDefaults.standard.set("yes", forKey: "Check_in")
                UserDefaults.standard.synchronize()
                self.Slider_Check_in()
                self.objAppDelegate?.StartTimer()
            }
            else if type == "checkout"
            {
                self.Check_location_start = false
                self.checkStatus = false
                UserDefaults.standard.set("no", forKey: "Check_in")
                UserDefaults.standard.synchronize()
                self.Slider_Check_out()
                self.objAppDelegate?.StopTimer()
            }
            return
        }
        
        AppDelegate.showWaitView()
    
        var parama = [String:Any]()
        parama["type"] = type
        parama["lat"] = latvalue1
        parama["long"] = longvalue1
        parama["visit_id"] = visit_id
        
        print("Bhushan :---->",parama)
        
        CheckinCheckout_API().Fetch(param : parama, Url: url) { (status, message,info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                print("Info :----> ",info as Any)
                self.CheckOutMessage(message: message)
                self.locationManager.stopUpdatingLocation()
                return
            }
            if type == "checkin"
            {
                self.Check_location_start = true
                self.checkStatus = true
                UserDefaults.standard.set("yes", forKey: "Check_in")
                UserDefaults.standard.synchronize()
                self.Slider_Check_in()
                self.objAppDelegate?.StartTimer()
                
            }
            else if type == "checkout"
            {
                self.Check_location_start = false
                self.checkStatus = false
                
                UserDefaults.standard.set("no", forKey: "Check_in")
                UserDefaults.standard.synchronize()
                self.Slider_Check_out()
                self.objAppDelegate?.StopTimer()
            }
        }
    }
    
    func CheckOutMessage (message:String)
    {
        if message == "Please fill all required fields."
        {
            self.Check_location_start = false
            self.checkStatus = false
            
            UserDefaults.standard.set("no", forKey: "Check_in")
            UserDefaults.standard.synchronize()
            self.Slider_Check_out()
            self.objAppDelegate?.StopTimer()
            return
        }
        
        self.popupAlert(title: languageKey(key: Bundle.appName()), message: message, actionTitles: [languageKey(key: "OK")], actions: [{action1 in
            
            self.tabBarController?.selectedIndex = 0
            
            if message == "Please fill all the mandatory fields for today's visits"
            {
                self.tabBarController?.selectedIndex = 0
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "todaypending"), object: nil)
            }
            else if message == "Please Submit your Report after that you can checkout from application."
            {
                self.tabBarController?.selectedIndex = 0
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubMitReport"), object: nil)
            }
            else
            {}
            
            }, nil])
    }
    
    // MARK: - Enter Exit Time API
    
    fileprivate func EnterExit_Time(url : String,type:String,id_visit:String)
    {
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            return
        }
        var parama = [String:Any]()
        parama["type"] = type
        parama["lat"] = latvalue1
        parama["long"] = longvalue1
        parama["visit_id"] = id_visit
        
        CheckinCheckout_API().Fetch(param : parama, Url: url) { (status, message,info) in
            AppDelegate.hideWaitView()
            
            if !status
            {
                return
            }
            self.VisitRouteListData?.remove(at: 0)
        }
    }
    
    // MARK: - timer Funcation
    
    func timerAction()
    {
        print("Call Timer")
        
        print(Check_location_start)
        
        if Check_location_start == true
        {
            print("Check_IN")
            self.locationManager.startUpdatingLocation()
            checkincheckout()
        }
        else
        {
            print("Check_OUT")
            self.locationManager.stopUpdatingLocation()
            
        }
    }
    // MARK: - Start Getting Current Location
    
    func LocationLatLong() {
        locationManager = CLLocationManager()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager!.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
    }
    
    // MARK: - LocationManager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if NetworkState.isConnected() {
            
            OfflineCheckinData()
        }
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        latvalue1 = locValue.latitude
        longvalue1 = locValue.longitude
        print("Location latvalue1 :----->",latvalue1)
        print("Call longvalue1 :------->",longvalue1)
        print("Call Location Delegate")
        
        guard let user_id = SignedUserInfo.sharedInstance?.user_id else{return}
        
        if latvalue2 == 0.0
        {
            print("locations = \(locValue.latitude) \(locValue.longitude)")
            
            Selected_Game_Array = ["time":CurrentTime(),"lat":latvalue1,"lng":longvalue1]
            
            if !NetworkState.isConnected() {
                
                DataBaseHelper.ShareInstance.Checkin(date: CurrentDate(), user_id: user_id, lat: "\(latvalue1)", lng: "\(longvalue1)", time: CurrentTime())
            }
            else
            {
                ref.child("locations").child(user_id).child(CurrentDate()).childByAutoId().setValue(Selected_Game_Array)
            }
            
            latvalue2 = latvalue1
            longvalue2 = longvalue1
        }
        else
        {
            let value = distance(lat1:latvalue1, lon1:longvalue1, lat2:latvalue2, lon2:longvalue2, unit: "K")
            
            print("Meter Value :-------> ",value)
            if( value < 10.0 ) {
                print("Within 10 meter radius")
                
            } else {
                
                latvalue2 = latvalue1
                longvalue2 = longvalue1
                
                Selected_Game_Array = ["time":CurrentTime(),"lat":latvalue1,"lng":longvalue1]
                
                if !NetworkState.isConnected() {
                    
                    DataBaseHelper.ShareInstance.Checkin(date: CurrentDate(), user_id: user_id, lat: "\(latvalue1)", lng: "\(longvalue1)", time: CurrentTime())
                    
                }
                else
                {
                    ref.child("locations").child(user_id).child(CurrentDate()).childByAutoId().setValue(Selected_Game_Array)
                }
                print("Outside 10 meter radius")
            }
        }
    }
    // MARK: - checkin checkout Function
    
    func checkincheckout()
    {
        if VisitRouteListData?.count != 0 && VisitRouteListData != nil
        {
            let list = self.VisitRouteListData?[0]
            
            let Visit_Lat = list!["lattitude"].doubleValue
            let Visit_Long = list!["longitude"].doubleValue
            
            let Distance    =  distance(lat1:latvalue1, lon1:longvalue1, lat2:Visit_Lat, lon2:Visit_Long, unit: "K")
            
            if Distance_Status == false
            {
                print("Distance :------>",Distance)
                
                visit_id = list!["visit_id"].intValue
                
                if( Distance < 20.0 ) {
                    
                    self.EnterExit_Time(url: "/visitenterexit", type: "enter",id_visit:list!["visit_id"].stringValue)
                    
                    Distance_Status = true
                    
                    print("Distance Within 10 meter radius in Enter Time")
                }
            }
            
            if Distance_Status == true
            {
                let Visit_Lat = list!["lattitude"].doubleValue
                let Visit_Long = list!["longitude"].doubleValue
                
                let Distance    =  distance(lat1:latvalue1, lon1:longvalue1, lat2:Visit_Lat, lon2:Visit_Long, unit: "K")
                
                print("Old = \(latvalue1) \(longvalue1)")
                print("New = \(Visit_Lat) \(Visit_Long)")
                visit_id = list!["visit_id"].intValue
                
                if( Distance > 150.0 ) {
                    self.EnterExit_Time(url: "/visitenterexit", type: "exit",id_visit:list!["visit_id"].stringValue)
                    Distance_Status = false
                    print("Distance OUT side 10 meter radius in Enter Time")
                }
            }
        }
    }
    // MARK: - Get Current Date
    
    func CurrentDate()->String{
        let currentDateTime = Date()
        
        // initialize the date formatter and set the style
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: currentDateTime)
    }
    
    // MARK: - Get Current Time
    
    func CurrentTime()->String{
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: currentDateTime)
    }
    
    // MARK: - distance Between Two Location
    
    func deg2rad(deg:Double) -> Double {
        return deg * M_PI / 180
    }
    
    func rad2deg(rad:Double) -> Double {
        return rad * 180.0 / M_PI
    }
    func distance(lat1:Double, lon1:Double, lat2:Double, lon2:Double, unit:String) -> Double {
        let theta = lon1 - lon2
        var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
        dist = acos(dist)
        dist = rad2deg(rad: dist)
        dist = dist * 60 * 1.1515
        dist = dist * 1609.34
        return dist
    }
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        Check_In_Out_LBL.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Check In", comment: "")
        
        Time_title_LBL.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Time", comment: "")
    }
    
    // MARK: - Fetch Offline Checkinout Data
    
    fileprivate func OfflineCheckinoutData()
    {
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.CheckInOutTime.rawValue) { (data) in
            
            if let object = data
            {
                print("object:---->",object.count)
                for CheckData in object
                {
                    
                    self.Selected_Game_Array = ["checkIn":CheckData.value(forKey: "checkIn") as! String,"checkout":CheckData.value(forKey: "checkout") as! String,"date":CheckData.value(forKey: "date") as! String]
                    print(self.Selected_Game_Array)
                }
                DataBaseHelper.ShareInstance.DeleteContext(ClassName : OfflineDataClass.CheckInOutTime.rawValue)
            }
        }
    }
    
    // MARK: - Fetch Offline Checkin Data
    fileprivate func OfflineCheckinData()
    {
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.CheckinTime.rawValue) { (data) in
            
            if let object = data
            {
                print("object:---->",object.count)
                for CheckData in object
                {
                    
                    self.Selected_Game_Array = ["time":CheckData.value(forKey: "time") as! String,"lat":CheckData.value(forKey: "lat") as! String,"lng":CheckData.value(forKey: "lng") as! String]
                    
                    self.ref.child("locations").child(CheckData.value(forKey: "user_id") as! String).child(CheckData.value(forKey: "date") as! String).childByAutoId().setValue(self.Selected_Game_Array)
                    
                }
                DataBaseHelper.ShareInstance.DeleteContext(ClassName : OfflineDataClass.CheckinTime.rawValue)
            }
        }
    }
}

// MARK: - Timer Protocols

extension CheckinVC : TimerProtocols
{
    func TimerProtocols_Call()
    {
        self.timerAction()
    }
}
// MARK: - Class Instance

extension CheckinVC
{
    class func instance()->CheckinVC?
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CheckinVC") as? CheckinVC
        return controller
    }
}
