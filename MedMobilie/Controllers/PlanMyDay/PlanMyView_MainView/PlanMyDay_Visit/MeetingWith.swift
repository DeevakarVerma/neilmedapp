//
//  MeetingWith.swift
//  MedMobilie
//
//  Created by MAC on 30/03/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//

import UIKit

import DGElasticPullToRefresh
import SwiftyJSON
import SDWebImage
import SwipeCellKit
import Letters
class MeetingWith: InterfaceExtendedController {
    var CheckArray = [JSON] ()
    var ContactArray = [String : Any]()
    var DoctorData : JSON = []
    var MeetingWithSTR  = ""
    var placeId  = ""
    var lat = 0.0
    var long = 0.0
    var titleName = ""
    var address = ""
    var checked = [Int]()
    var checked_values = [Int]()
    var SaveArray = [[String : Any]]()
    var delegate : SelectedContactProtocol?
    
    @IBOutlet weak var mtableview: UITableView!
   
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(DoctorData as Any)
        print(DoctorData.count)
        
        
         self.checked = Array(repeating: 90000, count: DoctorData.count)
        
      
        
       
        self.mtableview.delegate = self
        self.mtableview.dataSource = self
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
        mtableview.rowHeight = UITableView.automaticDimension
        mtableview.estimatedRowHeight = 100
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
         RightActionButton(Title:self.languageKey(key: "Create Visit"))
        
    }
    
    // MARK: - Save Button Action

    override func rightButtonAction(){
        
        print(checked)
        
        
        MeetingWithSTR = ""
        
        
        for index in 0..<(checked.count) {
            
            
            if  checked[index] != 90000
                       {
                        
                        
                     MeetingWithSTR += "\(checked[index]) \(",")"
                        
            }
            
            
            
            print(MeetingWithSTR)
            
        }
        
        
        if MeetingWithSTR == ""
        {
            
            self.popupAlert(title: Bundle.appName(), message: self.languageKey(key: "you must select atleast one Contact to Create Visit"), actionTitles: [self.languageKey(key: "OK")], actions:[{action1 in
                print("ok")
                return
                }, nil])
        }
        else
        {
            
         
            AppDelegate.showWaitView()

            
            createVisitFromPlace(placeId:placeId , lat: lat, long: long, titleName: titleName, address: address, MeetingWith: MeetingWithSTR)
            
        }
        }
      
  
    // MARK: - Language Setting
    @objc override func LanguageSet(){
        
        
    }
    
    
    
    // MARK: - Create Visit From Place API

     func createVisitFromPlace(placeId : String , lat : Double,long : Double , titleName : String,address :String,MeetingWith:String)
    {
        var param = [String:Any]()
        param["place_id"] = placeId
        param["lat"] = lat
        param["long"] = long
        param["title"] = titleName
        param["address"] = address
          param["doctor_ids"] = MeetingWith
        
        if !NetworkState.isConnected() {
            AppDelegate.hideWaitView()
            AppDelegate.alertViewForInterNet()
            return
        }
        DataFetchWithParam_Api().Request(parameter: param, link: "/createvisitfromplan") { (status, message, data) in
            
            AppDelegate.hideWaitView()
            if !status
            {
                self.alert(message)
                return
            }
            self.popupAlert(title: Bundle.appName(), message: message, actionTitles: [self.languageKey(key: "Ok")], actions: [{ action1 in
                
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Update_PlanMyDay"), object: nil)
                
                self.navigationController?.popViewController(animated: true)
                
                
                },nil])
            
            /*self.alert("Visit Created Succesfully")
             return*/
            
        }
        
    }
    
    
    
    
}
    

// MARK: - Table View Delegate/DataSource Methods

extension MeetingWith : UITableViewDelegate,UITableViewDataSource
{
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DoctorData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        
        
       let infos = DoctorData[indexPath.row]
        
        
        
        if infos["is_express"].boolValue
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
        }
        
        
        
        
        cell.imgUser?.setImage(string: infos["contact_name"].stringValue, color: nil, circular: true)
        
        
        
        cell.lblUserName.text = "\(infos["salutation"].stringValue) \(infos["contact_name"].stringValue)"
        cell.lblMessage.text = infos["category_name"].stringValue
        cell.selectionStyle = .none
        
        
        
        
        if checked[indexPath.row] == 90000 {
            cell.accessoryType = .none
        } else {
            cell.accessoryType = .checkmark
        }

        return cell
    }
   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
         let infos = DoctorData[indexPath.row]
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
          
                cell.accessoryType = .none
                checked[indexPath.row] = 90000
            } else {
                cell.accessoryType = .checkmark
                checked[indexPath.row] = infos["contact_id"].intValue
            }
        }
        
        print(checked)
        
    }
    
    
    
    
    
}


//Mark: - Class Instance

extension MeetingWith
{
    class func instance()->MeetingWith?{
        let storyboard = UIStoryboard(name: "PlanMyDay", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MeetingWith") as? MeetingWith
        
        
        return controller
    }
}

