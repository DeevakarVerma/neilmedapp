//
//  SearchResultsController.swift
//  PlacesLookup
//
//  Created by Malek T. on 9/30/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import GoogleMaps


protocol LocateOnTheMap{
    func locateWithLongitude(_ lon:Double, andLatitude lat:Double, andTitle title: String)
}

class SearchResultsController: UITableViewController {
    
    var searchResults: [String]!
    var delegate: LocateOnTheMap!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchResults = Array()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.searchResults.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        
        cell.textLabel?.text = self.searchResults[indexPath.row]
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath){
        // 1
        self.dismiss(animated: true, completion: nil)
        // 2
        let urlpath = "https://maps.googleapis.com/maps/api/geocode/json?address=\(self.searchResults[indexPath.row])&key=\(AppConnectionConfig.googleMapKeyplace)&sensor=false".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let url = URL(string: urlpath!)
        // print(url!)
        let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
            // 3
            
           
            do {
                if let temp = data{
                     let json = try JSON(data: temp)
                    print(json)
                if json["results"].isEmpty
                {
                    return
                }
                else if let location = json["results"].arrayValue[0]["geometry"]["location"].dictionary
                {
                    
                 let lon = location["lng"]?.doubleValue
                let lat = location["lat"]?.doubleValue
                    
                    self.delegate.locateWithLongitude(lon ?? 0.0, andLatitude: lat ?? 0.0, andTitle: self.searchResults[indexPath.row])
                    
                }
                 //   let lat = json["results"].arrayValue[0].dictionaryValue["geometry"]?.dictionaryValue["location"]?.dictionaryValue["lat"]?.stringValue
                    
                    /*let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                    
                    print(dic)
                    
                    let lat =   (((((dic.value(forKey: "results") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lat")) as! Double
                    
                    let lon =   (((((dic.value(forKey: "results") as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lng")) as! Double
                    // 4
                    self.delegate.locateWithLongitude(lon, andLatitude: lat, andTitle: self.searchResults[indexPath.row])*/
                }
                
            }catch {
                print("Error")
            }
        }
        // 5
        task.resume()
    }
    func geoAddress(lat : Double , long : Double)
    {
        let geocoder = GMSGeocoder()
        
        let coordinate = CLLocationCoordinate2DMake(lat, long)
        
        print(coordinate)
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                guard let lines = address.lines as? [String] else{return}
           //     print(lines)
                currentAddress = lines.joined(separator: ",")
                print(currentAddress)
                self.delegate.locateWithLongitude(long, andLatitude: lat, andTitle: currentAddress)
                //currentAddress = lines.joinWithSeparator("\n")
                
                //currentAdd(returnAddress: currentAddress)
            }
        }
    }

    
    
    
    func reloadDataWithArray(_ array:[String]){
        self.searchResults = array
        self.tableView.reloadData()
    }
    
  
    
}
