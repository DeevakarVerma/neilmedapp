//
//  slider.swift
//  DemoSwiftySignature
//
//  Created by dr.mac on 08/02/19.
//  Copyright © 2019 Walzy. All rights reserved.
//

import UIKit

class Mycustomslider: UISlider {
    
    var label: UILabel
    var labelXMin: CGFloat?
    var labelXMax: CGFloat?
    var labelText: ()->String = { "" }
    
    required init(coder aDecoder: NSCoder) {
        label = UILabel()
        
        super.init(coder: aDecoder)!
        self.addTarget(self, action: #selector(onValueChanged), for: .valueChanged)
        
    }
    func setup(){
        
        labelXMin = frame.origin.x + 10
        
        labelXMax = frame.origin.x + self.frame.width - 10
        
        let labelXOffset: CGFloat = labelXMax! - labelXMin!
        
        let valueOffset: CGFloat = CGFloat(self.maximumValue - self.minimumValue)
        
        let valueDifference: CGFloat = CGFloat(self.value - self.minimumValue)
        
        let valueRatio: CGFloat = CGFloat(valueDifference/valueOffset)
        
        let labelXPos = CGFloat(labelXOffset*valueRatio - labelXMin!)
        
        label.frame = CGRect(x: labelXPos, y: self.frame.origin.y - 35, width: 43, height: 20)
        // label.text = self.value.description
        
        let s = Int(round(Float(self.value.description)!))
        
        label.text = "\(s % 1000) miles"
        
        
        self.superview!.addSubview(label)
        
    }
    
    //MARK:- Harsh
    func updateLabel(){
        // label.text = labelText()
        label.backgroundColor =  UIColor.orange//UIColor(patternImage: UIImage(named: "comment_button")!)
        let labelXOffset: CGFloat = labelXMax! - labelXMin!
        let valueOffset: CGFloat = CGFloat(self.maximumValue - self.minimumValue)
        let valueDifference: CGFloat = CGFloat(self.value - self.minimumValue)
        let valueRatio: CGFloat = CGFloat(valueDifference/valueOffset)
        let labelXPos = CGFloat(labelXOffset*valueRatio + labelXMin!)
       
        label.frame =  CGRect(x: labelXPos - label.frame.width/2, y: self.frame.origin.y + 35, width: 43, height: 20)
        label.textAlignment = NSTextAlignment.center
        self.superview!.addSubview(label)
       // self.layoutSubviews()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
        updateLabel()
    }
   @objc func onValueChanged(sender: Mycustomslider){
        updateLabel()
    }
    
}

