//
//  SelectVisitVC.swift
//  MedMobilie
//
//  Created by dr.mac on 03/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class SelectVisitVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblSelectVisit: UILabel!
    
    var arrSelectVisitList = ["1","2","3","4"]
    
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    // MARK: - Button Action
    
    @IBAction func btnActForDone(_ sender: UIButton) {
        
    }
   

}

// MARK: - Table View Delegate/DataSource Methods
extension SelectVisitVC : UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - TableView Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSelectViewIdentifier", for: indexPath)
        
        let btnSelect = cell.viewWithTag(1) as! UIButton
        let lblTaskName = cell.viewWithTag(2) as! UILabel
        let lblTaskDate = cell.viewWithTag(3) as! UILabel
        let lblAssignedTo = cell.viewWithTag(4) as! UILabel
        
        if self.arrSelectVisitList.contains(arrSelectVisitList[(indexPath.row)]) {
            btnSelect.isSelected = true
        } else {
            btnSelect.isSelected = false
        }
        
        btnSelect.addTarget(self, action: #selector(self.BtnActForSelectAndUnselect(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    @objc func BtnActForSelectAndUnselect(_ sender : UIButton) {
    
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        if arrSelectVisitList.contains(arrSelectVisitList[(indexPath?.row)!]) {
            
            self.arrSelectVisitList.remove(at: (indexPath?.row)!)
            
        } else {
        self.arrSelectVisitList.append(arrSelectVisitList[(indexPath?.row)!])
            
        }
        
        
        print(self.arrSelectVisitList)
        
        self.tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let share = UITableViewRowAction(style: .normal, title: "Pending") { action, index in
            print("share button tapped")
        }
        share.backgroundColor = UIColor.gray
        
        return [share]
        
    }
    
}


