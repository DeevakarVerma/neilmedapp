
//
//  ContactsVC.swift
//  MedMobilie
//
//  Created by dr.mac on 20/12/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//
import UIKit
import DGElasticPullToRefresh
import SwiftyJSON
import SDWebImage
import SwipeCellKit
import Letters
class SelectSingleContact: InterfaceExtendedController {
    
    fileprivate var contactsData : [JSON]?
    
    
    var CheckArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var checked = [Int]()
    var checked_values = [Int]()
    var SaveArray = [[String : Any]]()
    var SelectedContactDetail: ((_ contactData : [String : Any]) -> ())?
    
    
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var mtableview: UITableView!
    
    
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // self.LanguageSet()
        btnView.createCircleForView()
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        self.mtableview.delegate = self
        self.mtableview.dataSource = self
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
        mtableview.rowHeight = UITableView.automaticDimension
        mtableview.estimatedRowHeight = 100
        AppDelegate.showWaitView()
        OfflinecontactDataFetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        
        
        
    }
    
    
    // MARK: - Offline contact DataFetch
    
    fileprivate func OfflinecontactDataFetch()
    {
        
        
        DataBaseHelper.ShareInstance.FetchcontactRequest(ClassName: OfflineDataClass.Contact.rawValue) { (data) in
            AppDelegate.hideWaitView()
            if let object = data
            {
                var jsonArray = [JSON]()
                for jsonData in object
                {
                    if let jsondata = jsonData.value(forKey: "json") as? Data {
                        
                        jsondata.retrieveJSON(completion: { (json) in
                            jsonArray.append(json!)
                        })
                        
                    }
                }
                self.mtableview.dg_stopLoading()
                self.contactsData = jsonArray
                
                self.checked = Array(repeating: 90000, count: self.contactsData?.count ?? 0)
                self.mtableview.reloadData()
            }
            
        }
    }
    
    
    
    // MARK: - create New Contact
    
    @IBAction func createNewContact(_ sender: Any) {
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [ self.languageKey(key: "Create contact"),self.languageKey(key: "Express contact")], actions:[{action1 in
            
            
            guard let AddContactViewController =  AddContactVC.instance() else{return}
            
            self.navigationController?.pushViewController(AddContactViewController, animated: true)
            
            },{action2 in
                
                
                
                guard let controller = ExpressContactVC.instance()
                    else{return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
        
        
    }
    
    
}


// MARK: - Table View Delegate/DataSource Methods

extension SelectSingleContact : UITableViewDelegate,UITableViewDataSource , SwipeTableViewCellDelegate
{
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        
        guard let infos = contactsData?[indexPath.row]  else{return cell}
        
        if infos["is_express"].boolValue
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.green)
        }
        else
        {
            cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        }
        
        cell.imgUser?.setImage(string: infos["contact_name"].stringValue, color: nil, circular: true)
        cell.lblUserName.text = "\(infos["salutation"].stringValue) \(infos["contact_name"].stringValue)"
        cell.lblMessage.text = infos["category_name"].stringValue
        cell.delegate = self
        cell.selectionStyle = .none
        
        if checked[indexPath.row] == 90000 {
            cell.accessoryType = .none
        } else {
            cell.accessoryType = .checkmark
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        
        return nil
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        let infos = contactsData?[indexPath.row]
        let param = ["id":infos!["id"].intValue,"contact_name":infos!["contact_name"].stringValue,"visit_contact_id":0,"salutation":infos!["salutation"].stringValue,"category_name":infos!["category_name"].stringValue,"temp_id":infos!["temp_id"].intValue,"created_at":infos!["created_date"].intValue] as [String:Any]
        self.SelectedContactDetail?(param)
        self.navigationController?.popViewController(animated: true)
        
        //        self.delegate?.Expresscontactdetails(name:infos!["contact_name"].stringValue,Contact_id:infos!["id"].intValue)
        
        
        
    }
    
    
    
    
    
}

// MARK: - Pull To Refresh
extension SelectSingleContact
{
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?.OfflinecontactDataFetch()
            //            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
            //                self?.mtableview.dg_stopLoading()
            //            })
            
            }, loadingView: loadingView)
        mtableview.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        mtableview.dg_setPullToRefreshBackgroundColor(mtableview.backgroundColor!)
    }
}


// MARK: - Class instance

extension SelectSingleContact
{
    class func instance()->SelectSingleContact?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectSingleContact") as? SelectSingleContact
        
        
        return controller
    }
}


