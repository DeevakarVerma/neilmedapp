
import UIKit
import DGElasticPullToRefresh
import SwiftyJSON
import SDWebImage
import SwipeCellKit
import Letters
class SelectContactVC: InterfaceExtendedController , UISearchBarDelegate {
    
    fileprivate var contactsData : [JSON] = [JSON]()
    fileprivate var mainJsonData : [JSON] = [JSON]()
    fileprivate var resultSearchController = UISearchController()
    
    
     var ClassType = ""
    var CheckArray = NSMutableArray ()
    var ContactArray = [String : Any]()
    var CheckedContactId = [Int]()
    var AllContactId = [Int]()
    var SaveArray = [[String : Any]]()
    var delegate : SelectedContactProtocol?
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblPlusSign: UILabel!
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var mtableview: UITableView!
    
    fileprivate var offset : Int = 0
    fileprivate var totalContacts : Int = 0
    fileprivate var FilterString : String = ""
    
    
     var SelectedContactDetail: ((_ contactData : [String : Any]) -> ())?
    
    // MARK: - Class life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print(CheckArray)
        print(CheckArray.count)
        print(CheckArray.value(forKey: "contact_id"))
        
        if let values = CheckArray.value(forKey: "contact_id") as? [Int]
        {
            CheckedContactId = values
        }
        
        
        print(CheckedContactId)
        if ClassType == ""
        {
        RightActionButton(Title:languageKey(key:"Save"))
        }
        
        // self.LanguageSet()
        btnView.createCircleForView()
        lblPlusSign.font = UIFont.ionicon(of: 30)
        lblPlusSign.text = String.ionicon(with: .plus )
        self.mtableview.delegate = self
        self.mtableview.dataSource = self
        self.mtableview.tableFooterView = UIView()
        self.mtableview.separatorStyle = .none
        mtableview.rowHeight = UITableView.automaticDimension
        mtableview.estimatedRowHeight = 100
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.SetUpSearchBar()
        PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: SettingNAVBAR(), BtnColor: UIColor.white)
        AppDelegate.showWaitView()
        self.FetchTotalContactsOfContacts()
        self.FetchingOfflineData(skip: 0  , refress:0)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resultSearchController.dismiss(animated: false, completion: nil)
    }
    
    
    // MARK: - Language Setting
    
    @objc override func LanguageSet(){
        
        
    }
    
    // MARK: - SetUp SearchBar
    
    func SetUpSearchBar()
    {
        self.resultSearchController = ({
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchBar.delegate = self
            controller.searchBar.placeholder = "Search contact".languageSet
            controller.searchBar.tintColor = TeamNAVBAR()
            controller.searchBar.barTintColor = .white
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.hidesNavigationBarDuringPresentation = false
            self.mtableview.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    fileprivate func FetchTotalContactsOfContacts ()
    {
        self.totalContacts = DataBaseHelper.ShareInstance.getRecordsCount()
    }
    
    fileprivate func FetchingOfflineData(skip : Int , str : String = "", refress:Int)
    {
        //AppDelegate.showWaitView()
        //        print("offlineData ---- -----")
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        if self.contactsData.count > 1 {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
        DataBaseHelper.ShareInstance.FetchContactLimitedData(offset : skip ,filterWord: FilterString ){ (json , totalCounts) in
            
            AppDelegate.hideWaitView()
          
            DispatchQueue.main.async{
                
                self.contactsData = [JSON]()
                self.mtableview.dg_stopLoading()
                self.mainJsonData.append(json)
                self.contactsData = self.mainJsonData
                print("contactsData offline Data :--->",self.contactsData)
                self.mtableview.reloadData()
                
                
                if self.contactsData.count == 0 {
                    self.FetchingOfflineData(skip: 0  , refress:0)
                }
                else
                {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
               
            }
        }
    }
    
    //MARK: - SearchBar Update
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if searchBar.text != ""
        {
            FilterString = searchBar.text ?? ""
            self.mainJsonData = [JSON]()
            self.FetchingOfflineData(skip: 0, str: searchBar.text ?? "" , refress:1)
        }
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        FilterString = ""
        self.mainJsonData = [JSON]()
        self.FetchingOfflineData(skip: 0, str:"" , refress:1)
        
    }
    
    
    
    // MARK: - Save Button Action
    
    override func rightButtonAction(){
        
        CheckArray.removeAllObjects()
        
        print(CheckedContactId)
        let  WithoutDuplicates = CheckedContactId.removingDuplicates()
        
        CheckedContactId.removeAll()
        CheckedContactId = WithoutDuplicates
        
        // checked.
        for index in 0..<(CheckedContactId.count) {
            
            var indexpath = 0
            
            mainJsonData.enumerated().forEach({
                
                if $0.element["id"].intValue == CheckedContactId[index]
                {
                    indexpath = $0.offset
                }
                
            })
            
            var contact_id = 0
            
            let infos = contactsData[indexpath]
            if  infos["contact_id"].exists() {
                
                contact_id = infos["contact_id"].intValue
            }
            else{
                
                contact_id = infos["id"].intValue
            }
            
            
            
            
            ContactArray = ["salutation":infos["salutation"].stringValue,"contact_name":infos["contact_name"].stringValue,"attn_name":infos["attn_name"].stringValue,"category_name":infos["category_name"].stringValue,"primary_speciality_name": infos["primary_speciality_name"].stringValue,"secondary_specialities_name": infos["secondary_specialities_name"].stringValue,"contact_title_name":infos["contact_title_name"].stringValue,"company_name":infos["company_name"].stringValue,"phone":infos["phone"].stringValue,"email":infos["email"].stringValue,"website":infos["website"].stringValue,"lead_status_name":infos["lead_status_name"].stringValue,"lead_source":infos["lead_source"].stringValue,"willingtostock":infos["willingtostock"].stringValue,"address1":infos["address1"].stringValue,"address2":infos["address2"].stringValue,"city":infos["city"].stringValue,"zipcode":infos["zipcode"].stringValue,"country_name":infos["country_name"].stringValue,"state_name":infos["state_name"].stringValue,"fax":infos["fax"].stringValue,"type":infos["type"].stringValue,"default_billing_address":infos["default_billing_address"].stringValue,"default_shipping_address":infos["default_shipping_address"].stringValue,"billing_address1":infos["billing_address1"].stringValue,"billing_address2":infos["billing_address2"].stringValue,"billing_city":infos["billing_city"].stringValue,"billing_zipcode":infos["billing_zipcode"].stringValue,"billing_country_name":infos["billing_country_name"].stringValue,"billing_state_name":infos["billing_state_name"].stringValue,"billing_fax":infos["billing_fax"].stringValue,"shipping_address1":infos["shipping_address1"].stringValue,"shipping_address2":infos["shipping_address2"].stringValue,"shipping_city":infos["shipping_city"].stringValue,"shipping_zip_code":infos["shipping_zip_code"].stringValue,"shipping_country_name":infos["shipping_country_name"].stringValue,"shipping_state_name":infos["shipping_state_name"].stringValue,"shipping_fax":infos["shipping_fax"].stringValue,"contact_id":contact_id]
            
            
            CheckArray.add(ContactArray)
            
            
            
        }
        
        print("Selected value Count:----->",CheckArray.count)
        print("Selected value :----->",CheckArray)
        
        self.navigationController?.popViewController(animated: true)
        
        
        self.delegate?.selected(selected_array: CheckArray)
        
        
    }
    
    
    
    // MARK: - create New Contact
    
    @IBAction func createNewContact(_ sender: Any) {
        
        
        self.popupAlertWithSheet(title: Bundle.appName(), message: nil, actionTitles: [ self.languageKey(key: "Create contact"),self.languageKey(key: "Express contact")], actions:[{action1 in
            
            //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
            
            guard let AddContactViewController =  AddContactVC.instance() else{return}
            
            self.navigationController?.pushViewController(AddContactViewController, animated: true)
            
            },{action2 in
                
                //      NotificationCenter.default.post(name: NSNotification.Name(rawValue: TabbarIndexChange), object: 2)
                
                guard let controller = ExpressContactVC.instance()
                    else{return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            }, nil])
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
}

// MARK: - Table View Delegate/DataSource Methods

extension SelectContactVC : UITableViewDelegate,UITableViewDataSource , SwipeTableViewCellDelegate
{
    // MARK: - Table view data source
    
    //1. determine number of rows of cells to show data
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TeamCell
        cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
        
        print("contactsData cellForRow Data :--->",self.contactsData)
        
        if !contactsData.isEmpty
        {
            
            let infos = contactsData[indexPath.row]
            
            if infos["is_express"].boolValue
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.red)
            }
            else
            {
                cell.viewShadow?.bottomViewShadow(ColorName: UIColor.gray)
            }
            
            cell.imgUser?.setImage(string: infos["contact_name"].stringValue, color: nil, circular: true)
            
            
            cell.lblUserName.text = "\(infos["salutation"].stringValue) \(infos["contact_name"].stringValue)"
            cell.lblMessage.text = infos["category_name"].stringValue
            cell.delegate = self
            cell.selectionStyle = .none
            
            
            let idvalue = infos["id"].intValue
            
            print("id Value",idvalue)
            print("CheckedContactId",CheckedContactId)
            
            
            
            let filteredItems = self.CheckedContactId.filter { $0 == idvalue}
            print(filteredItems.count)
            
            if filteredItems.count == 0
            {
                cell.accessoryType = .none
            }
            else
            {
                cell.accessoryType = .checkmark
            }
            
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // swipe cell for delete and edit
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        
        return nil
        
    }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.transitionStyle = .border
        return options
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let infos = contactsData[indexPath.row]
        
        if ClassType == ""
        {

        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                
                let json = self.contactsData[indexPath.row]
                
                if let index = CheckedContactId.index(of: json["id"].intValue) {
                    CheckedContactId.remove(at: index)
                }
                
                cell.accessoryType = .none
            } else {
                cell.accessoryType = .checkmark
                CheckedContactId.append(infos["id"].intValue)
            }
        }
        }
        else if ClassType == "Express"
        {
            let param = ["id":infos["id"].intValue,"contact_name":infos["contact_name"].stringValue,"visit_contact_id":0,"salutation":infos["salutation"].stringValue,"category_name":infos["category_name"].stringValue,"temp_id":infos["temp_id"].intValue,"created_at":infos["created_date"].intValue] as [String:Any]
            self.SelectedContactDetail?(param)
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            
        }
        
        
        print(CheckedContactId)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.size.height {
            
            
            if self.totalContacts > self.contactsData.count
            {
                self.offset = self.offset + 100
                self.FetchingOfflineData(skip: self.offset  , refress:1)
            }
            
        }
        
        
    }
    
    
    
}
// MARK: - Pull  ToRefresh

extension SelectContactVC
{
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = self.navBackgroundColor ?? BillNAVBAR()
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = SettingNAVBAR()
        mtableview.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self?.mainJsonData = [JSON]()
            self?.contactsData = [JSON]()
            self?.FetchingOfflineData(skip: 0  , refress:0)
            
            
            }, loadingView: loadingView)
        mtableview.dg_setPullToRefreshFillColor(navBackgroundColor ?? WhiteColor())
        mtableview.dg_setPullToRefreshBackgroundColor(mtableview.backgroundColor!)
    }
}


extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

// MARK: - Class instance
extension SelectContactVC
{
    class func instance()->SelectContactVC?{
        let storyboard = UIStoryboard(name: "Team", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SelectContactVC") as? SelectContactVC
        
        
        return controller
    }
}


