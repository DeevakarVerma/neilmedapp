//
//  FogotPasswordVC.swift
//  MedMobilie
//
//  Created by dr.mac on 22/11/18.
//  Copyright © 2018 dr.mac. All rights reserved.
//

import UIKit

class FogotPasswordVC: InterfaceExtendedController {
    
     @IBOutlet weak var Reset_Password_desc_LBL: UILabel!
     @IBOutlet weak var Email_Address_LBL: UILabel!
    
    @IBOutlet weak var Reset_Password_BTN: UIButton!
   
    // MARK: - Class life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     //  self.navigationController?.setNavigationBarHidden(false, animated: false)
         PaintNavigationBar(TitleColor: UIColor.white, BackgroundColor: navBackgroundColor ?? SettingNAVBAR(), BtnColor: UIColor.white)
        
    }
    
    // MARK: - Language Setting

    
    @objc override func LanguageSet()
    {
        
         NavigationBarTitleName(Title: "Forgot Password")
         
    
        
        Email_Address_LBL.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "Email address", comment: "")
        
        Reset_Password_desc_LBL.text = LocalizationSystem.SharedInstance.localizedStingforKey(key: "We need your registered e-mail address to send you a reset link.", comment: "")
        
        
        
        Reset_Password_BTN.setTitle(LocalizationSystem.SharedInstance.localizedStingforKey(key: "Reset Password", comment: ""), for: .normal)
        
    }
    
    // MARK: - Button Action
    
    @IBAction func btnActForResetPassword(_ sender: UIButton) {
  
              self.performSegue(withIdentifier: segueID.popUpViewIdentifier, sender: nil)
        
        
        
    }
    
    
}
